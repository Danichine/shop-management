# Shop Management App
This app was created for a specific business that needed a way to manage the customer base and the state of the products owned by those customers.

It's a NextJS app with TypeScript, Firebase, Jest and Mantine UI.

Feel free to use it or change it as you need


## Request flow
* The *Client* calls a *Service*, that knows the endpoint urls and *Api* object
* The *Service* calls the *Api* object and transforms the response into *Models*
* The *Models* have the Business logic
* The *Api* object wraps the fetch api for calling the *Server*
* The *Server* filters the request with middleware and forwards the call to the *Controller*
* The *Controller* chain calls to the *Interactors* to create a single result
* The *Interactors* connect to External *Services* like *Database* and *Storage*
* With the result of the *Interactor*, the response travels back to the *Client*