import api from "@/lib/api";
import { Customer, CustomerType, Product, ProductType } from "@/models";

export const fetchCustomer = async (customerId: string) => {
	const { customer } = await api.get(`/api/customers/${customerId}`);

	return new Customer(customer);
}

export const fetchCustomers = async ({
	page, limit, filter, sort, search, lastVisible
}: {
	page: string, limit: number, filter: string, sort: string, search: string, lastVisible?: string
}) => {

	const queryPage = page ? `page=${page}` : '';
	const queryLimit = limit ? `&limit=${limit}` : '';
	const queryFilter = filter ? `&filter=${filter}` : '';
	const querySort = sort ? `&sort=${sort}` : '';
	const querySearch = search ? `&search=${search}` : '';
	const queryLastVisible = lastVisible ? `&lastVisibleId=${lastVisible}` : '';

	const {
		customers, lastVisibleId, hasNextPage, hasPreviousPage
	} = await api.get(
		`/api/customers?${queryPage}${queryLimit}${queryFilter}${querySort}${querySearch}${queryLastVisible}`
	);

	const parsedCustomers = customers.map((item: CustomerType) => new Customer(item));

	return {
		customers: parsedCustomers,
		lastVisibleId,
		hasNextPage,
		hasPreviousPage
	}
}

export const fetchCustomerProducts = async ({
	customerId, page, limit, filter, sort, search, lastVisible
}: {
	customerId: string, page: string, limit: number, filter?: string, sort: string, search?: string, lastVisible?: string
}) => {
	const queryPage = page ? `page=${page}` : '';
	const queryLimit = limit ? `&limit=${limit}` : '';
	const queryFilter = filter ? `&filter=${filter}` : '';
	const querySort = sort ? `&sort=${sort}` : '';
	const querySearch = search ? `&search=${search}` : '';
	const queryLastVisible = lastVisible ? `&lastVisibleId=${lastVisible}` : '';

	const {
		customer,
		products,
		lastVisibleId,
		hasNextPage,
		hasPreviousPage
	} = await api.get(
		`/api/customers/${customerId}/products?${queryPage}${queryLimit}${queryFilter}${querySort}${querySearch}${queryLastVisible}`
	);

	const parsedProducts = products?.map((item: ProductType) => new Product(item)) || [];
	const parsedCustomer = new Customer(customer)

	return {
		customer: parsedCustomer,
		products: parsedProducts,
		lastVisibleId,
		hasNextPage,
		hasPreviousPage
	}
}

export const deleteCustomer = async (customerId: string) => {
	return await api.delete( `/api/customers/${customerId}` );
}

export const createCustomer = async (body: any) => {
	return await api.post( `/api/customers`, body );
}

export const updateCustomer = async (customerId: string, body: any) => {
	return await api.put( `/api/customers/${customerId}`, body );
}
