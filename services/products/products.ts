import api from "@/lib/api";
import { Product, ProductType } from "@/models";

export const fetchProducts = async ({
	page, limit, filter, sort, search, lastVisible
}: {
	page: string, limit: number, filter: string, sort: string, search: string, lastVisible?: string
}) => {
	const queryPage = page ? `page=${page}` : '';
	const queryLimit = limit ? `&limit=${limit}` : '';
	const queryFilter = filter ? `&filter=${filter}` : '';
	const querySort = sort ? `&sort=${sort}` : '';
	const querySearch = search ? `&search=${search}` : '';
	const queryLastVisible = lastVisible ? `&lastVisibleId=${lastVisible}` : '';

	const { products, lastVisibleId, hasNextPage, hasPreviousPage } = await api.get(
		`/api/products?${queryPage}${queryLimit}${queryFilter}${querySort}${querySearch}${queryLastVisible}`
	);

	const parsedProducts = products.map((item: ProductType) => new Product(item));

	return { products: parsedProducts, lastVisibleId, hasNextPage, hasPreviousPage };
}

export const deleteProduct = async (productId: string) => {
	return await api.delete( `/api/products/${productId}` );
}

export const createProduct = async (body: any) => {
	return await api.post( `/api/products`, body );
}

export const updateProduct = async (productId: string, body: any) => {
	return await api.put( `/api/products/${productId}`, body );
}

export const updateProductState = async (productId: string, body: any) => {
	return await api.put( `/api/products/${productId}/update-state`, body );
}
