import api from "@/lib/api";

export const fetchCurrentUser = async () => {
	return await api.get( '/api/user' );
}
