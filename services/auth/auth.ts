import api from "@/lib/api";

export const login = async (credentials: { email: string, password: string }) => {
	return await api.post( '/api/login', credentials );
}

export const logout = async () => {
	return await api.post('/api/logout');
}

export const changePassword = async (credentials: { oldPassword: string, newPassword: string, confirmNewPassword: string }) => {
	return await api.post('/api/change-password', credentials);
}
