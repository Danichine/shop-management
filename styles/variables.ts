// COLORS
export const COLOR_MAIN = '#EBEBEB';
export const COLOR_SECONDARY = '#CAD2C5';
export const COLOR_TERCIARY = '#84A98C';
export const COLOR_ACCENT = '#59C3C3';
export const COLOR_ACCENT_DARK = '#408f8f';
export const COLOR_HIGHLIGHT = '#52489C';

export const COLOR_ERROR = '#D55050';
export const COLOR_ERROR_DARK = '#A83E3E';
export const COLOR_SUCCESS = '#5fEE5f';
export const COLOR_SUCCESS_DARK = '#47B547';

export const COLOR_WHITE = '#FFF';
export const COLOR_GREY_ULTRA_LIGHT = '#F6F6F6';
export const COLOR_GREY_LIGHT = '#DADADA';
export const COLOR_GREY_MID = '#AFAFAF';
export const COLOR_GREY_DARK = '#8B8B8B';
export const COLOR_GREY_ULTRA_DARK = '#565656';
export const COLOR_BLACK = '#000';

// LAYOUT
export const background = COLOR_MAIN;