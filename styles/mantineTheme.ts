import { COLOR_ERROR, COLOR_SUCCESS } from "./variables";
import { MantineThemeOverride} from '@mantine/core';

const mantineTheme: MantineThemeOverride = {
    colors: {
        success: [
            COLOR_SUCCESS, COLOR_SUCCESS, COLOR_SUCCESS, COLOR_SUCCESS, COLOR_SUCCESS,
            COLOR_SUCCESS, COLOR_SUCCESS, COLOR_SUCCESS, COLOR_SUCCESS, COLOR_SUCCESS
        ],
        error: [
            COLOR_ERROR, COLOR_ERROR, COLOR_ERROR, COLOR_ERROR, COLOR_ERROR,
            COLOR_ERROR, COLOR_ERROR, COLOR_ERROR, COLOR_ERROR, COLOR_ERROR
        ]
    }
};

export default mantineTheme;