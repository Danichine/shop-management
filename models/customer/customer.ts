import Document, { DocumentType } from "../document"
import { Product } from "../product"
import { STATES } from "../product/product"

type CustomerCreateType = {
    name: string
    email?: string
    telephone?: string
    address?: string
    description?: string
    defaultFeeRate?: number
}

export type CustomerType =
    DocumentType
    & CustomerCreateType
    & {
        totalAvailableProducts: number
        totalDebt: number
        totalDebtProducts: number
        totalSold: number
        totalSoldProducts: number
        totalPaid: number
        productIds: string[]
    }

export default class Customer extends Document {
    name: string
    email: string
    telephone: string
    address: string
    description: string
    defaultFeeRate?: number
    totalAvailableProducts: number
    totalDebt: number
    totalDebtProducts: number
    totalSold: number
    totalSoldProducts: number
    totalPaid: number
    productIds: string[]

    constructor({
        id, name, email, telephone, address,
        description, defaultFeeRate, createdAt,
        updatedAt, totalAvailableProducts, totalDebt,
        totalDebtProducts, totalSold, totalSoldProducts,
        totalPaid, productIds
    }: CustomerType){
        super({ id, createdAt, updatedAt })
        this.name = name;
        this.email = email || "";
        this.telephone = telephone || "";
        this.address = address || "";
        this.description = description || "";
        this.defaultFeeRate = defaultFeeRate;
        this.totalAvailableProducts = typeof totalAvailableProducts === "string" ? parseFloat(totalAvailableProducts) : totalAvailableProducts
        this.totalDebt = typeof totalDebt === "string" ? parseFloat(totalDebt) : totalDebt
        this.totalDebtProducts = typeof totalDebtProducts === "string" ? parseFloat(totalDebtProducts) : totalDebtProducts
        this.totalSold = typeof totalSold === "string" ? parseFloat(totalSold) : totalSold
        this.totalSoldProducts = typeof totalSoldProducts === "string" ? parseFloat(totalSoldProducts) : totalSoldProducts
        this.totalPaid = typeof totalPaid === "string" ? parseFloat(totalPaid) : totalPaid
        this.productIds = productIds
    }

    static create({ name, email, telephone, address, description, defaultFeeRate }: CustomerCreateType) {
        return new Customer({
            id: "", name, email, telephone, address, description, defaultFeeRate,
            totalAvailableProducts: 0, totalDebt: 0, totalDebtProducts: 0,
            totalSold: 0, totalSoldProducts: 0, totalPaid: 0, productIds: []
        })
    }

    deleteProduct(product: Product) {
        this.productIds = this.productIds.filter((item) => item !== product.id);

		if ( product.state === STATES.AVAILABLE ) {
			this.totalAvailableProducts = Math.max( this.totalAvailableProducts - 1, 0);
		}
		if ( product.state === STATES.SOLD ) {
			this.totalDebt = Math.max( this.totalDebt - product.debt, 0 );
		}
    }

    static clone(customer: Customer) {
        return new Customer({
            ...customer
        })
    }

    asObject() {
        return Object.assign({}, this)
    }

    asCsvObject() {
        return {
            name: this.name,
            email: this.email,
            dateCreated: this.dateCreated.toLocaleDateString(),
            dateUpdated: this.dateUpdated.toLocaleDateString(),
            telephone: this.telephone,
            address: this.address,
            description: this.description,
            availableProducts: this.totalAvailableProducts,
            debt: this.totalDebt,
            debtProducts: this.totalDebtProducts,
            sold: this.totalSold,
            soldProducts: this.totalSoldProducts,
            paid: this.totalPaid,
        }
    }
}