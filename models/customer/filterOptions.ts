export type CUSTOMER_FILTER_OPTION = typeof CUSTOMER_FILTER_OPTIONS[keyof typeof CUSTOMER_FILTER_OPTIONS]

export const CUSTOMER_FILTER_OPTIONS = Object.freeze({
    ALL: 'all',
    PRODUCTS: 'products',
    DEBT: 'debt'
});

export const DEFAULT_CUSTOMER_FILTER = CUSTOMER_FILTER_OPTIONS.ALL;