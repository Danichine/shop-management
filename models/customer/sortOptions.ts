export type CUSTOMER_SORT_OPTION = typeof CUSTOMER_SORT_OPTIONS[keyof typeof CUSTOMER_SORT_OPTIONS]

export const CUSTOMER_SORT_OPTIONS = Object.freeze({
    UPDATED: 'updatedAt',
    CREATED: 'createdAt',
    DEBT: 'totalDebt',
    PRODUCTS: 'totalAvailableProducts'
});

export type CustomerSortOption = typeof CUSTOMER_SORT_OPTIONS[keyof typeof CUSTOMER_SORT_OPTIONS]

export const DEFAULT_CUSTOMER_SORT = CUSTOMER_SORT_OPTIONS.UPDATED;