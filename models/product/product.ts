import { Timestamp } from 'firebase/firestore';
import Document, { DocumentType } from '../document';


export const STATES = { AVAILABLE: 'available', SOLD: 'sold', PAID: 'paid' } as const;
export type STATES_OPTIONS = typeof STATES[keyof typeof STATES];

type ProductCreateType = {
    name: string
    price: number
    feeRate: number
    brand?: string
    size?: string
    category?: string
    subcategory?: string
    color?: string
    description?: string
    reservation?: string
    images?: {
        smallImageUrl?: string
        largeImageUrl?: string
    }
    ownerId: string
}

export type ProductType =
    DocumentType &
    ProductCreateType &
    {
        state: typeof STATES[keyof typeof STATES];
        saleDate: Timestamp | null;
    }


export default class Product extends Document {
    name: string;
    price: number;
    feeRate: number;
    brand: string;
    size: string;
    category: string;
    subcategory: string;
    color: string;
    description: string;
    reservation: string;
    state: STATES_OPTIONS;
    images: {
        smallImageUrl: string;
        largeImageUrl: string;
    };
    private saleDate: Timestamp | null;
    ownerId: string;

    constructor({
        id, name, price, feeRate, brand,
        size, category, subcategory, color,
        description, reservation, state, images, saleDate, ownerId, createdAt,
        updatedAt
    }: ProductType){
        super({ id, createdAt, updatedAt })
        this.name = name;
        this.price = typeof price === "string" ? parseFloat(price) : price;
        this.feeRate = typeof feeRate === "string" ? parseFloat(feeRate) : feeRate;;
        this.brand = brand || "";
        this.size = size || "";
        this.category = category || "";
        this.subcategory = subcategory || "";
        this.color = color || "";
        this.description = description || "";
        this.reservation = reservation || "";
        this.state = state;
        this.images = {
            smallImageUrl: images?.smallImageUrl || "",
            largeImageUrl: images?.largeImageUrl || ""
        };
        this.saleDate = !saleDate ? null : new Timestamp(saleDate.seconds, saleDate.nanoseconds);
        this.ownerId = ownerId;
    }

    static create({
        name, price, feeRate, ownerId, brand,
        size, color, category, subcategory, description,
        reservation, images
    }: ProductCreateType) {
        return new Product({
            id: "", name, price, feeRate, ownerId, brand,
            size, color, category, subcategory, description,
            reservation, images,
            state: STATES.AVAILABLE, saleDate: null
        })
    }

    get dateSold() {
        return this.saleDate ? this.saleDate.toDate() : null
    }

    set setSaleDate(newDate: Timestamp | null) {
        this.saleDate = newDate
    }

    get fee() {
        return Math.floor(this.price * (this.feeRate / 100));
    }

    get debt() {
        return this.price - this.fee;
    }

    static clone(product: Product) {
        const { images: { smallImageUrl, largeImageUrl }, saleDate, ...rest } = product;
        return new Product({
            ...rest,
            saleDate: !product.saleDate ? null : new Timestamp(product.saleDate.seconds, product.saleDate.nanoseconds),
            images: {
                smallImageUrl,
                largeImageUrl
            }
        })
    }

    asObject() {
        return Object.assign({}, this)
    }

    asCsvObject() {
        return {
            name: this.name,
            dateCreated: this.dateCreated.toLocaleDateString(),
            dateUpdated: this.dateUpdated.toLocaleDateString(),
            price: this.price,
            feeRate: this.feeRate,
            brand: this.brand,
            size: this.size,
            category: this.category,
            subcategory: this.subcategory,
            color: this.color,
            description: this.description,
            reservation: this.reservation,
            state: this.state,
            hasImage: !!this.images?.smallImageUrl,
            saleDate: this.dateSold ? this.dateSold.toLocaleDateString() : "",
            ownerId: this.ownerId,
        }
    }
}
