export type PRODUCT_SORT_OPTION = typeof PRODUCT_SORT_OPTIONS[keyof typeof PRODUCT_SORT_OPTIONS]

export const PRODUCT_SORT_OPTIONS = Object.freeze({
    UPDATED: 'updatedAt',
    CREATED: 'createdAt',
    PRICE: 'price',
    BRAND: 'brand'
});


export const DEFAULT_PRODUCT_SORT = PRODUCT_SORT_OPTIONS.UPDATED;