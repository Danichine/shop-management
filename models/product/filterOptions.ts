export type PRODUCT_FILTER_OPTION = typeof PRODUCT_FILTER_OPTIONS[keyof typeof PRODUCT_FILTER_OPTIONS]

export const PRODUCT_FILTER_OPTIONS = Object.freeze({
    ALL: 'all',
    AVAILABLE: 'available',
    SOLD: 'sold',
    PAID: 'paid'
});

export const DEFAULT_PRODUCT_FILTER = PRODUCT_FILTER_OPTIONS.ALL;