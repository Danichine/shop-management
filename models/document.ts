import { Timestamp } from "firebase/firestore";

export type DocumentType = {
    id: string;
    createdAt?: Timestamp
    updatedAt?: Timestamp
};

class Document {
    id: string;
    private createdAt: Timestamp;
    private updatedAt: Timestamp;
    
    constructor({ id, createdAt, updatedAt }: DocumentType) {
        this.id = id;
        this.createdAt = !createdAt ? Timestamp.fromDate(new Date()) : new Timestamp(createdAt.seconds, createdAt.nanoseconds);
        this.updatedAt = !updatedAt ? Timestamp.fromDate(new Date()) : new Timestamp(updatedAt.seconds, updatedAt.nanoseconds);
    }

    get dateCreated() {
        return this.createdAt.toDate();
    }

    get dateUpdated() {
        return this.updatedAt.toDate();
    }
};

export default Document;