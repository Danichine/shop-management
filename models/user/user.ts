export type UserType = {
    id: string;
    name: string;
    email: string;
    role: string;
}

export default class User {
    id: string;
    name: string;
    email: string;
    role: string;

    constructor({ id, name, email, role }: UserType) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.role = role;
    }
}