import { Customer, Product, STATES } from "./";
import { throwUnprocessableEntityError } from "../lib/errors";
import { validateProductState } from "../lib/validators";
import { Timestamp } from "firebase/firestore";


interface Updater {
    updateState: ({ product, customer, newState }: { product: Product, customer: Customer, newState: typeof STATES[keyof typeof STATES] }) => ({ product: Product, customer: Customer });
    updatePriceAndFee: ({ product, customer, newPrice, newFeeRate }: { product: Product, customer: Customer, newPrice?: number, newFeeRate?: number }) => ({ product: Product, customer: Customer });
}

export class AvailableStateManager implements Updater {

    updateState({ product, customer, newState }: { product: Product, customer: Customer, newState: typeof STATES[keyof typeof STATES] }) {
        validateProductState(newState);

        const newCustomer = Customer.clone(customer)
        const newProduct = Product.clone(product)

        if (newState === STATES.AVAILABLE) throwUnprocessableEntityError('Product state remains the same.');;

        newCustomer.totalAvailableProducts = Math.max(newCustomer.totalAvailableProducts - 1, 0);
        if (newState === STATES.SOLD) {
            newCustomer.totalDebt += newProduct.debt;
            newCustomer.totalDebtProducts++;
            newCustomer.totalSold += newProduct.price;
            newCustomer.totalSoldProducts++;
            newProduct.setSaleDate = Timestamp.fromDate(new Date());
            newProduct.state = newState;
            return { customer: newCustomer, product: newProduct };
        }
        newCustomer.totalSold += newProduct.price;
        newCustomer.totalSoldProducts++;
        newProduct.setSaleDate = Timestamp.fromDate(new Date());
        newCustomer.totalPaid += newProduct.debt;
        newProduct.state = newState;

        return { customer: newCustomer, product: newProduct };
    }

    // AVAILABLE STATE MANAGER HAS NO USE FOR THIS METHOD BUT IS REQUIRED BY THE INTERFACE
    updatePriceAndFee({ product, customer, newPrice, newFeeRate }: { product: Product, customer: Customer, newPrice?: number, newFeeRate?: number }) {
        const newCustomer = Customer.clone(customer)
        const newProduct = Product.clone(product)

        return { customer: newCustomer, product: newProduct }
    }
}

export class SoldStateManager implements Updater {

    updateState({ product, customer, newState }: { product: Product, customer: Customer, newState: typeof STATES[keyof typeof STATES] }) {
        validateProductState(newState);

        const newCustomer = Customer.clone(customer)
        const newProduct = Product.clone(product)
    
        if (newState === STATES.SOLD) throwUnprocessableEntityError('Product state remains the same.');;

        newCustomer.totalDebt = Math.max(newCustomer.totalDebt - newProduct.debt, 0);
        newCustomer.totalDebtProducts = Math.max(newCustomer.totalDebtProducts - 1, 0);
        if (newState === STATES.AVAILABLE) {
            newCustomer.totalAvailableProducts++;
            newProduct.setSaleDate = null;
            newCustomer.totalSold = Math.max(newCustomer.totalSold - newProduct.price, 0);
            newCustomer.totalSoldProducts = Math.max(newCustomer.totalSoldProducts - 1, 0);
            newProduct.state = newState;
            return { customer: newCustomer, product: newProduct };
        }
        newCustomer.totalPaid += newProduct.debt;
        newProduct.state = newState;

        return { customer: newCustomer, product: newProduct }
    }

    updatePriceAndFee({ product, customer, newPrice: price, newFeeRate: feeRate }: { product: Product, customer: Customer, newPrice?: number, newFeeRate?: number }) {
        const newCustomer = Customer.clone(customer)
        const newProduct = Product.clone(product)

        const newPrice = price !== undefined ? price : product.price;
        const newFeeRate = feeRate !== undefined ? feeRate : product.feeRate;
        const originalProductDebt = product.debt;

        newProduct.price = newPrice;
        newProduct.feeRate = newFeeRate;

        const newProductDebt = newProduct.debt;
        newCustomer.totalDebt = Math.max(customer.totalDebt - originalProductDebt + newProductDebt, 0);
        newCustomer.totalSold = Math.max(customer.totalSold - product.price + newPrice, 0);

        return { customer: newCustomer, product: newProduct }
    }
}

export class PaidStateManager implements Updater {

    updateState({ product, customer, newState }: { product: Product, customer: Customer, newState: typeof STATES[keyof typeof STATES] }) {
        validateProductState(newState);

        const newCustomer = Customer.clone(customer)
        const newProduct = Product.clone(product)

        if (newState === STATES.PAID) throwUnprocessableEntityError('Product state remains the same.');;
    
        newCustomer.totalPaid = Math.max(newCustomer.totalPaid - newProduct.debt, 0);
        if (newState === STATES.SOLD) {
            newCustomer.totalDebt += newProduct.debt;
            newCustomer.totalDebtProducts++;
            newProduct.state = newState;

            return { customer: newCustomer, product: newProduct };
        }
        newProduct.setSaleDate = null;
        newCustomer.totalSold = Math.max(newCustomer.totalSold - newProduct.price, 0);
        newCustomer.totalSoldProducts = Math.max(newCustomer.totalSoldProducts - 1, 0);
        newCustomer.totalAvailableProducts++;
        newProduct.state = newState;

        return { customer: newCustomer, product: newProduct }
    }

    updatePriceAndFee({ product, customer, newPrice: price, newFeeRate: feeRate }: { product: Product, customer: Customer, newPrice?: number, newFeeRate?: number }) {
        const newCustomer = Customer.clone(customer)
        const newProduct = Product.clone(product)

        const newPrice = price !== undefined ? price : product.price;
        const newFeeRate = feeRate !== undefined ? feeRate : product.feeRate;
        const originalProductDebt = product.debt;

        newProduct.price = newPrice;
        newProduct.feeRate = newFeeRate;

        const newProductDebt = newProduct.debt;
        newCustomer.totalPaid = Math.max(newCustomer.totalPaid - originalProductDebt + newProductDebt, 0);
        newCustomer.totalSold = Math.max(newCustomer.totalSold - product.price + newPrice, 0);
        
        return { customer: newCustomer, product: newProduct }
    }
}

export const stateManagerFactory = (product: Product) => {
    const currentState = product.state;
    validateProductState(currentState);

    if (currentState === STATES.AVAILABLE) return new AvailableStateManager();
    return currentState === STATES.SOLD
        ? new SoldStateManager()
        : new PaidStateManager()
};