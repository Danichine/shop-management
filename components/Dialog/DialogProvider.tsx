import { cloneElement, FC, ReactElement } from 'react';
import Dialog from './Dialog';
import useDialog from '@/lib/hooks/useDialog';

type props = {
    children: ReactElement
}


const DialogProvider: FC<props> = ({ children }) => {
	const { showDialog, onShowDialog, dialogProps } = useDialog();
    const Component = cloneElement( children, { onShowDialog } );

	return (
        <>
            {showDialog &&
                <Dialog {...dialogProps} />
            }
            {Component}
        </>
	)
};

export default DialogProvider;
