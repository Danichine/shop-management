import type { FC } from 'react';
import { Text } from '@mantine/core';
import styles from './Dialog.module.scss';
import classNames from 'classnames';
import { Button } from '../UI';

export type dialogProps = {
    title?: string;
    subtitle?: string;
    confirmLabel?: string;
    cancelLabel?: string;
	confirmAction: () => void;
	cancelAction?: () => void;
	dangerAction?: boolean;
};


const Dialog: FC<dialogProps> = ({
	subtitle, title = 'Are you sure?', confirmLabel = 'Confirm', cancelLabel = 'Cancel',
	confirmAction, cancelAction, dangerAction = false
}) => {
	const onConfirmClicked = () => {
		confirmAction();
	}

	const onCancelClicked = () => {
		if ( !cancelAction ) return;
		cancelAction();
	};

	return (
		<div className={styles.Dialog}>
			<div className={styles.container}>
				<Text className={styles.title}>{title}</Text>
				{subtitle && <Text className={styles.subtitle}>{subtitle}</Text>}
				<div className={styles.buttonsContainer}>
					<Button onClick={onCancelClicked} label={cancelLabel} />
					<Button
						className={classNames( { [styles.dangerAction]: dangerAction } )}
						onClick={onConfirmClicked}
						label={confirmLabel}
						variant='danger'
					/>
				</div>
			</div>
		</div>
	);
};

export default Dialog;
