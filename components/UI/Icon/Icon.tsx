import { FC } from 'react';
import styles from './Icon.module.scss';
import classNames from 'classnames';
import {
    Search, Feather, ArrowLeft, Edit3,
    XCircle, Plus, ChevronLeft, ChevronRight,
    ChevronsLeft, ChevronsRight, ChevronDown,
    Table
} from 'react-feather';

export const ICONS = {
    search: Search,
    arrowLeft: ArrowLeft,
    edit: Edit3,
    default: Feather,
    xCircle: XCircle,
    plus: Plus,
    chevronLeft: ChevronLeft,
    chevronsLeft: ChevronsLeft,
    chevronRight: ChevronRight,
    chevronsRight: ChevronsRight,
    chevronDown: ChevronDown,
    table: Table
};

export type availableIcon = keyof typeof ICONS;

type props = {
    className?: string;
    iconName: availableIcon;
    withBackground?: boolean;
};

const Icon: FC<props> = ( {
    className, iconName, withBackground = false
} ) => {
    const IconComponent = ICONS[ iconName ];
    const mergedClassnames = classNames(
        styles.Icon,
        className,
        {
            [styles.withBackground]: withBackground
        }
    )

    return (
        <div className={mergedClassnames}>
            <IconComponent />
        </div>
    );
};

export default Icon;