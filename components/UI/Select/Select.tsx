import { FC, useEffect, useRef, useState } from 'react';
import styles from './Select.module.scss';
import classNames from 'classnames';
import Icon from '../Icon/Icon';
import useClickOutside from '@/lib/hooks/useClickOutside';

type selectOption = {
    value: string;
    label: string;
}

type props = {
    className?: string;
    onChange: ( value: string ) => void;
    options: selectOption[];
    selectedValue?: string | number;
    defaultValue?: string | number;
};


const Select: FC<props> = ( {
    className, onChange, selectedValue, defaultValue,
    options = []
} ) => {
    const selectRef = useRef( null );

    useClickOutside( selectRef, () => setShowOptions( false ));

    const [ showOptions, setShowOptions ] = useState( false );
    const [ animateOpen, setAnimateOpen ] = useState( false );

    const onOptionSelected = ( value: string ) => {
        onChange(value);
        setShowOptions( false );
    }

    useEffect(() => {
        setTimeout(() => {
            setAnimateOpen( showOptions )
        }, 0 );
    }, [ showOptions ])

    const optionsList = options.map( ( o: selectOption ) => {
        const isSelected = selectedValue === o.value;
        return (
            <div
                className={isSelected ? styles.selected : undefined}
                key={o.value}
                onClick={() => onOptionSelected( o.value )}
            >
                <h3>
                    {o.label}
                </h3>
            </div>
        )
    });

    const defaultLabel = options.find( ( o: selectOption ) => o.value === defaultValue )?.label || options[ 0 ].label;
    const selectedLabel = options.find( ( o: selectOption ) => o.value === selectedValue )?.label || defaultLabel;

    return (
        <div className={classNames(styles.Select, className)} ref={selectRef}>
            <div className={styles.selectionContainer} onClick={() => setShowOptions( !showOptions )}>
                <h3>{selectedLabel.toString()}</h3>
                <Icon className={classNames({[styles.optionsOpen]: showOptions})} iconName='chevronDown'/>
            </div>
            {showOptions &&
                <div className={classNames(styles.optionsContainer, {[styles.optionsOpen]: animateOpen})}>
                    {optionsList}
                </div>}
        </div>
    );
};

export default Select;