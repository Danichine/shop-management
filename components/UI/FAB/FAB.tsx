import { FC, ReactNode } from 'react';
import styles from './FAB.module.scss';
import classNames from 'classnames';
import { ICONS } from '../Icon/Icon';
import { Icon } from '..';

type props = {
    className?: string;
    children?: ReactNode;
    onClick: () => void;
    iconName?: keyof typeof ICONS;
    relativePosition?: boolean
};

const FAB: FC<props> = ( {
    className, onClick, children,
    iconName, relativePosition
} ) => {
    return (
        <button
            className={classNames(styles.FAB, className, { [styles.relative]: !!relativePosition })}
            onClick={onClick}
        >
            {children && children}
            {iconName &&
                <Icon className={styles.icon} iconName={iconName} />}
        </button>
    );
};

export default FAB;