import { FC, SyntheticEvent, useState } from 'react';
import classNames from 'classnames';
import styles from './Search.module.scss';
import { TextInput, IconButton } from '..';

type props = {
    className?: string;
    onSearch: ( value: string ) => void;
};

const Search: FC<props> = ( {
    className, onSearch
} ) => {
    const [ searchValue, setSearchValue ] = useState('');

    const onSearchChanged = ( event: SyntheticEvent ) => {
        const { value } = (event.target) as HTMLInputElement;
        setSearchValue( value )
        if ( value === '' ) {
            onSearch('');
        }
    }

    const onResetClicked = () => {
        setSearchValue('');
        onSearch('');
    };

    return (
    <div className={classNames(styles.Search, className)}>
        <div className={styles.textInputContainer}>
            <TextInput
                className={styles.textInput}
                value={searchValue}
                onChange={onSearchChanged}
            />
            <IconButton
                className={classNames(
                    styles.resetSearchButton,
                    { [styles.visible]: searchValue }
                )}
                iconName='xCircle'
                withBackground
                onClick={onResetClicked}
            />
        </div>
        <IconButton
            className={styles.searchButton}
            iconName='search'
            withBackground
            disabled={!searchValue}
            onClick={() => onSearch( searchValue )}
        />
    </div>
)};

export default Search;