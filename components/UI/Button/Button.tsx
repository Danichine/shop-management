import { FC } from 'react';
import classNames from 'classnames';
import styles from './Button.module.scss';


type props = {
    className?: string;
    onClick?: () => void;
    label: string|number;
    disabled?: boolean;
    variant?: 'regular' | 'subtle' | 'danger';
    type?: "button" | "submit" | "reset" | undefined;
};

const Button: FC<props> = ( {
    className, onClick, disabled,
    label, type, variant = 'regular'
} ) => {
    return (
        <button
            className={classNames(
                styles.Button,
                { [styles[variant]]: variant },
                { [styles.disabled]: disabled },
                className
            )}
            onClick={onClick}
            disabled={disabled}
            type={type}
        >
            <h3 className={styles.label}>{label}</h3>
        </button>
    );
};

export default Button;