import classNames from 'classnames';
import { FC } from 'react';
import { COLOR_GREY_DARK, COLOR_GREY_ULTRA_DARK, COLOR_WHITE } from '@/styles/variables';
import styles from './Loader.module.scss';

const COLORS = {
    white: COLOR_WHITE,
    grey: COLOR_GREY_DARK,
    black: COLOR_GREY_ULTRA_DARK
}

type props = {
    className?: string;
    color?: 'white'|'grey'|'black'
}

const Loader: FC<props> = ({ className, color = 'white' }) => (
    <div className={classNames(styles.Loader, className)}>
        <div className={styles.ldsRing}>
            <div style={{borderColor: `${COLORS[ color ]} transparent transparent transparent`}}></div>
            <div style={{borderColor: `${COLORS[ color ]} transparent transparent transparent`}}></div>
            <div style={{borderColor: `${COLORS[ color ]} transparent transparent transparent`}}></div>
            <div style={{borderColor: `${COLORS[ color ]} transparent transparent transparent`}}></div>
        </div>
    </div>
);

export default Loader;