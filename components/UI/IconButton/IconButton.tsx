import { FC } from 'react';
import styles from './IconButton.module.scss';
import Icon, { availableIcon } from '../Icon/Icon';
import classNames from 'classnames';

type props = {
    className?: string;
    iconName: availableIcon;
    onClick: () => void;
    withBackground?: boolean;
    disabled?: boolean;
};


const IconButton: FC<props> = ( {
    className, iconName, onClick,
    withBackground, disabled = false
} ) => {
    return (
        <button
            className={classNames(
                styles.IconButton,
                { [styles.disabled]: disabled },
                className
            )}
            onClick={onClick}
            disabled={disabled}
        >
            <Icon
                iconName={iconName}
                withBackground={withBackground}
            />
        </button>
    );
};

export default IconButton;