import { ChangeEvent, FC } from 'react';
import classNames from 'classnames';
import styles from './TextInput.module.scss';

type props = {
    className?: string;
    onChange?: ( event: ChangeEvent<HTMLInputElement> ) => void;
    value?: string|number|undefined;
    label?: string;
    disabled?: boolean;
    type?: 'text' | 'number' | 'password' | 'file' | 'email' | 'tel' | undefined;
    required?: boolean;
    error?: string|null;
    saveErrorSpace?: boolean;
    placeholder?: string;
    max?: number;
    min?: number;
};

const TextInput: FC<props> = ( {
    className, onChange, label,
    disabled, value, error, placeholder,
    saveErrorSpace = false,
    max, min, type = 'text', required = false
} ) => (
        <div className={
            classNames(
                styles.TextInput, 
                { [styles.withError]: !!error },
                { [styles.disabled]: disabled },
                className
            )}
        >
            {label &&
                <div className={styles.labelContainer}>
                    <label className={styles.label}>{label}</label>
                    {required && <span className={styles.required}>*</span>}
                </div>}
            <input
                className={styles.inputElement}
                type={type}
                value={value}
                onChange={onChange}
                disabled={disabled}
                placeholder={placeholder}
                max={max}
                min={min}
            />
            {( error || saveErrorSpace ) && <p className={styles.errorMessage}>{error}</p>}
        </div>
    );

export default TextInput;