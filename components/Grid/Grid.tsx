import { FC, ReactNode } from 'react';
import styles from './Grid.module.scss';
import classNames from 'classnames';

const MEDIUM = 'm';
const LARGE = 'l';

type objectSizes = {
    m: number;
    l: number;
};

const GRID_ITEM_SIZES: objectSizes = Object.freeze({
    [MEDIUM]: 42,
    [LARGE]: 45
});

type props = {
    className?: string;
    children: ReactNode;
    itemSize?: keyof objectSizes;
};

const Grid: FC<props> = ( { className, children, itemSize = MEDIUM } ) => (
    <div className={classNames(styles.Grid, className)}>
        <ul
            className={styles.gridContainer}
            style={{ gridTemplateColumns: `repeat(auto-fit, minmax(min-content, ${GRID_ITEM_SIZES[itemSize]}rem)`}}
        >
            {children}
        </ul>
    </div>
);

export default Grid;