import { FC } from 'react';
import styles from './EmptyListMessage.module.scss';
import classNames from 'classnames';

type props = {
    className?: string;
    message: string;
    secondaryMessage?: string;
    applySecondary?: boolean;
};


const EmptyListMessage: FC<props> = ({
    className, message, secondaryMessage,
    applySecondary
}) => (
    <div className={classNames(styles.EmptyListMessage, className)}>
        <h2>{applySecondary ? secondaryMessage : message}</h2>
    </div>
);

export default EmptyListMessage;