import { FC, useState } from 'react';
import { Modal, Text } from '@mantine/core';
import useForm from '@/lib/hooks/useForm';
import styles from './UpdateCustomerModal.module.scss';
import { showNotification } from '@mantine/notifications';
import { Customer } from '@/models';
import { useRouter } from 'next/router';
import type { dialogProps } from '../Dialog/Dialog';
import { TextInput, Button } from '../UI';
import { createCustomer, deleteCustomer, updateCustomer } from '@/services';


type props = {
    isOpen: boolean;
    onClose: () => void;
    editableCustomer?: Customer;
    onUpdateCustomer: ( customer: any ) => void;
    showDialog?: ( dialogProps: dialogProps ) => void;
};

const UpdateCustomerModal: FC<props> = ({
    isOpen, onClose, onUpdateCustomer, editableCustomer,
    showDialog
}) => {
    const router = useRouter();

    const [ isLoading, setIsLoading ] = useState( false );

    const isEdit = !!editableCustomer;

    const {
        getInputProps, values, onSubmit, reset,
        wasEdited
    } = useForm({
        initialValues: {
            name: editableCustomer?.name || '',
            email: editableCustomer?.email || '',
            telephone: editableCustomer?.telephone || '',
            address: editableCustomer?.address || '',
            defaultFeeRate: editableCustomer?.defaultFeeRate || '',
            description: editableCustomer?.description || ''
        },
        validate: {
            name: value => value.length > 2 && value.length < 50 ? null : 'Name should be between 3 and 50 characters',
            email: value => (!value || (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)) ? null : 'Invalid email'),
            telephone: value => !value || value.length > 7 && value.length < 50 ? null : 'Telephone should be between 8 and 15 characters',
            address: value => !value || value.length < 200 ? null : 'Address should be between 1 and 200 characters',
            description: value => !value || value.length < 200 ? null : 'Description should be between 1 and 200 characters',
            defaultFeeRate: value => !value || value >= 0 && value <= 100 ? null : 'Default Fee Rate should be between 0 and 100'
        }
    });

    const onDeleteCustomerClicked = async () => {
        if (!editableCustomer) return
        
        const { id } = editableCustomer

        try {
            setIsLoading( true );

            await deleteCustomer(id)

            showNotification({
                title: 'Success!',
                message: 'Customer deleted!',
                color: 'success'
            });
            
            router.push( '/customers' );
            reset();
            onClose();
        }
        catch( error: any ) {
            if ( error?.statusCode === 401 ) {
                router.push( '/login' );
            }
            showNotification({
                title: 'Failed!',
                message: error.message || 'Customer wasn\'t deleted. Try again later',
                color: 'error'
            });
        }
        finally {
            setIsLoading( false );
        }
    }

    
    const handleSubmit = onSubmit( async ( finalValues: typeof values) => {
        if (isEdit && !editableCustomer) return;
        
        const { id } = editableCustomer || {};

        try {
            setIsLoading( true );

            const request = isEdit ? () => updateCustomer(id || "", finalValues) : () => createCustomer(finalValues)
            
            const { customer } = await request();
            onUpdateCustomer( customer );
            
            reset();
            showNotification({
                title: 'Success!',
                message: `Customer ${isEdit ?  'edited' : 'created'}.`,
                color: 'success'
            });
        }
        catch( error: any ) {
            showNotification({
                title: 'Failed!',
                message: error.message || `Customer wasn\'t ${isEdit ?  'edited' : 'created'}.`,
                color: 'error'
            });
        }
        finally {
            setIsLoading( false );
        }
    });
    
    const onShowDialog = () => {
        if ( !showDialog ) return;
        showDialog({
            title: 'Delete Customer',
            subtitle: 'Are you sure? This cannot be undone. All products asociated with this customer will be lost.',
            confirmLabel: 'Accept',
            cancelLabel: 'Cancel',
            confirmAction: onDeleteCustomerClicked,
            dangerAction: true
        })
    };

    const onModalClosed = () => {
        reset();
        onClose();
    }
    
    const title = isEdit ? 'Edit Customer' :'Create New Customer';

    return (
        <Modal
            opened={isOpen}
            onClose={onModalClosed}
            styles={{
                modal: { width: 'auto' },
                header: { marginBottom: 0 }
            }}
        >
            <div className={styles.UpdateCustomerModal}>
                <Text className={styles.title}>{title}</Text>
                <form onSubmit={handleSubmit} className={styles.form}>
                    <div className={styles.textInputsContainer}>
                        <div className={styles.sideContainer}>
                            <TextInput
                                className={styles.textInput}
                                label="Name"
                                placeholder='E.g: Eduardo Berizzo'
                                required
                                {...getInputProps( 'name' )}
                            />
                            <TextInput
                                className={styles.textInput}
                                label="Email"
                                type='email'
                                placeholder='E.g: toto@berizzo.com'
                                {...getInputProps( 'email' )}
                            />
                            <TextInput
                                className={styles.textInput}
                                label="Telephone"
                                type='tel'
                                placeholder='E.g: 1142424242'
                                {...getInputProps( 'telephone' )}
                            />
                        </div>
                        <div className={styles.sideContainer}>
                            <TextInput
                                className={styles.textInput}
                                label="Address"
                                placeholder='E.g: 123 Fake St., Las Vegas, Nevada'
                                {...getInputProps( 'address' )}
                            />
                            <TextInput
                                className={styles.textInput}
                                label="Description"
                                placeholder='E.g: The guy who bought all my products.'
                                {...getInputProps( 'description' )}
                            />
                            <TextInput
                                className={styles.textInput}
                                label="Default Fee Rate"
                                type='number'
                                placeholder='E.g: 55 (percentage)'
                                {...getInputProps( 'defaultFeeRate' )}
                            />
                        </div>
                    </div>
                    <div className={styles.buttonsContainer}>
                        <Button
                            type='submit'
                            disabled={isLoading || !wasEdited}
                            label={isLoading ? 'Saving...' : 'Save'}
                        />
                        {isEdit &&
                            <Button
                                className={styles.deleteButton}
                                type='button'
                                disabled={isLoading}
                                onClick={onShowDialog}
                                label='Delete'
                                variant='danger'
                            />}
                    </div>
                </form>
            </div>
        </Modal>
    )
};

export default UpdateCustomerModal;