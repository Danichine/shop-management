import { FC } from 'react';
import styles from './Toolbar.module.scss';
import classNames from 'classnames';
import { Select, Search } from '../UI';


type options = {
    value: string;
    label: string;
};

type props = {
    className?: string;
    sortValue: string;
    sortOptions: options[];
    onSortValueChange: (value: string) => void;
    filterValue: string;
    filterOptions: options[];
    onFilterValueChange: (value: string) => void;
    onSearch: (value: string) => void;
};


const Toolbar: FC<props> = ({
    className, sortValue, sortOptions, onSortValueChange,
    filterValue, filterOptions, onFilterValueChange,
    onSearch
}) => (
    <div className={classNames(styles.Toolbar, className)}>
        <div className={styles.search}>
            <h3 className={styles.label}>Search:</h3>
            <Search
                onSearch={onSearch}
            />
        </div>
        <div className={styles.select}>
            <h3 className={styles.label}>Sort:</h3>
            <Select
                className={styles.sortSelect}
                selectedValue={sortValue}
                options={sortOptions}
                onChange={onSortValueChange}
            />
        </div>
        <div className={styles.select}>
            <h3 className={styles.label}>Filter:</h3>
            <Select
                className={styles.filterSelect}
                selectedValue={filterValue}
                options={filterOptions}
                onChange={onFilterValueChange}
            />
        </div>
    </div>
);

export default Toolbar;