import { FC } from 'react';
import classNames from 'classnames';
import styles from './Pagination.module.scss';
import { IconButton, Select } from '../UI';


export const DEFAULT_PER_PAGE_LIMIT = 2;

export const PER_PAGE_OPTIONS = [
    { value: '1', label: '1' },
    { value: '2', label: '2' },
    { value: '5', label: '5' },
    { value: '10', label: '10' },
    { value: '20', label: '20' },
    { value: '50', label: '50' },
    { value: '100', label: '100' }
]

export const PAGE_OPTIONS = Object.freeze({
    FIRST: 'first',
    NEXT: 'next',
    PREVIOUS: 'previous',
    LAST: 'last'
});
export type PageOption = typeof PAGE_OPTIONS[keyof typeof PAGE_OPTIONS];

type props = {
    onPageSet: (pageOption: PageOption) => void;
    onLimitSet: (value: number) => void;
    limit: number;
    hasNextPage: boolean;
    hasPreviousPage: boolean;
    contrastMode?: boolean;
};

type arrowTypes = 'chevronLeft' | 'chevronRight' | 'chevronsLeft' | 'chevronsRight';
const createArrowIconButton = (iconName: arrowTypes, isDisabled: boolean, onClickHandler: () => void, contrastMode?: boolean, ) => (
    <IconButton
        className={classNames(
            styles.paginationButton,
            {
                [styles.contrastMode]: contrastMode && !isDisabled,
                [styles.disabled]: isDisabled
            }
    )}
        iconName={iconName}
        disabled={isDisabled}
        onClick={onClickHandler}
    />
);

const Pagination: FC<props> = ({
    onPageSet, onLimitSet, limit = DEFAULT_PER_PAGE_LIMIT,
    hasNextPage, hasPreviousPage, contrastMode
}) => {

    const setLimitAndReset = (value: string) => {
        onPageSet(PAGE_OPTIONS.FIRST);
        onLimitSet(parseInt(value));
    };
    const stringLimit = limit.toString()

    return (
        <div className={styles.Pagination}>
            <div className={styles.buttonsContainer}>
                {createArrowIconButton('chevronsLeft', !hasPreviousPage, () => onPageSet(PAGE_OPTIONS.FIRST), contrastMode,)}
                {createArrowIconButton('chevronLeft', !hasPreviousPage, () => onPageSet(PAGE_OPTIONS.PREVIOUS), contrastMode,)}
                {createArrowIconButton('chevronRight', !hasNextPage, () => onPageSet(PAGE_OPTIONS.NEXT), contrastMode,)}
                {createArrowIconButton('chevronsRight', !hasNextPage, () => onPageSet(PAGE_OPTIONS.LAST), contrastMode,)}
            </div>
            <h3 className={styles.perPageTitle}>Per page:</h3>
            <Select
                selectedValue={stringLimit}
                options={PER_PAGE_OPTIONS}
                onChange={setLimitAndReset}
            />
        </div>
);
};

export default Pagination;