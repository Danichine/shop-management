import { FC, useCallback, useEffect, useState } from 'react';
import { Modal, Text } from '@mantine/core';
import styles from './UpdateProductStateModal.module.scss';
import { showNotification } from '@mantine/notifications';
import { Product, STATES } from '@/models';
import { capitalizeFirstLetter } from '@/lib/utils';
import { Button, Select } from '../UI';
import { useRouter } from 'next/router';
import { updateProductState } from '@/services';

type props = {
    isOpen: boolean;
    onClose: () => void;
    product?: Product;
    onUpdateProduct: (product: Product) => void;
};

const UpdateProductStateModal: FC<props> = ({
    isOpen, onClose, onUpdateProduct, product
}) => {
    const [ isLoading, setIsLoading ] = useState(false);
    const [ selectedState, setSelectedState ] = useState<string|undefined>(undefined);

    const router = useRouter();

    const setInitialProductState = useCallback(() => {
        if (!product) return;
        setSelectedState(product.state);
    }, [ product ])

    useEffect(() => {
        setInitialProductState()
    },[ setInitialProductState ])

    const wasEdited = product && selectedState && product.state !== selectedState;

    const onSaveClicked = async () => {
        if (!product) return
        
        try {
            setIsLoading(true);
            
            const { product: updatedProduct } = await updateProductState(product.id, { state: selectedState });
            onUpdateProduct(updatedProduct);
    
            showNotification({
                title: 'Success!',
                message: 'Product state updated.',
                color: 'success'
            });
        }
        catch(error: any) {

            if (error?.statusCode === 401) {
                router.push('/login');
            }
            showNotification({
                title: 'Failed!',
                message: error.message || 'Product state wasn\'t updated.',
                color: 'error'
            });
        }
        finally {
            setIsLoading(false);
        }
    };
    
    return (
        <Modal
            opened={isOpen}
            onClose={onClose}
            styles={{
                inner: { alignItems: 'center', marginTop: '-10%' },
                modal: { width: 'auto' },
                header: { marginBottom: 0 }
            }}
        >
            <div className={styles.UpdateProductStateModal}>
                <Text className={styles.title}>Update Product State</Text>
                <div className={styles.stateUpdateContainer}>
                    <Text className={styles.title}>{capitalizeFirstLetter(product?.state)}</Text>
                    <Text className={styles.doubleArrows}>{'>>'}</Text>
                    <Select
                        className={styles.select}
                        selectedValue={selectedState}
                        options={[
                            { value: STATES.AVAILABLE, label: 'Available' },
                            { value: STATES.SOLD, label: 'Sold' },
                            { value: STATES.PAID, label: 'Paid' }
                        ]}
                        onChange={(value: string) => setSelectedState(value)}
                    />
                </div>
                <Button
                    onClick={onSaveClicked}
                    disabled={isLoading || !wasEdited}
                    label={isLoading ? 'Saving...' : 'Save'}
                />
            </div>
        </Modal>
)
};

export default UpdateProductStateModal;