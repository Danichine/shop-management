import { FC, useState } from 'react';
import { Modal, Text } from '@mantine/core';
import useForm from '@/lib/hooks/useForm';
import styles from './UpdateProductModal.module.scss';
import { showNotification } from '@mantine/notifications';
import { Product } from '@/models';
import type { dialogProps } from '../Dialog/Dialog';
import { stringToDate } from '@/lib/utils';
import useFetchCustomer from '@/lib/hooks/useFetchCustomer';
import Link from 'next/link';
import { TextInput, Button } from '../UI';
import { useRouter } from 'next/router';
import { createProduct, deleteProduct, updateProduct } from '@/services';


type props = {
    isOpen: boolean;
    onClose: () => void;
    editableProduct?: Product;
    onUpdateProduct: (product: Product) => void;
    onDeleteProduct: (product: Product) => void;
    showDialog?: (dialogProps: dialogProps) => void;
    ownerId: string;
    defaultFeeRate?: number;
};

type formValues = {
    [key: string]: string | Blob;
}

type formFileValues = {
    [key: string]: Blob;
}

const UpdateProductModal: FC<props> = ({
    isOpen, onClose, onUpdateProduct, editableProduct,
    showDialog, ownerId, onDeleteProduct, defaultFeeRate
}) => {
    const [ isLoading, setIsLoading ] = useState(false);

    const router = useRouter();

    const isEdit = !!editableProduct;

    const {
        getInputProps, onSubmit, reset,
        wasEdited, getFileInputProps
    } = useForm({
        initialValues: {
            name: editableProduct?.name || '',
            price: editableProduct?.price || '',
            feeRate: editableProduct?.feeRate || defaultFeeRate || '',
            brand: editableProduct?.brand || '',
            size: editableProduct?.brand || '',
            category: editableProduct?.category || '',
            subcategory: editableProduct?.subcategory || '',
            color: editableProduct?.color || '',
            description: editableProduct?.description || '',
            reservation: editableProduct?.reservation || '',
            imageUrl: editableProduct?.images?.smallImageUrl || ''
        },
        validate: {
            name: value => value.length > 2 && value.length < 50 ? null : 'Name should be between 3 and 50 characters',
            price: value => value >= 0 && value <= 999999 ? null : 'Price should be between 0 and 999999',
            feeRate: value => value >= 0 && value <= 100 ? null : 'Fee Rate should be between 0 and 100',
            brand: value => !value || value.length < 50 ? null : 'Brand should be up to 50 characters',
            size: value => !value || value.length < 50 ? null : 'Size should be up to 50 characters',
            category: value => !value || value.length < 50 ? null : 'Category should be up to 50 characters',
            subcategory: value => !value || value.length < 50 ? null : 'Subcategory should be up to 50 characters',
            color: value => !value || value.length < 50 ? null : 'Color should be up to 50 characters',
            description: value => !value || value.length < 200 ? null : 'Description should be up to 200 characters',
            reservation: value => !value || value.length < 200 ? null : 'Reservation should be up to 200 characters'
        }
    });

    const { customer: owner } = useFetchCustomer(editableProduct?.ownerId);

    const onDeleteProductClicked = async () => {
        try {
            if (!editableProduct) return;
            const { id } = editableProduct;

            setIsLoading(true);

            await deleteProduct(id)

            showNotification({
                title: 'Success!',
                message: 'Product deleted!',
                color: 'success'
            });

            onDeleteProduct(editableProduct);
            reset();
        }
        catch(error: any) {
            if (error?.statusCode === 401) {
                router.push('/login');
            }
            showNotification({
                title: 'Failed!',
                message: error.message || 'Product wasn\'t deleted. Try again later',
                color: 'error'
            });
        }
        finally {
            setIsLoading(false);
        }
    }

    const handleSubmit = onSubmit(async (finalValues: formValues, fileValues: formFileValues) => {
        if (isEdit && !editableProduct) return;
        
        const { id } = editableProduct || {};

        try {
            setIsLoading(true);

            const request = isEdit ? (values: any) => updateProduct(id || "", values) : (values: any) => createProduct(values);

            const formData = new FormData();
            formData.append('name', finalValues.name);
            formData.append('price', finalValues.price);
            formData.append('feeRate', finalValues.feeRate);
            formData.append('brand', finalValues.brand);
            formData.append('size', finalValues.size);
            formData.append('category', finalValues.category);
            formData.append('subcategory', finalValues.subcategory);
            formData.append('color', finalValues.color);
            formData.append('description', finalValues.description);
            formData.append('reservation', finalValues.reservation);
            formData.append('image', fileValues.image);
            formData.append('customerId', ownerId || '');
            
            const { product } = await request(formData);
            onUpdateProduct(product);
            
            reset();
            showNotification({
                title: 'Success!',
                message: `Product ${isEdit ?  'edited' : 'created'}.`,
                color: 'success'
            });
        }
        catch(error: any) {
            showNotification({
                title: 'Failed!',
                message: error.message || `Product wasn\'t ${isEdit ?  'edited' : 'created'}.`,
                color: 'error'
            });
        }
        finally {
            setIsLoading(false);
        }
    });
    
    const onShowDialog = () => {
        if (!showDialog) return;
        showDialog({
            title: 'Delete Product',
            subtitle: 'Are you sure? This cannot be undone.',
            confirmLabel: 'Accept',
            cancelLabel: 'Cancel',
            confirmAction: onDeleteProductClicked,
            dangerAction: true
        })
    };
    
    const onModalClosed = () => {
        reset();
        onClose();
    }
    
    const title = isEdit ? 'Edit Product' :'Create New Product';
    const {
        dateUpdated, dateCreated, state, dateSold
    } = editableProduct || {};
    
    return (
        <Modal
            opened={isOpen}
            onClose={onModalClosed}
            styles={{
                modal: { width: 'auto' },
                header: { marginBottom: 0 }
            }}
        >
            <div className={styles.UpdateProductModal}>
                <Text className={styles.title}>{title}</Text>
                {isEdit &&
                    <Text className={styles.lastUpdated}>{`Last updated: ${dateUpdated?.toLocaleDateString()}`}</Text>}
                <form onSubmit={handleSubmit} className={styles.form}>
                    <div className={styles.textInputsContainer}>
                        {owner && 
                            <div>
                                <Text mb={10} size='sm' weight={500}>Owner</Text>
                                <Link href={`/customers/${ownerId}`}>
                                    <a>
                                        <h3 className={styles.owner}>{owner.name}</h3>
                                    </a>
                                </Link>
                            </div>}
                        {isEdit &&
                            <>
                                <TextInput
                                    className={styles.textInput}
                                    label="Date Created"
                                    value={dateCreated?.toLocaleDateString()}
                                    saveErrorSpace
                                    disabled
                                />
                                <TextInput
                                    className={styles.textInput}
                                    label="State"
                                    value={state}
                                    saveErrorSpace
                                    disabled
                                />
                                <TextInput
                                    className={styles.textInput}
                                    label="Sale date"
                                    value={stringToDate(dateSold ? dateSold.toString() : undefined, '-')}
                                    saveErrorSpace
                                    disabled
                                />
                            </>}
                        <TextInput
                            className={styles.textInput}
                            label="Name"
                            placeholder='E.g: Short sleeve shirt'
                            required
                            {...getInputProps('name')}
                            saveErrorSpace
                        />
                        <TextInput
                            className={styles.textInput}
                            label="Price"
                            type='number'
                            placeholder='E.g: 2000'
                            required
                            {...getInputProps('price')}
                            saveErrorSpace
                        />
                        <TextInput
                            className={styles.textInput}
                            label="Fee Rate"
                            type='number'
                            placeholder='E.g: 57 (percentage)'
                            required
                            min={0}
                            max={100}
                            {...getInputProps('feeRate')}
                            saveErrorSpace
                        />
                        <TextInput
                            className={styles.textInput}
                            label="Image"
                            type='file'
                            {...getFileInputProps('image')}
                            saveErrorSpace
                        />
                        <TextInput
                            className={styles.textInput}
                            label="Brand"
                            placeholder='E.g: Waldorf'
                            {...getInputProps('brand')}
                            saveErrorSpace
                        />
                        <TextInput
                            className={styles.textInput}
                            label="Color"
                            placeholder='E.g: White'
                            {...getInputProps('color')}
                            saveErrorSpace
                        />
                        <TextInput
                            className={styles.textInput}
                            label="Size"
                            placeholder='E.g: Large'
                            {...getInputProps('size')}
                            saveErrorSpace
                        />
                        <TextInput
                            className={styles.textInput}
                            label="Category"
                            placeholder='E.g: Tops'
                            {...getInputProps('category')}
                            saveErrorSpace
                        />
                        <TextInput
                            className={styles.textInput}
                            label="Subcategory"
                            placeholder='E.g: Shirts'
                            {...getInputProps('subcategory')}
                            saveErrorSpace
                        />
                        <TextInput
                            className={styles.textInput}
                            label="Description"
                            placeholder='E.g: Has red sleeves. XL size'
                            {...getInputProps('description')}
                            saveErrorSpace
                        />
                        <TextInput
                            className={styles.textInput}
                            label="Reservation"
                            placeholder='E.g: John said to hold it for him'
                            {...getInputProps('reservation')}
                            saveErrorSpace
                        />
                    </div>
                    <div className={styles.buttonsContainer}>
                        <Button
                            type='submit'
                            disabled={isLoading || !wasEdited}
                            label={isLoading ? 'Saving...' : 'Save'}
                        />
                        {isEdit &&
                            <Button
                                type='button'
                                className={styles.deleteButton}
                                disabled={isLoading}
                                onClick={onShowDialog}
                                label='Delete'
                            />}
                    </div>
                </form>
            </div>
        </Modal>
)
};

export default UpdateProductModal;