import { FC } from 'react';
import { Modal } from '@mantine/core';
import styles from './ViewImageModal.module.scss';
import Loader from '../UI/Loader/Loader';
import Image from 'next/image';

type props = {
    imagePath: string|undefined;
    isOpen: boolean;
    onClose: () => void;
};


const ViewImageModal: FC<props> = ({
    imagePath, isOpen, onClose
}) => (
    <Modal
        opened={isOpen}
        onClose={onClose}
        styles={{
            modal: { width: 'auto' },
            header: { marginBottom: 0 }
        }}
    >
        <div className={styles.ViewImageModal}>
            <Image
                className={styles.image}
                src={imagePath || "/product-placeholder.png"}
                layout="fill"
                objectFit="contain"
                alt='Product Image'
            />
            <Loader className={styles.Loader} color='grey' />
        </div>
    </Modal>
);

export default ViewImageModal;