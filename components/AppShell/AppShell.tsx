import { cloneElement, FC, ReactElement } from 'react';
import styles from './AppShell.module.scss';
import Link from 'next/link';
import classNames from 'classnames';
import { useRouter } from 'next/router';
import { Button, Icon } from '../UI';
import Head from 'next/head';
import useRouteChangeLoader from '@/lib/hooks/useRouteChangeLoader';
import Loader from '../UI/Loader/Loader';
import { logout } from '@/services';


export const NAVBAR_OPTIONS = {
	HOME: 'HOME',
	CUSTOMERS: 'CUSTOMERS',
	PRODUCTS: 'PRODUCTS',
	PROFILE: 'PROFILE',
} as const;

type navBarOption = keyof typeof NAVBAR_OPTIONS;

type props = {
    children: ReactElement;
	selectedMenuOption: navBarOption;
	title?: string;
	backHref?: string;
	onShowDialog?: () => void;
};

const createNavBarOption = ( label: string, url: string, selected: navBarOption ) => {
	const isActive = NAVBAR_OPTIONS[ selected ] === label.toUpperCase();
	return (
		<Link href={url}>
			<a
				className={classNames( styles.navButton, { [styles.active]: isActive })}
			>
				{label}
			</a>
		</Link>
	);
};

const AppShell: FC<props> = ( props ) => {
	const { children, selectedMenuOption, backHref, title, ...rest } = props;

	const isLoading = useRouteChangeLoader();

	const router = useRouter();

	const onLogoutPressed = () => {
		logout()
		router.push( '/login' );
	}

	const Component = cloneElement( children, { ...rest } );

	return (
		<div className={styles.AppShell}>
			<Head>
				<title>Store Management</title>
			</Head>
			<nav className={styles.navBar}>
				{createNavBarOption( 'Customers', '/customers', selectedMenuOption )}
				{createNavBarOption( 'Products', '/products', selectedMenuOption )}
				{createNavBarOption( 'Profile', '/profile', selectedMenuOption )}
			</nav>
			<header
				className={styles.header}
			>
				<div className={styles.navState}>
					{backHref &&
						<Link href={backHref}>
							<a>
								<Icon
									className={styles.searchIcon}
									iconName='arrowLeft'
								/>
							</a>
						</Link>
					}
					<h1 className={styles.title}>{title || selectedMenuOption}</h1>
				</div>
				<Button
					className={styles.logoutButton}
					variant='subtle'
					onClick={onLogoutPressed}
					label='Logout'
				/>
			</header>
			<main className={styles.main}>
				{isLoading ?
					<Loader /> :
					Component}
			</main>
		</div>
	)
};

export default AppShell;
