import { FC } from 'react';
import { Text } from '@mantine/core';
import styles from './ProductGridItem.module.scss';
import { Product } from '@/models';
import { Button } from '../UI';
import Image from 'next/image';


type props = {
    product: Product;
    onEditDetailsClick: () => void;
    onUpdateStateClick: () => void;
    onViewImageClick: () => void;
};

const ProductGridItem: FC<props> = ( {
    product, onEditDetailsClick, onUpdateStateClick, onViewImageClick
} ) => {
    const {
        images: { smallImageUrl }, price, name,
        brand, state, feeRate, size,
        color, dateCreated
    } = product;

    return (
        <li className={styles.ProductGridItem}>
            <div className={styles.editOptions}>
                <Button onClick={onEditDetailsClick} className={styles.button} label='Details' />
                <Button onClick={onUpdateStateClick} className={styles.button} label='Update State' />
                <Button onClick={onViewImageClick} className={styles.button} label='View Image' />
            </div>
            <div className={styles.cardContainer}>
                <div className={styles.imageContainer}>
                    <Image
                        src={smallImageUrl || "/product-placeholder.png"}
                        alt="Product Image"
                        layout="fill"
                        objectFit="cover"
                    />
                    <Text
                        className={`${styles.state} ${styles[ state ]}`}
                    >
                        {state.toUpperCase()}
                    </Text>
                </div>
                <div className={styles.dataContainer}>
                        <h2 className={styles.productName}>{name}</h2>
                    <div className={styles.numericDataContainer}>
                        <h3 className={styles.numericData}>{`$ ${price}`}</h3>
                        <h3 className={styles.numericData}>{`${feeRate}%`}</h3>
                        <h3 className={styles.numericData}>{dateCreated?.toLocaleDateString()}</h3>
                    </div>
                    <div className={styles.softData}>
                        <h4 className={styles.dataLabel}>Color</h4>
                        <h4 className={styles.dataContent}>{color || '-'}</h4>
                        <h4 className={styles.dataLabel}>Size</h4>
                        <h4 className={styles.dataContent}>{size || '-'}</h4>
                        <h4 className={styles.dataLabel}>Brand</h4>
                        <h4 className={styles.dataContent}>{brand || '-'}</h4>
                    </div>
                </div>
            </div>
        </li>
    );
};

export default ProductGridItem;