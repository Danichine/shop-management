import { FC } from 'react';
import Image from 'next/image';
import styles from './CustomerGridItem.module.scss';
import Link from 'next/link';
import { Customer } from '@/models';

type props = {
    customer: Customer
};

const CustomerGridItem: FC<props> = ( { customer } ) => {
    const {
        id, name, email, telephone, totalAvailableProducts,
        totalDebt, address, description
    } = customer;

    return (
        <Link href={`/customers/${id}`}>
            <li className={styles.CustomerGridItem}>
                <div className={styles.topContainer}>
                    <h2 className={styles.customerName}>{name}</h2>
                </div>
                <div className={styles.bottomContainer}>
                    <div className={styles.imageContainer}>
                        <Image
                            src="/product-placeholder.png"
                            alt="Customer image"
                            layout="fill"
                            objectFit="cover"
                        />
                    </div>
                    <div className={styles.numericData}>
                        <h4 className={styles.dataLabel}>Products</h4>
                        <h3 className={styles.dataContent}>{totalAvailableProducts}</h3>
                        <h4 className={styles.dataLabel}>Debt</h4>
                        <h3 className={styles.dataContent}>{totalDebt}</h3>
                    </div>
                    <div className={styles.personalData}>
                        <h4 className={styles.dataLabel}>Email</h4>
                        <h4 className={styles.dataContent}>{email}</h4>
                        <h4 className={styles.dataLabel}>Telephone</h4>
                        <h4 className={styles.dataContent}>{telephone}</h4>
                        <h4 className={styles.dataLabel}>Address</h4>
                        <h4 className={styles.dataContent}>{address}</h4>
                        <h4 className={styles.dataLabel}>Description</h4>
                        <h4 className={styles.dataContent}>{description}</h4>
                    </div>
                </div>
            </li>
        </Link>
    );
};

export default CustomerGridItem;