import { FC, useMemo } from 'react';
import styles from './ProductsGrid.module.scss';
import Grid from '../Grid/Grid';
import Toolbar from '../Toolbar/Toolbar';
import Pagination, { DEFAULT_PER_PAGE_LIMIT, PageOption } from '../Pagination/Pagination';
import ProductGridItem from '../ProductGridItem/ProductGridItem';
import {
	Product, PRODUCT_SORT_OPTION, PRODUCT_SORT_OPTIONS,
	DEFAULT_PRODUCT_FILTER, PRODUCT_FILTER_OPTION, PRODUCT_FILTER_OPTIONS
} from '@/models';
import EmptyListMessage from '../EmptyListMessage/EmptyListMessage';
import Loader from '../UI/Loader/Loader';
import classNames from 'classnames';

type props = {
	className?: string;
	products: Product[];
	limit: number;
	onPageChange: (value: PageOption) => void;
	onLimitChange: (value: number) => void;
	sortValue: string;
	onSortChange: (value: PRODUCT_SORT_OPTION) => void;
	filterValue: string;
	onFilterChange: (value: PRODUCT_FILTER_OPTION) => void;
	onEditDetailsClick: (product: Product) => void;
	onUpdateStateClick: (product: Product) => void;
	onViewImageClick: (product: Product) => void;
	onSearch: (value: string) => void;
	searchValue: string;
	isLoading: boolean;
	hasNextPage: boolean;
	hasPreviousPage: boolean;
	contrastMode?: boolean;
};


const ProductsGrid: FC<props> = ({
	className, products, onPageChange, onLimitChange,
	sortValue, onSortChange, filterValue, onFilterChange,
	onEditDetailsClick, onUpdateStateClick, limit = DEFAULT_PER_PAGE_LIMIT,
	onSearch, onViewImageClick, searchValue, isLoading,
	hasNextPage, hasPreviousPage, contrastMode
}) => {
	const productsItemComponents = products.map(p => (
		<ProductGridItem
			key={p.id}
			product={p}
			onEditDetailsClick={() => onEditDetailsClick(p)}
			onUpdateStateClick={() => onUpdateStateClick(p)}
			onViewImageClick={() => onViewImageClick(p)}
		/>
));

	const productsExist = productsItemComponents?.length > 0;
	const isFiltering = filterValue !== DEFAULT_PRODUCT_FILTER || !!searchValue;

	const placeholderComponent = useMemo(() => isLoading ?
		<Loader /> :
		<EmptyListMessage
			message='There are no products yet'
			secondaryMessage='There are no products for your current applied filters.'
			applySecondary={isFiltering}
		/>,
		[ isFiltering, isLoading ]
);

	return (
		<div className={classNames(styles.ProductsGrid, className)}>
			<Toolbar
				className={styles.toolbar}
				sortValue={sortValue}
				sortOptions={[
					{ value: PRODUCT_SORT_OPTIONS.UPDATED, label: 'Date updated' },
					{ value: PRODUCT_SORT_OPTIONS.CREATED, label: 'Date created' },
					{ value: PRODUCT_SORT_OPTIONS.PRICE, label: 'Price' },
					{ value: PRODUCT_SORT_OPTIONS.BRAND, label: 'Brand' },
				]}
				onSortValueChange={onSortChange as (value: string) => void}
				filterValue={filterValue}
				filterOptions={[
					{ value: PRODUCT_FILTER_OPTIONS.ALL, label: 'All' },
					{ value: PRODUCT_FILTER_OPTIONS.AVAILABLE, label: 'Available' },
					{ value: PRODUCT_FILTER_OPTIONS.SOLD, label: 'Sold' },
					{ value: PRODUCT_FILTER_OPTIONS.PAID, label: 'Paid' }
				]}
				onFilterValueChange={onFilterChange as (value: string) => void}
				onSearch={onSearch}
			/>
			<Pagination
				onPageSet={onPageChange}
				onLimitSet={onLimitChange}
				limit={limit}
				hasNextPage={hasNextPage}
				hasPreviousPage={hasPreviousPage}
				contrastMode={contrastMode}
			/>
			{productsExist ?
				<Grid className={styles.gridContainer}>
					{productsItemComponents}
				</Grid> :
				placeholderComponent
			}
		</div>
);
};

export default ProductsGrid;