import { storage } from '@/firebaseConfig';
import { deleteObject, getDownloadURL, getStream, ref, uploadBytesResumable } from 'firebase/storage';

export type ImageBuffers = {
    smallImageBuffer: Buffer;
    largeImageBuffer: Buffer;
}

export type StoredImageNames = {
    smallImageName: string;
    largeImageName: string;
}

export const IMAGES_BASE_PATH = 'images'

export const uploadImage = async ({ path, imageBuffers, storedImageNames }: { path: string, imageBuffers: ImageBuffers, storedImageNames: StoredImageNames }) => {
    const { smallImageBuffer, largeImageBuffer } = imageBuffers;
    
    const { smallImageName, largeImageName} = storedImageNames;

    const storageRefSmall = ref(storage, `${path}${smallImageName}`);
    const storageRefLarge = ref(storage, `${path}${largeImageName}`);

    const snapshotSmall = await uploadBytesResumable(storageRefSmall, smallImageBuffer);
    const snapshotLarge = await uploadBytesResumable(storageRefLarge, largeImageBuffer);

    const downloadUrlSmall = await getDownloadURL(snapshotSmall.ref)
    const downloadUrlLarge = await getDownloadURL(snapshotLarge.ref)

    return { downloadUrlSmall, downloadUrlLarge };
}

export const downloadImage = ( imagePath: string ) => {
    
    const storageRef = ref(storage, imagePath);
    
    return getStream(storageRef)
}

export const removeImage = ( imagePath: string ) => {

    const storageRef = ref(storage, imagePath);

    return deleteObject(storageRef)
}

export const removeImages = ( imagePaths: string[] ) => {

    const promises = imagePaths.map((item) => { removeImage(item) });

    return Promise.all(promises)
}