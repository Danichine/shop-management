import { IMAGES_BASE_PATH, ImageBuffers, StoredImageNames, uploadImage } from "./crud";

export const PRODUCTS_IMAGES_PATH = `${IMAGES_BASE_PATH}/products/`;
export const PRODUCTS_IMAGES_ENCODED_PATH = `${IMAGES_BASE_PATH}%2Fproducts%2F`;

export const uploadProductImage = ({ imageBuffers, storedImageNames }: { imageBuffers: ImageBuffers, storedImageNames: StoredImageNames }) => {
    return uploadImage({ path: PRODUCTS_IMAGES_PATH, imageBuffers, storedImageNames })
};
