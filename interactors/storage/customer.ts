import { IMAGES_BASE_PATH, ImageBuffers, StoredImageNames, uploadImage } from "./crud";

export const CUSTOMERS_IMAGES_PATH = `${IMAGES_BASE_PATH}/customers/`;
export const CUSTOMERS_IMAGES_ENCODED_PATH = `${IMAGES_BASE_PATH}%2Fcustomers%2F`;

export const uploadCustomerImage = ({ imageBuffers, storedImageNames }: { imageBuffers: ImageBuffers, storedImageNames: StoredImageNames }) => {
    return uploadImage({ path: CUSTOMERS_IMAGES_PATH, imageBuffers, storedImageNames })
};
