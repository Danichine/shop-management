import {
    collection, getDoc, doc,
    query, getDocs, orderBy, limit, startAfter, limitToLast, endAt, endBefore, QuerySnapshot, where, WhereFilterOp
} from "firebase/firestore";
import { db } from "@/firebaseConfig";

interface ClassConstructor<T> {
    new (...args: any[]): T;
}

export type QueryFilter = {
    field: string,
    operator: WhereFilterOp,
    targetValue: string | number
}

export type Sort = {
    value: string,
    direction: "desc" | "asc"
}
export type Filter = { field: string, operator: string, targetValue: string|number };

const getDocSnapshot = async ({ collectionName, documentId }: { collectionName: string, documentId: string }) => {
    const docRef = doc(db, collectionName, documentId);

    return await getDoc(docRef);
}

const convertDocsToModel = <T>({ snapshot, model: Model }: { snapshot: QuerySnapshot, model: ClassConstructor<T>}) => {
    return snapshot.docs.map((item) => {
        const data = item.data();
        data.id = item.id;
        return new Model(data);
    })
}

export const getDocument = async <T>({ collectionName, model: Model, documentId }: { collectionName: string, model: ClassConstructor<T>, documentId: string }) => {

    const document = await getDocSnapshot({ collectionName, documentId });
    const data = document.data();

    if (data) {
        data.id = document.id;
    }
    return new Model(data)
}

export const getCollectionFirstPage = async <T>({
    collectionName,
    model,
    sortBy,
    limit: countLimit,
    filters
}: {
    collectionName: string,
    model: ClassConstructor<T>,
    sortBy: Sort[],
    limit: number,
    filters?: QueryFilter[]
}) => {
    const whereClause = filters?.map((item) => where(item.field, item.operator, item.targetValue)) || [];
    const sortClause = sortBy.map((item) => orderBy(item.value, item.direction))

    const collectionQuery = query(
        collection(db, collectionName),
        limit(countLimit),
        ...whereClause,
        ...sortClause
    );
    const snapshot = await getDocs(collectionQuery);

    return convertDocsToModel({ snapshot, model });
}

export const getCollectionLastPage = async <T>({
    collectionName,
    model,
    sortBy,
    limit: countLimit,
    filters
}: {
    collectionName: string,
    model: ClassConstructor<T>,
    sortBy: Sort[],
    limit: number
    filters?: QueryFilter[]
}) => {
    const whereClause = filters?.map((item) => where(item.field, item.operator, item.targetValue)) || [];
    const sortClause = sortBy.map((item) => orderBy(item.value, item.direction))

    const collectionQuery = query(
        collection(db, collectionName),
        limit(countLimit),
        ...whereClause,
        ...sortClause
    );
    const snapshot = await getDocs(collectionQuery);

    return convertDocsToModel({ snapshot, model }).reverse()
}

export const getCollectionNextPage = async <T>({
    collectionName,
    model,
    sortBy,
    limit: countLimit,
    lastVisibleId,
    filters
}: {
    collectionName: string,
    model: ClassConstructor<T>,
    sortBy: Sort[],
    limit: number,
    lastVisibleId: string,
    filters?: QueryFilter[]
}) => {
    const lastVisibleDoc = await getDocSnapshot({ collectionName, documentId: lastVisibleId })

    const whereClause = filters?.map((item) => where(item.field, item.operator, item.targetValue)) || [];
    const sortClause = sortBy.map((item) => orderBy(item.value, item.direction))

    const collectionQuery = query(
        collection(db, collectionName),
        startAfter(lastVisibleDoc),
        limit(countLimit),
        ...whereClause,
        ...sortClause
    );
    const snapshot = await getDocs(collectionQuery);

    return convertDocsToModel({ snapshot, model })
}

export const getCollectionPreviousPage = async <T>({
    collectionName,
    model,
    sortBy,
    limit,
    lastVisibleId,
    filters,
}: {
    collectionName: string,
    model: ClassConstructor<T>,
    sortBy: Sort[],
    limit: number,
    lastVisibleId: string,
    filters?: QueryFilter[]
}) => {
    const lastVisibleDoc = await getDocSnapshot({ collectionName, documentId: lastVisibleId });

    const whereClause = filters?.map((item) => where(item.field, item.operator, item.targetValue)) || [];
    const sortClause = sortBy.map((item) => orderBy(item.value, item.direction))

    const collectionQuery = query(
        collection(db, collectionName),
        endBefore(lastVisibleDoc),
        limitToLast(limit),
        ...whereClause,
        ...sortClause
    );
    const snapshot = await getDocs(collectionQuery);

    return convertDocsToModel({ snapshot, model })
}