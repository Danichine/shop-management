import { doc, deleteDoc } from "firebase/firestore";
import { db } from "@/firebaseConfig";

export const deleteDocument = async ({
    collectionName,
    documentId
}: {
    collectionName: string,
    documentId: string
}) => {
    const docRef = doc(db, collectionName, documentId);

    await deleteDoc(docRef);
}
