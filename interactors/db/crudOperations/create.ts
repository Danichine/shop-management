import {
    collection, addDoc, PartialWithFieldValue, DocumentData
} from "firebase/firestore";
import { db } from "@/firebaseConfig";

interface ClassConstructor<T> {
    new (...args: any[]): T;
}

export const addDocument = async <T extends PartialWithFieldValue<DocumentData>>({
    collectionName,
    data,
    model: Model
}: {
    collectionName: string,
    data: T,
    model: ClassConstructor<T>
}) => {
    const colRef = collection(db, collectionName)

    delete data.id;

    // TODO: CHEQUEAR SI EXISTE EL DOCUMENTO PARA NO DUPLICARLO?
    const docRef = await addDoc(colRef, data);

    return new Model({ ...data, id: docRef.id })
}
