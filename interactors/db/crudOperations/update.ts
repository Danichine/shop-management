import {
    doc, updateDoc, PartialWithFieldValue, DocumentData
} from "firebase/firestore";
import { db } from "@/firebaseConfig";

export const updateDocument = async <Model extends PartialWithFieldValue<DocumentData>>({
    collectionName,
    documentId,
    data
}: { collectionName: string, documentId: string, data: Model }) => {

    const docRef = doc(db, collectionName, documentId);

    delete data.id;

    return await updateDoc(docRef, data);
}
