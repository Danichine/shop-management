import { STATES } from "@/models";
import { Filter, QueryFilter } from "../crudOperations";

const SUPPORTED_FILTERS: { [key: string]: Filter } = Object.freeze({
	available: { field: "state", operator: "==", targetValue: STATES.AVAILABLE },
	sold:{ field: "state", operator: "==", targetValue: STATES.SOLD },
	paid:{ field: "state", operator: "==", targetValue: STATES.PAID }
});

const createSearchFilters: (search: string) => Filter[] = (search: string) => [
    { field: "name", operator: ">=", targetValue: search },
    { field: "name", operator: "<=", targetValue: `${search} \uf8ff` }
]

export const getQueryOptions = ({
    customerId, search, filters, sort
}: {
    customerId?: string, search?: string, filters?: string[], sort: string
}) => {
    const reformattedFilters: QueryFilter[] = [];
    const mergedSort: { value: string, direction: "desc"|"asc" }[] = [{ value: sort, direction: "desc" }];

    if (customerId) {
        reformattedFilters.push({ field: 'ownerId', operator: '==', targetValue: customerId });
    }

    if (filters?.length) {
        filters.forEach((filterKey) => {
            const filter = SUPPORTED_FILTERS[filterKey];

            if (!filter) return;

            const { field, operator } = filter;

            reformattedFilters.push(filter as QueryFilter);
            if (operator !== "==" && !mergedSort.find((item) => item.value === field)) {
				mergedSort.unshift({ value: field, direction: "desc" });
			}
        });
    }

    if (search) {
        const searchFilters = createSearchFilters(search);

        searchFilters.forEach((filter) => {
            const { field, operator } = filter;

            reformattedFilters.push(filter as QueryFilter);
            if (!mergedSort.find((item) => item.value === field)) {
				mergedSort.unshift({ value: field, direction: "desc" });
			}
        });
    }

    return { filters: reformattedFilters, sortBy: mergedSort }
}
