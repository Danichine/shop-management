import { Product } from "@/models";
import {
    getCollectionFirstPage, getCollectionLastPage, getCollectionNextPage,
    getCollectionPreviousPage
} from "../crudOperations";
import { PRODUCTS_COLLECTION } from "./crud";
import { getQueryOptions } from "./getQueryOptions";

export const getProductsFirstPage = async ({
	customerId, sortBy: defaultSort, limit, filters: unformattedFilters, search: unformattedSearch
}: {
	customerId?: string, sortBy: string, limit: number, filters?: string[], search?: string
}): Promise<Product[]> => {
	const { filters, sortBy } = getQueryOptions({ customerId, filters: unformattedFilters, sort: defaultSort, search: unformattedSearch });

	return await getCollectionFirstPage<Product>({
		collectionName: PRODUCTS_COLLECTION,
		model: Product,
		sortBy,
		limit,
		filters
	});
}

export const getProductsNextPage = async ({
	customerId, sortBy: defaultSort, limit, lastVisibleId, filters: unformattedFilters, search: unformattedSearch
}: {
	customerId?: string, sortBy: string, limit: number, lastVisibleId: string, filters?: string[], search?: string
}): Promise<Product[]> => {
	const { filters, sortBy } = getQueryOptions({ customerId, filters: unformattedFilters, sort: defaultSort, search: unformattedSearch });

	return await getCollectionNextPage<Product>({
		collectionName: PRODUCTS_COLLECTION,
		model: Product,
		sortBy,
		limit,
		lastVisibleId,
		filters
	});
}

export const getProductsPreviousPage = async ({
	customerId, sortBy: defaultSort, limit, lastVisibleId, filters: unformattedFilters, search: unformattedSearch
}: {
	customerId?: string, sortBy: string, limit: number, lastVisibleId: string, filters?: string[], search?: string
}): Promise<Product[]> => {
	const { filters, sortBy } = getQueryOptions({ customerId, filters: unformattedFilters, sort: defaultSort, search: unformattedSearch });

	return await getCollectionPreviousPage<Product>({
		collectionName: PRODUCTS_COLLECTION,
		model: Product,
		sortBy,
		limit,
		lastVisibleId,
		filters
	});
}

export const getProductsLastPage = async ({
	 customerId, sortBy: defaultSort, limit, filters: unformattedFilters, search: unformattedSearch
}: {
	 customerId?: string, sortBy: string, limit: number, filters?: string[], search?: string
}): Promise<Product[]> => {
	const { filters, sortBy } = getQueryOptions({ customerId, filters: unformattedFilters, sort: defaultSort, search: unformattedSearch });

	return await getCollectionLastPage<Product>({
		collectionName: PRODUCTS_COLLECTION,
		model: Product,
		sortBy,
		limit,
		filters
	});
}
