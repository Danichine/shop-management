import {
	addDocument, deleteDocument, getDocument, updateDocument
} from "../crudOperations";
import { Product } from "@/models";

export const PRODUCTS_COLLECTION = 'products';

export const createProduct = async (product: Product) => {
	return await addDocument({
		collectionName: PRODUCTS_COLLECTION,
		data: product.asObject(),
		model: Product
	})
}

export const getProductById = async (productId: string): Promise<Product> => {
	return await getDocument<Product>({
		collectionName: PRODUCTS_COLLECTION,
		model: Product,
		documentId: productId
	});
}

export const updateProduct = async (product: Product) => {
	return await updateDocument({ collectionName: PRODUCTS_COLLECTION, documentId: product.id, data: product.asObject() })
}

export const deleteProduct = async (productId: string) => {
	return await deleteDocument({ collectionName: PRODUCTS_COLLECTION, documentId: productId })
}

