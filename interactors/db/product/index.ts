export {
    createProduct as dbCreateProduct,
    getProductById as dbGetProductById,
    updateProduct as dbUpdateProduct,
    deleteProduct as dbDeleteProduct
} from './crud'
export {
    getProductsFirstPage as dbGetProductsFirstPage,
    getProductsNextPage as dbGetProductsNextPage,
    getProductsPreviousPage as dbGetProductsPreviousPage,
    getProductsLastPage as dbGetProductsLastPage
} from './pagination'