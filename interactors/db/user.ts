import { getAuth } from "firebase/auth";
import { User } from "@/models";
import { getDocument } from "./crudOperations";
import firebaseApp from "@/firebaseConfig";

const USERS_COLLECTION = 'users';

export const getUser = async (userId: string): Promise<User> => {
    return await getDocument<User>({
        collectionName: USERS_COLLECTION,
        model: User,
        documentId: userId
    });
}

export const getCurrentUser = async (): Promise<User> => {
    const loggedUser = getAuth(firebaseApp).currentUser

    return await getUser(loggedUser?.uid || "");
}