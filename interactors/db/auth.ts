import { getAuth, signInWithEmailAndPassword, signOut } from "firebase/auth";
import firebaseApp from '@/firebaseConfig';
import { throwUnauthorizedError } from "@/lib/errors";

export const login = async ( email: string, password: string ) => {
    const auth = getAuth(firebaseApp);
    const { user } = await signInWithEmailAndPassword(auth, email, password);

    if ( !user ) {
        throwUnauthorizedError( 'Wrong credentials.' );
    }

    const accessToken = await user.getIdToken();

    return {
        user,
        accessToken
    }
}

export const logout = async () => {
    const auth = getAuth(firebaseApp);
    return await signOut(auth)
}