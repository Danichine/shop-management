import {
    getCollectionLastPage,
    getCollectionFirstPage,
    getCollectionNextPage,
    getCollectionPreviousPage,
} from "../crudOperations";
import { Customer } from "@/models";
import { CUSTOMERS_COLLECTION } from "./crud";
import { getQueryOptions } from "./getQueryOptions";

export const getCustomersFirstPage = async ({
    sortBy: defaultSort, limit, filters: unformattedFilters, search: unformattedSearch
}: {
    sortBy: string, limit: number, filters?: string[], search?: string
}): Promise<Customer[]> => {

    const { filters, sortBy } = getQueryOptions({
        filters: unformattedFilters, sort: defaultSort, search: unformattedSearch
    });

    return await getCollectionFirstPage<Customer>({
        collectionName: CUSTOMERS_COLLECTION,
        model: Customer,
        sortBy,
        limit,
        filters
    })
}

export const getCustomersNextPage = async ({
    sortBy: defaultSort, limit, lastVisibleId, filters: unformattedFilters, search: unformattedSearch
}: {
    sortBy: string, limit: number, lastVisibleId: string, filters?: string[], search?: string
}): Promise<Customer[]> => {
    const { filters, sortBy } = getQueryOptions({
        filters: unformattedFilters, sort: defaultSort, search: unformattedSearch
    });

    return await getCollectionNextPage<Customer>({
        collectionName: CUSTOMERS_COLLECTION,
        model: Customer,
        sortBy,
        limit,
        lastVisibleId,
        filters
    })
}

export const getCustomersPreviousPage = async ({
    sortBy: defaultSort, limit, lastVisibleId, filters: unformattedFilters, search: unformattedSearch
}: {
        sortBy: string, limit: number, lastVisibleId: string, filters?: string[], search?: string
    }): Promise<Customer[]> => {
    const { filters, sortBy } = getQueryOptions({
        filters: unformattedFilters, sort: defaultSort, search: unformattedSearch
    });

    return await getCollectionPreviousPage<Customer>({
        collectionName: CUSTOMERS_COLLECTION,
        model: Customer,
        sortBy,
        limit,
        lastVisibleId,
        filters
    })
}

export const getCustomersLastPage = async ({
    sortBy: defaultSort, limit, filters: unformattedFilters, search: unformattedSearch
}: {
    sortBy: string, limit: number, filters?: string[], search?: string
}): Promise<Customer[]> => {
    const { filters, sortBy } = getQueryOptions({
        filters: unformattedFilters, sort: defaultSort, search: unformattedSearch
    });

    return await getCollectionLastPage<Customer>({
        collectionName: CUSTOMERS_COLLECTION,
        model: Customer,
        sortBy,
        limit,
        filters
    })
}