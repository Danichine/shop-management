import { collection, getDocs, query, where, writeBatch } from "firebase/firestore";
import {
    addDocument, deleteDocument, getDocument, updateDocument
} from "../crudOperations";
import { Customer, Product, ProductType } from "@/models";
import { db } from "@/firebaseConfig";
import { PRODUCTS_COLLECTION } from "../product/crud";

export const CUSTOMERS_COLLECTION = 'customers';

export const createCustomer = async (customer: Customer) => {
    return await addDocument({
        collectionName: CUSTOMERS_COLLECTION,
        model: Customer,
        data: customer.asObject()
    })
}

export const getCustomerById = async (customerId: string): Promise<Customer> => {
    return await getDocument<Customer>({
        collectionName: CUSTOMERS_COLLECTION,
        model: Customer,
        documentId: customerId
    });
}

const getCustomerProductsSnapshot = async (customerId: string) => {
    const colRef = collection(db, PRODUCTS_COLLECTION);

    const queryFilter = where("ownerId", "==", customerId);

    const q = query(colRef, queryFilter)

    return await getDocs(q)
}

export const updateCustomer = async (customer: Customer) => {
    return await updateDocument({ collectionName: CUSTOMERS_COLLECTION, documentId: customer.id, data: customer.asObject() })
}

export const deleteCustomerProducts = async (customerId: string) => {
    const snapshot = await getCustomerProductsSnapshot(customerId)
    
    const batch = writeBatch(db)
    
    const deletedProducts: Product[] = []

    snapshot.forEach((doc) => {
        const data = doc.data() as ProductType;
        batch.delete(doc.ref);
        deletedProducts.push(new Product(data))
    });
    
    batch.commit();

    return { deletedProducts }
}

export const deleteCustomer = async (customerId: string) => {
    await deleteDocument({ collectionName: CUSTOMERS_COLLECTION, documentId: customerId });
    return await deleteCustomerProducts(customerId);
}

