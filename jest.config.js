const nextJest = require('next/jest')

const createJestConfig = nextJest({
  dir: './',
})

const customJestConfig = {
  setupFilesAfterEnv: ['<rootDir>/jest.setup.js'],
  moduleDirectories: ['node_modules', '<rootDir>/'],
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/$1', // Maps "@/*" to the root directory
  },
  testPathIgnorePatterns: [
    '<rootDir>/__tests__/testTypes.d.ts',
    '<rootDir>/__tests__/factories/*',
    '<rootDir>/__tests__/models/productStateManagerTest.ts'
  ]
}

module.exports = createJestConfig(customJestConfig)