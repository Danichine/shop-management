import { createUnprocessableEntityError } from '@/lib/errors';
import {
	stateManagerFactory, AvailableStateManager, SoldStateManager,
	PaidStateManager, STATES
} from '@/models';
import { createCustomer, createProduct } from '../factories';
import { productStateManagerTest } from './productStateManagerTest';

// MOCKS
const currentDate = '2022-01-01';
jest
  .useFakeTimers()
  .setSystemTime(new Date(currentDate));

const originalTotalDebt = 80;
const originalTotalDebtProducts = 2;
const originalTotalSold = 150;
const originalTotalSoldProducts = 5;
const originalTotalAvailableProducts = 3;
const originalTotalPaid = 200;


describe('Product State Manager', () => {

	describe('stateManagerFactory', () => {
		describe(`When the stateManagerFactory recieves a product with an invalid state`, () => {
			const product = createProduct({ state: 'invalidState'});
			it('throws a 422 error', () => {
				expect(() => stateManagerFactory(product))
					.toThrow(createUnprocessableEntityError('Invalid product state.'));
			});
		});

		describe(`When the stateManagerFactory recieves a product with a state of "${STATES.AVAILABLE}"`, () => {
			const product = createProduct({ state: STATES.AVAILABLE});
            const instance = stateManagerFactory(product);
    
            it('returns an instance of AvailableStateManager', () => {
                expect(instance instanceof AvailableStateManager).toEqual(true);
            });
		});

		describe(`When the stateManagerFactory recieves a product with a state of "${STATES.SOLD}"`, () => {
			const product = createProduct({ state: STATES.SOLD});
            const instance = stateManagerFactory(product);
    
            it('returns an instance of SoldStateManager', () => {
                expect(instance instanceof SoldStateManager).toEqual(true);
            });
		});

		describe(`When the stateManagerFactory recieves a product with a state of "${STATES.PAID}"`, () => {
			const product = createProduct({ state: STATES.PAID});
            const instance = stateManagerFactory(product);
    
            it('returns an instance of PaidStateManager', () => {
                expect(instance instanceof PaidStateManager).toEqual(true);
            });
		});
	});

		const customerAtStart = createCustomer({
			totalDebt: originalTotalDebt,
			totalDebtProducts: originalTotalDebtProducts,
			totalSold: originalTotalSold,
			totalSoldProducts: originalTotalSoldProducts,
			totalAvailableProducts: originalTotalAvailableProducts,
			totalPaid: originalTotalPaid
		});

		const productAvailable = createProduct({ price: 100, feeRate: 30, state: STATES.AVAILABLE });
		const productSold = createProduct({ price: 100, feeRate: 30, state: STATES.SOLD });
		const productPaid = createProduct({ price: 100, feeRate: 30, state: STATES.PAID });
		const newPrice = 120;
		const newFeeRate = 25;

		// AVAILABLE STATE MANAGER
		productStateManagerTest({
			productAtStart: productAvailable,
			customerAtStart,
			updatePriceAndFee: {
				totalPaid: { describe: 'is kept the same', expect: customerAtStart.totalPaid },
				totalSold: { describe: 'is kept the same', expect: customerAtStart.totalSold },
				newPrice: 120,
				newFeeRate: 25,
			},
			updateStatesConfig: [
				{
					state: STATES.SOLD,
						totalDebt: { describe: 'is increased', expect: customerAtStart.totalDebt + productAvailable.debt },
						totalDebtProducts: { describe: 'is increased by one', expect: customerAtStart.totalDebtProducts + 1 },
						totalSold: { describe: 'is increased', expect: customerAtStart.totalSold + productAvailable.price },
						totalSoldProducts: { describe: 'is increased by one', expect: customerAtStart.totalSoldProducts + 1 },
						dateSold: { describe: 'is set to "now"', expect: new Date(currentDate) },
						totalAvailableProducts:  { describe: 'is reduced by one', expect: customerAtStart.totalAvailableProducts - 1 },
						totalPaid: { describe: 'is kept the same', expect: customerAtStart.totalPaid }
				},
				{
					state: STATES.PAID,
					totalDebt: { describe: 'is kept the same', expect: customerAtStart.totalDebt },
					totalDebtProducts: { describe: 'is kept the same', expect: customerAtStart.totalDebtProducts },
					totalSold: { describe: 'is increased', expect: customerAtStart.totalSold + productAvailable.price },
					totalSoldProducts: { describe: 'is increased by one', expect: customerAtStart.totalSoldProducts + 1 },
					dateSold: { describe: 'is set to "now"', expect: new Date(currentDate) },
					totalAvailableProducts:  { describe: 'is reduced by one', expect: customerAtStart.totalAvailableProducts - 1 },
					totalPaid: { describe: 'is increased', expect: customerAtStart.totalPaid + productAvailable.debt }
				}
			]
		})

		// SOLD STATE MANAGER
		productStateManagerTest({
			productAtStart: productSold,
			customerAtStart,
			updatePriceAndFee: {
				totalPaid: { describe: 'is kept the same', expect: customerAtStart.totalPaid },
				totalSold: { describe: 'is updated', expect: customerAtStart.totalSold - productSold.price + newPrice },
				newPrice,
				newFeeRate,
			},
			updateStatesConfig: [
				{
					state: STATES.AVAILABLE,
					totalDebt: { describe: 'is reduced', expect: customerAtStart.totalDebt - productSold.debt },
					totalDebtProducts: { describe: 'is reduced by one', expect: customerAtStart.totalDebtProducts - 1 },
					totalSold: { describe: 'is reduced', expect: customerAtStart.totalSold - productSold.price },
					totalSoldProducts: { describe: 'is reduced by one', expect: customerAtStart.totalSoldProducts - 1 },
					dateSold: { describe: 'is set to null', expect: null },
					totalAvailableProducts:  { describe: 'is increased by one', expect: customerAtStart.totalAvailableProducts + 1 },
					totalPaid: { describe: 'is kept the same', expect: customerAtStart.totalPaid }
				},
				{
					state: STATES.PAID,
					totalDebt: { describe: 'is reduced', expect: customerAtStart.totalDebt - productSold.debt },
					totalDebtProducts: { describe: 'is reduced by one', expect: customerAtStart.totalDebtProducts - 1 },
					totalSold: { describe: 'is kept the same', expect: customerAtStart.totalSold },
					totalSoldProducts: { describe: 'is kept the same', expect: customerAtStart.totalSoldProducts },
					dateSold: { describe: 'is kept the same', expect: productSold.dateSold },
					totalAvailableProducts:  { describe: 'is kept the same', expect: customerAtStart.totalAvailableProducts },
					totalPaid: { describe: 'is increased', expect: customerAtStart.totalPaid + productSold.debt }
				}
			]
		})

		// PAID STATE MANAGER
		productStateManagerTest({
			productAtStart: productPaid,
			customerAtStart,
			updatePriceAndFee: {
				totalPaid: { describe: 'is updated', expect: ({ product }) => customerAtStart.totalPaid - productPaid.debt + product.debt },
				totalSold: { describe: 'is updated', expect: customerAtStart.totalSold - productPaid.price + newPrice },
				newPrice,
				newFeeRate,
			},
			updateStatesConfig: [
				{
					state: STATES.AVAILABLE,
					totalDebt: { describe: 'is kept the same', expect: customerAtStart.totalDebt },
					totalDebtProducts: { describe: 'is kept the same', expect: customerAtStart.totalDebtProducts },
					totalSold: { describe: 'is reduced', expect: customerAtStart.totalSold - productPaid.price },
					totalSoldProducts: { describe: 'is reduced by one', expect: customerAtStart.totalSoldProducts - 1 },
					dateSold: { describe: 'is set to null', expect: null },
					totalAvailableProducts:  { describe: 'is increased by one', expect: customerAtStart.totalAvailableProducts + 1 },
					totalPaid: { describe: 'is reduced', expect: customerAtStart.totalPaid - productPaid.debt }
				},
				{
					state: STATES.SOLD,
					totalDebt: { describe: 'is increased', expect: customerAtStart.totalDebt + productPaid.debt },
					totalDebtProducts: { describe: 'is increased by one', expect: customerAtStart.totalDebtProducts + 1 },
					totalSold: { describe: 'is kept the same', expect: customerAtStart.totalSold },
					totalSoldProducts: { describe: 'is kept the same', expect: customerAtStart.totalSoldProducts },
					dateSold: { describe: 'is kept the same', expect: productPaid.dateSold },
					totalAvailableProducts:  { describe: 'is kept the same', expect: customerAtStart.totalAvailableProducts },
					totalPaid: { describe: 'is reduced', expect: customerAtStart.totalPaid - productPaid.debt }
				}
			]
		})
	});

		// 		it('Updates the customer\'s totalPaid', () => {
		// 			const originalDebt = product.price - product.fee;
		// 			const newDebt = newPrice - newFeeRate;
		// 			expect(customer.totalPaid).toEqual(originalTotalPaid - originalDebt + newDebt);
		// 		});

		// 		it('Updates the customer\'s totalSold', () => {
		// 			expect(customer.totalSold).toEqual(originalTotalSold - product.price + newPrice);
		// 		});
		// 	});

		// 	describe('updateState', () => {

		// 		describe('When updateState is called with an invalid state', () => {
		// 			it('throws a 422 error', () => {
		// 				expect(() => instance.updateState('invalid'))
		// 					.toThrow(createUnprocessableEntityError('That product state is not supported.'));
		// 			});
		// 		});

		// 		describe(`When updateState is called with "${STATES.PAID}"`, () => {
		// 			it('throws a 422 error', () => {
		// 				expect(() => instance.updateState(STATES.PAID))
		// 					.toThrow(createUnprocessableEntityError('Product state remains the same.'));
		// 			});
		// 		});
			
		// 		describe(`When updateState is called with "${STATES.AVAILABLE}"`, () => {
		// 			const originalSaleDate = new Date('2022-03-03');
		// 			const product = {
		// 				state: STATES.PAID,
		// 				price: 100,
		// 				feeRate: 30,
		// 				dateSold: originalSaleDate
		// 			};
		// 			const customer = {
		// 				totalDebt: originalTotalDebt,
		// 				totalDebtProducts: originalTotalDebtProducts,
		// 				totalSold: originalTotalSold,
		// 				totalSoldProducts: originalTotalSoldProducts,
		// 				totalAvailableProducts: originalTotalAvailableProducts,
		// 				totalPaid: originalTotalPaid
		// 			};

		// 			const stateManager = stateManagerFactory(product);

		// 			beforeAll(() => {
		// 				stateManager.updateState(STATES.AVAILABLE);
		// 			});

		// 			it('Keeps the customer\'s totalDebt the same', () => {
		// 				expect(customer.totalDebt).toEqual(originalTotalDebt);
		// 			});

		// 			it('Keeps the customer\'s totalDebtProducts the same', () => {
		// 				expect(customer.totalDebtProducts).toEqual(originalTotalDebtProducts);
		// 			});

		// 			it('Reduces the customer\'s totalSold', () => {
		// 				expect(customer.totalSold).toEqual(originalTotalSold - product.price);
		// 			});

		// 			it('Reduces the customer\'s totalSoldProducts by one', () => {
		// 				expect(customer.totalSoldProducts).toEqual(originalTotalSoldProducts - 1);
		// 			});

		// 			it('Resets the product\'s dateSold', () => {
		// 				expect(product.dateSold).toEqual(null);
		// 			});

		// 			it('Increases the customer\'s totalAvailableProducts by one', () => {
		// 				expect(customer.totalAvailableProducts).toEqual(originalTotalAvailableProducts + 1);
		// 			});

		// 			it('Reduces the customer\'s totalPaid', () => {
		// 				const debt = product.price - product.fee;
		// 				expect(customer.totalPaid).toEqual(originalTotalPaid - debt);
		// 			});
		// 		});

		// 		describe(`When updateState is called with "${STATES.SOLD}"`, () => {
		// 			const originalSaleDate = new Date('2022-03-03');
		// 			const product = {
		// 				state: STATES.PAID,
		// 				price: 100,
		// 				feeRate: 30,
		// 				dateSold: originalSaleDate
		// 			};
		// 			const customer = {
		// 				totalDebt: originalTotalDebt,
		// 				totalDebtProducts: originalTotalDebtProducts,
		// 				totalSold: originalTotalSold,
		// 				totalSoldProducts: originalTotalSoldProducts,
		// 				totalAvailableProducts: originalTotalAvailableProducts,
		// 				totalPaid: originalTotalPaid
		// 			};

		// 			const stateManager = stateManagerFactory(product);
		// 			const {
		// 				customer: updatedCustomer, product: updatedProduct
		// 			} = stateManager.updateState(STATES.SOLD);

		// 			it('Increases the customer\'s totalDebt', () => {
		// 				const newDebt = updatedProduct.price - updatedProduct.fee;
		// 				expect(updatedCustomer.totalDebt).toEqual(originalTotalDebt + newDebt);
		// 			});

		// 			it('Increases the customer\'s totalDebtProducts by one', () => {
		// 				expect(updatedCustomer.totalDebtProducts).toEqual(originalTotalDebtProducts + 1);
		// 			});

		// 			it('Keeps the customer\'s totalSold the same', () => {
		// 				expect(updatedCustomer.totalSold).toEqual(originalTotalSold);
		// 			});

		// 			it('Keeps the customer\'s totalSoldProducts the same', () => {
		// 				expect(updatedCustomer.totalSoldProducts).toEqual(originalTotalSoldProducts);
		// 			});

		// 			it('Sets the product\'s originalSaleDate to now', () => {
		// 				expect(product.dateSold).toEqual(new Date(currentDate));
		// 			});

		// 			it('Keeps the customer\'s totalAvailableProducts the same', () => {
		// 				expect(updatedCustomer.totalAvailableProducts).toEqual(originalTotalAvailableProducts);
		// 			});

		// 			it('Reduces the customer\'s totalPaid', () => {
		// 				const debt = updatedProduct.price - updatedProduct.fee;
		// 				expect(updatedCustomer.totalPaid).toEqual(originalTotalPaid - debt);
		// 			});
		// 		});
		// 	});
		// });