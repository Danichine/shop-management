import { Customer, Product, STATES, stateManagerFactory } from "@/models";
import { createUnprocessableEntityError } from "@/lib/errors";

type TestConfig = {
	describe: string
	expect: string | number | Date | null | (({ product, customer }: { product: Product, customer: Customer }) => string | number | Date)
}

const getExpectedValue = ({ expected, customer, product }: { expected: TestConfig["expect"], customer: Customer, product: Product }) => {
	return typeof expected === "function" ? expected({ customer, product }) : expected
}

export const productStateManagerTest = ({
	productAtStart,
	customerAtStart,
	updatePriceAndFee,
	updateStatesConfig
}: {
	productAtStart: Product
	customerAtStart: Customer
	updatePriceAndFee: {
		newPrice: number
		newFeeRate: number
		totalPaid: TestConfig
		totalSold: TestConfig
	},
	updateStatesConfig: {
		state: typeof STATES[ keyof typeof STATES ]
		totalDebt: TestConfig
		totalDebtProducts: TestConfig
		totalSold: TestConfig
		totalSoldProducts: TestConfig
		dateSold: TestConfig
		totalAvailableProducts: TestConfig
		totalPaid: TestConfig
	}[]
}) => {
	const stateManager = stateManagerFactory(productAtStart);

	describe(`${stateManager.constructor.name}`, () => {
			
		describe('updatePriceAndFee', () => {

			const { totalPaid, totalSold, newPrice, newFeeRate } = updatePriceAndFee;
			
			it(`The customer\'s totalPaid ${totalPaid.describe}`, () => {
				const { customer, product } = stateManager.updatePriceAndFee({ product: productAtStart, customer: customerAtStart, newPrice, newFeeRate});

				const expected = getExpectedValue({ customer, product, expected: totalPaid.expect })
				expect(customer.totalPaid).toEqual(expected);
			});

			it(`The customer\'s totalSold ${totalSold.describe}`, () => {
				const { customer, product } = stateManager.updatePriceAndFee({ product: productAtStart, customer: customerAtStart, newPrice, newFeeRate});

				const expected = getExpectedValue({ customer, product, expected: totalSold.expect })
				expect(customer.totalSold).toEqual(expected);
			});
		});

		updateStatesConfig.forEach(({
			state, totalAvailableProducts, totalDebt, totalDebtProducts, totalPaid,
			totalSold, totalSoldProducts, dateSold
		}) => {
			describe('updateState', () => {
	
				describe('When updateState is called with an invalid state', () => {
					it('throws a 422 error', () => {
						// @ts-ignore
						expect(() => stateManager.updateState({ product: productAtStart, customer: customerAtStart, newState: 'invalid' }))
							.toThrow(createUnprocessableEntityError('Invalid product state.'));
					});
				});
	
				describe(`When updateState is called with "${productAtStart.state}"`, () => {
					it('throws a 422 error', () => {
						expect(() => stateManager.updateState({ product: productAtStart, customer: customerAtStart, newState: productAtStart.state }))
							.toThrow(createUnprocessableEntityError('Product state remains the same.'));
					});
				});
	
				describe(`When updateState is called with "${state}"`, () => {
					const stateManager = stateManagerFactory(productAtStart);
					
					it(`The customer\'s totalDebt ${totalDebt.describe}`, () => {
						const { customer, product } = stateManager.updateState({ product: productAtStart, customer: customerAtStart, newState: state});
						const expected = getExpectedValue({ customer, product, expected: totalDebt.expect })
	
						expect(customer.totalDebt).toEqual(expected);
					});
	
					it(`The customer\'s totalDebtProducts ${totalDebtProducts.describe}`, () => {
						const { customer, product } = stateManager.updateState({ product: productAtStart, customer: customerAtStart, newState: state});
						const expected = getExpectedValue({ customer, product, expected: totalDebtProducts.expect })
	
						expect(customer.totalDebtProducts).toEqual(expected);
					});
	
					it(`The customer\'s totalSold ${totalSold.describe}`, () => {
						const { customer, product } = stateManager.updateState({ product: productAtStart, customer: customerAtStart, newState: state});
						const expected = getExpectedValue({ customer, product, expected: totalSold.expect })
	
						expect(customer.totalSold).toEqual(expected);
					});
	
					it(`The customer\'s totalSoldProducts ${totalSoldProducts.describe}`, () => {
						const { customer, product } = stateManager.updateState({ product: productAtStart, customer: customerAtStart, newState: state});
						const expected = getExpectedValue({ customer, product, expected: totalSoldProducts.expect })
	
						expect(customer.totalSoldProducts).toEqual(expected);
					});
	
					it(`The product\'s dateSold ${dateSold.describe}`, () => {
						const { customer, product } = stateManager.updateState({ product: productAtStart, customer: customerAtStart, newState: state});
						const expected = getExpectedValue({ customer, product, expected: dateSold.expect })
	
						expect(product.dateSold).toEqual(expected);
					});
	
					it(`The customer\'s totalAvailableProducts ${totalAvailableProducts.describe}`, () => {
						const { customer, product } = stateManager.updateState({ product: productAtStart, customer: customerAtStart, newState: state});
						const expected = getExpectedValue({ customer, product, expected: totalAvailableProducts.expect })
	
						expect(customer.totalAvailableProducts).toEqual(expected);
					});
	
					it(`The customer\'s totalPaid ${totalPaid.describe}`, () => {
						const { customer, product } = stateManager.updateState({ product: productAtStart, customer: customerAtStart, newState: state});
						const expected = getExpectedValue({ customer, product, expected: totalPaid.expect })
	
						expect(customer.totalPaid).toEqual(expected);
					});
				});
			});
	
		})
	});
}