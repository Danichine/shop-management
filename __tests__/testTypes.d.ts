export type MockResponse = {
    status?: () => MockResponse,
    json?: () => void,
    send?: () => void,
    setHeader?: () => void
};

export type MockRequest = {
    userId?: string;
    userName?: string;
    userRole?: string;
    userEmail?: string;
    file?: any;
    query?: {
        [key: string]: string
    },
    body?: {
        [key: string]: string
    }
};