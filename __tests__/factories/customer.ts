import { Customer } from "@/models";

export const createCustomer = (customerProps?: { [key: string]: string | number | string[] }) => {
	const product = Customer.create({
		name: "John"
	})

	const mergedProps = {
		...product,
		...customerProps,
	}

	return new Customer(mergedProps);
}