import { Product } from "@/models";
import { Timestamp } from "firebase/firestore";

export const createProduct = (productProps?: { [key: string]: string | number | Timestamp }) => {
	const product = Product.create({
		name: "Bag",
		price: 123,
		feeRate: 23,
		ownerId: "123"
	})

	const mergedProps = {
		...product,
		saleDate: null,
		...productProps,
	}

	return new Product(mergedProps);
}