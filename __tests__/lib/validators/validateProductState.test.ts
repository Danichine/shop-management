import { createUnprocessableEntityError } from '@/lib/errors';
import { validateProductState } from '@/lib/validators';
import { STATES } from '@/models';


describe('Validate Product State', () => {

    describe('When no state is passed', () => {
        it('Sends a 422 error response', () => {
            expect(validateProductState)
                .toThrowError(
                    createUnprocessableEntityError('Invalid product state.')
            );
        });

        describe('When an invalid state is passed', () => {
            it('Sends a 422 error response', () => {
                // @ts-ignore
                expect(() => validateProductState('invalidState'))
                    .toThrowError(
                        createUnprocessableEntityError('Invalid product state.')
                );
            });
        });

        describe('When an valid state is passed', () => {
            it('Sends a 422 error response', () => {
                expect(() => validateProductState(STATES.AVAILABLE))
                    .not.toThrow();
            });
        });
    });
});
