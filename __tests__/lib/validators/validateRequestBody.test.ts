const { validationResult } = require('express-validator');
import { validateRequestBody } from '@/lib/validators';
import { MockResponse } from '@/__tests__/testTypes';
import { Request, Response } from '@/lib/types';

// MOCKS
jest.mock('express-validator', () => {

    const expressValidator: any = {
        validationResult: jest.fn(() => ({ isEmpty: () => true })),
        body: jest.fn(() => expressValidator),
        exists: jest.fn(() => expressValidator),
        trim: jest.fn(() => expressValidator),
        isLength: jest.fn(() => expressValidator),
        withMessage: jest.fn(() => expressValidator),
        custom: jest.fn(() => expressValidator),
        if: jest.fn(() => expressValidator)
    };
    return expressValidator;
});

describe('Validate Request Body', () => {

    describe('When no validations are passed', () => {
        const res: MockResponse = {
            status: jest.fn(() => res),
            json: jest.fn(),
            send: jest.fn()
        };
        const req = { query: {} };
        const requestHandler = jest.fn();

        beforeEach(() => {
            // @ts-ignore
            validateRequestBody(null)(requestHandler)(req as Request, res as unknown as Response);
        });

        it('Sends a 422 error response', () => {
            expect(res.status).toHaveBeenCalledWith(422);
            expect(res.send).toHaveBeenCalledWith({
                message: 'Validations error.',
                type: 'UnprocessableEntity',
                statusCode: 422
            });
        });
    });

    describe('When a list of validations are passed', () => {

        describe('When the validations fail', () => {
            const res: MockResponse = {
                status: jest.fn(() => res),
                json: jest.fn(),
                send: jest.fn()
            };
            const req = { query: {} };
            const requestHandler = jest.fn();
            const validation = { run: () => Promise.resolve(true) };
            const validations = [ validation, validation ];
            const validationError = { param: 'Field', msg: 'Not valid.'}
    
            beforeEach(() => {
                validationResult.mockImplementationOnce(jest.fn(() => ({
                    isEmpty: () => false,
                    array: () => ([
                        validationError
                    ])
                })))
                validateRequestBody(validations)(requestHandler)(req as Request, res as unknown as Response);
            });
            
            it('Sends a 422 error response', () => {
                expect(res.status).toHaveBeenCalledWith(422);
                expect(res.send).toHaveBeenCalledWith({
                    message: `"${validationError.param}" ${validationError.msg}`,
                    type: 'UnprocessableEntity',
                    statusCode: 422
                });
            });
        });

        describe('When the validations succeed', () => {
            const res: MockResponse = {
                status: jest.fn(() => res),
                json: jest.fn(),
                send: jest.fn()
            };
            const req = { query: {} };
            const requestHandler = jest.fn();
            const validation = { run: () => Promise.resolve(true) };
            const validations = [ validation, validation ];
    
            beforeEach(() => {
                validateRequestBody(validations)(requestHandler)(req as Request, res as unknown as Response);
            });
    
            it('Calls the handler with "req" and "res"', () => {
                expect(requestHandler).toHaveBeenCalledWith(req as Request, res as unknown as Response);
            });
        });
    });
});
