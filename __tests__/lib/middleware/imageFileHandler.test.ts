import { MockResponse } from '@/__tests__/testTypes';
import multerRequestHandler from '@/lib/imagesProcessing/multerRequestHandler';
import imageFileHandler from '@/lib/middleware/imageFileHandler';

// MOCKS
jest.mock('@/lib/imagesProcessing/multerRequestHandler', () => ({
	__esModule: true,
	default: jest.fn()
}));

describe('Image File Handler', () => {
	
	describe('When a handler is provided', () => {
		const res: MockResponse = {
			status: jest.fn(() => res),
			json: jest.fn(),
			send: jest.fn()
		};
		const originalRequestHandler = jest.fn();
		const req = {};

		beforeEach(() => {
			imageFileHandler(originalRequestHandler)(req, res);
		})

		it('Calls the "multerRequestHandler" with the "originalRequestHandler" wrapped in a function as the third argument', () => {
			expect(multerRequestHandler).toHaveBeenCalled();
		});
	});
});
