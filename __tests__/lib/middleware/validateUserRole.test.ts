import { MockRequest, MockResponse } from '@/__tests__/testTypes';
import { validateUserRole } from '@/lib/middleware';
import { Request, Response } from '@/lib/types';
import { ROLES } from '@/roles/roles';

describe('validateUserRole', () => {
    const res: MockResponse = {
        status: jest.fn(() => res),
        json: jest.fn(() => {}),
        send: jest.fn(() => {})
    };
    const requestHandler = jest.fn();

    afterEach(() => {
        res.status = jest.fn(() => res),
        res.send = jest.fn(() => {})
    })

    describe('When no target role is passed', () => {
        const req = { userRole: ROLES.ADMIN };

        it('responds with a 403 error', () => {
            // @ts-ignore
            validateUserRole()(requestHandler)(req as MockRequest, res as unknown as Response);

            expect(res.status).toHaveBeenCalledWith(403);
            expect(res.send).toHaveBeenCalledWith({
                message: 'A target role must be provided.',
                type: 'Forbidden',
                statusCode: 403
            });
        });
    });

    describe('When no user role is passed', () => {
        const req = {};

        it('responds with a 403 error', () => {
            validateUserRole(ROLES.ADMIN)(requestHandler)(req as Request, res as unknown as Response);

            expect(res.status).toHaveBeenCalledWith(403);
            expect(res.send).toHaveBeenCalledWith({
                message: 'A user role must be provided.',
                type: 'Forbidden',
                statusCode: 403
            });
        });
    });

    describe('When the target role is not supported', () => {
        const req = { userRole: ROLES.ADMIN };

        it('responds with a 403 error', () => {
            // @ts-ignore
            validateUserRole("notSupported")(requestHandler)(req as MockRequest, res as unknown as Response);

            expect(res.status).toHaveBeenCalledWith(403);
            expect(res.send).toHaveBeenCalledWith({
                message: 'The target role provided is not supported.',
                type: 'Forbidden',
                statusCode: 403
            });
        });
    });

    describe('When the user role is not supported', () => {
        const req = { userRole: "notSupported" };

        it('responds with a 403 error', () => {
            validateUserRole(ROLES.ADMIN)(requestHandler)(req as Request, res as unknown as Response);

            expect(res.status).toHaveBeenCalledWith(403);
            expect(res.send).toHaveBeenCalledWith({
                message: 'The user role provided is not supported.',
                type: 'Forbidden',
                statusCode: 403
            });
        });
    });

    describe(`When the target role is "${ROLES.BASIC}"`, () => {
        describe(`When the user\'s role is "${ROLES.BASIC}"`, () => {
            const req = { userRole: ROLES.BASIC };
    
            it('responds by executing the recieved handler', () => {
                validateUserRole(ROLES.BASIC)(requestHandler)(req as Request, res as unknown as Response);
    
                expect(requestHandler).toHaveBeenCalledWith(req, res as unknown as Response);
            });
        });

        describe(`When the user\'s role is "${ROLES.ADMIN}"`, () => {
            const req = { userRole: ROLES.ADMIN };
    
            it('responds by executing the recieved handler', () => {
                validateUserRole(ROLES.BASIC)(requestHandler)(req as Request, res as unknown as Response);
    
                expect(requestHandler).toHaveBeenCalledWith(req, res as unknown as Response);
            });
        });
    });

    describe(`When the target role is "${ROLES.ADMIN}"`, () => {
        describe(`When the user\'s role is "${ROLES.BASIC}"`, () => {
            const req = { userRole: ROLES.BASIC };
    
            it('responds with a 403 error', () => {
                validateUserRole(ROLES.ADMIN)(requestHandler)(req as Request, res as unknown as Response);
    
                expect(res.status).toHaveBeenCalledWith(403);
                expect(res.send).toHaveBeenCalledWith({
                    message: 'You are not allowed to access this resource.',
                    type: 'Forbidden',
                    statusCode: 403
                });
            });
        });

        describe(`When the user\'s role is "${ROLES.ADMIN}"`, () => {
            const req = { userRole: ROLES.ADMIN };
    
            it('responds by executing the recieved handler', () => {
                validateUserRole(ROLES.ADMIN)(requestHandler)(req as Request, res as unknown as Response);
    
                expect(requestHandler).toHaveBeenCalledWith(req, res as unknown as Response);
            });
        });
    });
});
