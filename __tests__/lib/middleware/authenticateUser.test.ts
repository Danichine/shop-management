import authenticateUser from '@/lib/middleware/authenticateUser';
import { ROLES } from '@/roles/roles';
import { MockRequest, MockResponse } from '@/__tests__/testTypes';
import { Request, Response } from '@/lib/types';
import { User } from '@/models';
const firebaseAuth = require('firebase/auth');
const dbInteractors = require('@/interactors');

// MOCKS
jest.mock('@/firebaseConfig', () => ({}));
jest.mock('firebase/auth', () => ({
    getAuth: jest.fn()
}));
jest.mock('@/interactors', () => ({
    dbGetUser: jest.fn()
}));


describe('Authenticate User', () => {

    describe('When the user is found', () => {
        const res: MockResponse = {
            status: jest.fn(() => res),
            json: jest.fn(),
            send: jest.fn()
        };
        const requestHandler = jest.fn();
        const req: MockRequest = {};
        const user = new User({ id: "123", name: "Richard", email: "elhuer@fanito.com", role: ROLES.ADMIN })

        beforeAll(() => {
            firebaseAuth.getAuth = () => ({ currentUser: { uid: user.id } })
            dbInteractors.dbGetUser = () => user
            authenticateUser(requestHandler)(req as Request, res as unknown as Response);
        })

        it('Adds the userId and userRole to the request', () => {
            expect(req.userId).toEqual(user.id);
            expect(req.userRole).toEqual(user.role);
        });

        it('Calls the handler with the request and response objects', () => {
            expect(requestHandler).toHaveBeenCalledWith(req, res as unknown as Response);
        });
    });
});
