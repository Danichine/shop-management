import { MockRequest, MockResponse } from '@/__tests__/testTypes';
import validateRequestMethod from '@/lib/middleware/validateRequestMethod';
import { Request, Response } from '@/lib/types';

describe('validateRequestMethod', () => {
    const res: MockResponse = {
        status: jest.fn(() => res),
        json: jest.fn(),
        send: jest.fn()
    };
    const requestHandler = jest.fn();

    describe('When no methods are passed', () => {
        const req = { method: 'POST' };

        it('responds with a 405 error', () => {
            validateRequestMethod()(requestHandler)(req as Request, res as unknown as Response);

            expect(res.status).toHaveBeenCalledWith(405);
            expect(res.send).toHaveBeenCalledWith({
                message: 'Request method not supported.',
                type: 'Method not allowed',
                statusCode: 405
            });
        });
    });

    describe('When one method is passed', () => {
        const methods = [ 'POST' ];
        
        describe('When the method is incorrect', () => {
            const req = { method: 'GET' };

            it('responds with a 405 error', () => {
                validateRequestMethod(methods)(requestHandler)(req as Request, res as unknown as Response);
                expect(res.status).toHaveBeenCalledWith(405);
                expect(res.send).toHaveBeenCalledWith({
                    message: `Request method not supported. Try ${methods}`,
                    type: 'Method not allowed',
                    statusCode: 405
                });
            });
        });

        describe('When the method is correct', () => {
            const req = { method: 'POST' };

            it('responds by executing the recieved handler', () => {
                validateRequestMethod(methods)(requestHandler)(req as Request, res as unknown as Response);
                expect(requestHandler).toHaveBeenCalledWith(req as MockRequest, res as unknown as Response);
            });
        });
    });

    describe('When multiple methods are passed', () => {
        const methods = [ 'GET', 'POST' ];
        
        describe('When no methods are correct', () => {
            const req = { method: 'PUT' };

            it('responds with a 405 error', () => {
                validateRequestMethod(methods)(requestHandler)(req as Request, res as unknown as Response);
                expect(res.status).toHaveBeenCalledWith(405);
                expect(res.send).toHaveBeenCalledWith({
                    message: `Request method not supported. Try ${methods}`,
                    type: 'Method not allowed',
                    statusCode: 405
                });
            });
        });

        describe('When one method is correct', () => {
            const req = { method: 'POST' };

            it('responds by executing the recieved handler', () => {
                validateRequestMethod(methods)(requestHandler)(req as Request, res as unknown as Response);
                expect(requestHandler).toHaveBeenCalledWith(req as MockRequest, res as unknown as Response);
            });
        });
    });
});
