import { createCustomer, createProduct } from "@/__tests__/factories";
import { MockResponse } from "@/__tests__/testTypes";
const { createProduct: createProductController } = require('@/controllers');
const dbInteractors = require('@/interactors');

// MOCKS
jest.mock('@/interactors', () => ({
	dbGetCustomerById: jest.fn(),
    dbUpdateCustomer: jest.fn(),
    dbCreateProduct: jest.fn(),
	uploadProductImage: jest.fn()
}))

const testRequestBodyFields = (fieldName: string, fields: { [key: string]: string|number}) => {
    describe(`When "${fieldName}" is not in the request body`, () => {
        const res: MockResponse = {
            status: jest.fn(() => res),
            json: jest.fn(),
            send: jest.fn()
        };
        beforeEach(() => {
            const req = { body: fields };
            createProductController(req, res);
        });
        
        it('Sends a 422 error response', () => {
            expect(res.status).toHaveBeenCalledWith(422);
            expect(res.send).toHaveBeenCalledWith({
                message: 'Name, price, feeRate and customerId are mandatory.',
                type: 'UnprocessableEntity',
                statusCode: 422
            });
        });
    });
};

describe('Create Product', () => {
    const name = 'Bob';
    const feeRate = 22;
    const price = 33;
    const customerId = 123;
    testRequestBodyFields('name', { feeRate, price, customerId });
    testRequestBodyFields('feeRate', { price, customerId, name });
    testRequestBodyFields('price', { feeRate, customerId, name });
    testRequestBodyFields('customerId', { feeRate, price, name });

    describe('When all the required fields are present in the request body', () => {

        describe('When the customer is not found', () => {
            const res: MockResponse = {
                status: jest.fn(() => res),
                json: jest.fn(),
                send: jest.fn()
            };
            const req = { body: { name, feeRate, price, customerId } };
    
            beforeEach(() => {
                createProductController(req, res);
            });
            
            it('Sends a 404 error response', () => {
                expect(res.status).toHaveBeenCalledWith(404);
                expect(res.send).toHaveBeenCalledWith({
                    message: 'Customer not found.',
                    type: 'NotFound',
                    statusCode: 404
                });
            });
        });

        describe('When the customer is found', () => {
            const res: MockResponse = {
                status: jest.fn(() => res),
                json: jest.fn(),
                send: jest.fn()
            };
            const req = { body: { name, feeRate, price, customerId } };
            const customer = createCustomer({
                id: "123",
            });
            const product = createProduct({ id: "223" });
    
            beforeEach(() => {
                res.status = jest.fn(() => res);
                dbInteractors.dbGetCustomerById = () => customer;
                dbInteractors.dbCreateProduct = () => product
                createProductController(req, res);
            });
            
            it('Updates the customer\'s "product" list and totalAvailableProducts', () => {
                const updatedCustomer = createCustomer({
                    ...customer,
                    totalAvailableProducts: 1,
                    productIds: [ "223" ]
                })
                expect(dbInteractors.dbUpdateCustomer).toHaveBeenCalledWith(updatedCustomer)
            });

            it('Sends a success response', () => {
                expect(res.status).toHaveBeenCalledWith(201);
                expect(res.send).toHaveBeenCalledWith({
                    message: 'Product created.',
                    product
                });
            });
        });
    });
});
