import { createCustomer, createProduct } from '@/__tests__/factories';
import { MockRequest, MockResponse } from '@/__tests__/testTypes';
import { deleteProduct } from '@/controllers';
const dbInteractors = require('@/interactors');
import { Request, Response } from '@/lib/types';

const { Product, Customer, STATES } = require('@/models');

// MOCKS
jest.mock('@/interactors', () => ({
    dbGetProductById: jest.fn(),
    dbGetCustomerById: jest.fn(),
    dbDeleteProduct: jest.fn(),
    dbUpdateCustomer: jest.fn(),
    removeImages: jest.fn()
}))


describe('Delete Product', () => {
    describe('When there is no "productId" in the request query', () => {
        const res: MockResponse = {
            status: jest.fn(() => res),
            json: jest.fn(),
            send: jest.fn()
        };
        const req: MockRequest = { query: {} };

        beforeEach(() => {
            deleteProduct(req as Request, res as unknown as Response);
        });
        
        it('Sends a 422 error response', () => {
            expect(res.status).toHaveBeenCalledWith(422);
            expect(res.send).toHaveBeenCalledWith({
                message: 'Product ID should be provided.',
                type: 'UnprocessableEntity',
                statusCode: 422
            });
        });
    });

    describe('When there is a "productId" in the request query', () => {
        describe('When the product is not found', () => {
            const res: MockResponse = {
                status: jest.fn(() => res),
                json: jest.fn(),
                send: jest.fn()
            };
            const req: MockRequest = { query: { productId: "123" } };
    
            beforeEach(() => {
                dbInteractors.dbGetProductById = () => undefined
                deleteProduct(req as Request, res as unknown as Response);
            });
            
            it('Sends a 404 error response', () => {
                expect(res.status).toHaveBeenCalledWith(404);
                expect(res.send).toHaveBeenCalledWith({
                    message: 'Product not found.',
                    type: 'NotFound',
                    statusCode: 404
                });
            });
        });
    
        describe('When the product is found', () => {

            describe('When the customer is not found', () => {
                const product = { id: 1, owner: 312 };
                const res: MockResponse = {
                    status: jest.fn(() => res),
                    json: jest.fn(),
                    send: jest.fn()
                };
                const req: MockRequest = { query: { productId: "123" } };
        
                beforeEach(() => {
                    dbInteractors.dbGetProductById = () => product
                    dbInteractors.dbGetCustomerById = () => undefined
                    deleteProduct(req as Request, res as unknown as Response);
                });
    
                it('Sends a 404 error response', () => {
                    expect(res.status).toHaveBeenCalledWith(404);
                    expect(res.send).toHaveBeenCalledWith({
                        message: 'Product owner not found.',
                        type: 'NotFound',
                        statusCode: 404
                    });
                });
            });

            describe('When the customer is found', () => {    

                describe(`When the product state is "${STATES.AVAILABLE}"`, () => {
                    const product = createProduct({
                        id: "122",
                        ownerId: "312",
                        state: STATES.AVAILABLE
                    });
                    const originalTotalDebt = 2
                    const customer = createCustomer({
                        productIds: [
                            "333",
                            "122"
                        ],
                        totalAvailableProducts: originalTotalDebt
                    })
                    const res: MockResponse = {
                        status: jest.fn(() => res),
                        json: jest.fn(),
                        send: jest.fn()
                    };
                    const req: MockRequest = { query: { productId: "123" } };
            
                    beforeAll(() => {
                        dbInteractors.dbGetProductById = () => product
                        dbInteractors.dbGetCustomerById = () => customer
                        deleteProduct(req as Request, res as unknown as Response);
                    });

                    it('Removes the product from the customer', () => {
                        expect(customer.productIds.length).toBe(1);
                        expect(customer.productIds.includes(product.id)).toBe(false);
                    });
        
                    it('Reduces the customer\'s totalAvailableProducts count by one', () => {
                        expect(customer.totalAvailableProducts).toEqual(originalTotalDebt - 1);
                    });

                    it('Return a success response', () => {
                        expect(res.status).toHaveBeenCalledWith(200);
                        expect(res.send).toHaveBeenCalledWith({
                            message: 'Product deleted.',
                            product
                        });
                    });
                });

                describe(`When the product state is "${STATES.SOLD}"`, () => {
                    const product = createProduct({
                        id: "122",
                        ownerId: "312",
                        state: STATES.SOLD,
                        feeRate: 10,
                        price: 100
                    });
                    const originalTotalDebt = 200
                    const customer = createCustomer({
                        productIds: [
                            "333",
                            "122"
                        ],
                        totalDebt: originalTotalDebt
                    })
                    const res: MockResponse = {
                        status: jest.fn(() => res),
                        json: jest.fn(),
                        send: jest.fn()
                    };
                    const req: MockRequest = { query: { productId: "123" } };
            
                    beforeAll(() => {
                        dbInteractors.dbGetProductById = () => product
                        dbInteractors.dbGetCustomerById = () => customer
                        deleteProduct(req as Request, res as unknown as Response);
                    });

                    it('Removes the product from the customer', () => {
                        expect(customer.productIds.length).toBe(1);
                        expect(customer.productIds.includes(product.id)).toBe(false);
                    });
        
                    it('Reduces the customer\'s totalDebt by the product\'s feeRate', () => {
                        expect(customer.totalDebt).toEqual(originalTotalDebt - product.debt);
                    });

                    it('Return a success response', () => {
                        expect(res.status).toHaveBeenCalledWith(200);
                        expect(res.send).toHaveBeenCalledWith({
                            message: 'Product deleted.',
                            product
                        });
                    });
                });

                describe(`When the product state is "${STATES.PAID}"`, () => {
                    const product = createProduct({
                        id: "122",
                        ownerId: "312",
                        state: STATES.PAID,
                        feeRate: 10,
                        price: 100
                    });
                    const originalTotalDebt = 200
                    const originalTotalAvailableProducts = 2
                    const customer = createCustomer({
                        productIds: [
                            "333",
                            "122"
                        ],
                        totalDebt: originalTotalDebt,
                        totalAvailableProducts: originalTotalAvailableProducts
                    })
                    const res: MockResponse = {
                        status: jest.fn(() => res),
                        json: jest.fn(),
                        send: jest.fn()
                    };
                    const req: MockRequest = { query: { productId: "123" } };
            
                    beforeAll(() => {
                        dbInteractors.dbGetProductById = () => product
                        dbInteractors.dbGetCustomerById = () => customer
                        deleteProduct(req as Request, res as unknown as Response);
                    });

                    it('Removes the product from the customer', () => {
                        expect(customer.productIds.length).toBe(1);
                        expect(customer.productIds.includes(product.id)).toBe(false);
                    });
        
                    it('Doesn\'t change the customer\'s totalDebt', () => {
                        expect(customer.totalDebt).toEqual(originalTotalDebt);
                    });

                    it('Doesn\'t change the customer\'s totalAvailableProducts', () => {
                        expect(customer.totalAvailableProducts).toEqual(originalTotalAvailableProducts);
                    });

                    it('Return a success response', () => {
                        expect(res.status).toHaveBeenCalledWith(200);
                        expect(res.send).toHaveBeenCalledWith({
                            message: 'Product deleted.',
                            product
                        });
                    });
                });
            });
        });
    });
});
