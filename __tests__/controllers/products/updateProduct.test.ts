import { createCustomer, createProduct } from "@/__tests__/factories";
import { MockResponse } from "@/__tests__/testTypes";
import { STATES, stateManagerFactory } from "@/models";
const { updateProduct } = require('@/controllers');
const dbInteractors = require('@/interactors');

jest.mock('@/interactors', () => ({
    dbGetProductById: jest.fn(),
    dbUpdateCustomer: jest.fn(),
    removeImages: jest.fn(),
    uploadProductImage: jest.fn(),
    dbGetCustomerById: jest.fn(),
    dbUpdateProduct: jest.fn()
}))

describe('Update Product', () => {

    describe('When there is no "productId" in the request query', () => {
        const res: MockResponse = {
            status: jest.fn(() => res),
            json: jest.fn(),
            send: jest.fn()
        };
        const req = { query: {}, body: {} };

        beforeEach(() => {
            updateProduct(req, res);
        });
        
        it('Sends a 422 error response', () => {
            expect(res.status).toHaveBeenCalledWith(422);
            expect(res.send).toHaveBeenCalledWith({
                message: 'Product ID should be provided.',
                type: 'UnprocessableEntity',
                statusCode: 422
            });
        });
    });

    describe('When there is a "productId" in the request query', () => {
        
        describe('When the product was not found', () => {
            const res: MockResponse = {
                status: jest.fn(() => res),
                json: jest.fn(),
                send: jest.fn()
            };
            const req = { query: { productId: "123" } };

            beforeEach(() => {
                updateProduct(req, res);
            });
            
            it('Sends a 404 error response', () => {
                expect(res.status).toHaveBeenCalledWith(404);
                expect(res.send).toHaveBeenCalledWith({
                    message: 'Product not found.',
                    type: 'NotFound',
                    statusCode: 404
                });
            });
        });

        describe('When the product was found', () => {
            describe('When no valid fields are found in the request', () => {
                const res: MockResponse = {
                    status: jest.fn(() => res),
                    json: jest.fn(),
                    send: jest.fn()
                };
                const req = { query: { productId: "123" }, body: {} };

                beforeEach(() => {
                    dbInteractors.dbGetProductById = () => ({})
                    updateProduct(req, res);
                });
                
                it('Sends a 422 error response', () => {
                    expect(res.status).toHaveBeenCalledWith(422);
                    expect(res.send).toHaveBeenCalledWith({
                        message: 'At least one field should be changed.',
                        type: 'UnprocessableEntity',
                        statusCode: 422
                    });
                });
            });

            describe('When there is one correct field in the request', () => {

                describe('When the product feeRate is going to change', () => {
                    describe('When the customer is not found', () => {
                        const res: MockResponse = {
                            status: jest.fn(() => res),
                            json: jest.fn(),
                            send: jest.fn()
                        };
                        const req = {
                            query: { productId: "1234" },
                            body: { name: 'Johnny', feeRate: "22" }
                        };
        
                        beforeEach(() => {
                            dbInteractors.dbGetCustomerById = () => undefined
                            updateProduct(req, res);
                        });
                        
                        it('Sends a 404 error response', () => {
                            expect(res.status).toHaveBeenCalledWith(404);
                            expect(res.send).toHaveBeenCalledWith({
                                message: 'Customer not found.',
                                type: 'NotFound',
                                statusCode: 404
                            });
                        });
                    });

                    describe('When the customer is found', () => {
                        const res: MockResponse = {
                            status: jest.fn(() => res),
                            json: jest.fn(),
                            send: jest.fn()
                        };
                        const req = {
                            query: { productId: "1234" },
                            body: { name: 'Johnny', feeRate: "22" }
                        };

                        const { name: newName, feeRate: newFeeRate } = req.body;

                        const initialTotalDebt = 200;

                        const product = createProduct({
                            feeRate: 50,
                            price: 100,
                            state: STATES.SOLD
                        });
                        const customer = createCustomer({
                            totalDebt: initialTotalDebt,
                        })
                        beforeEach(() => {
                            res.status = jest.fn(() => res);
                            res.send = jest.fn();
                            dbInteractors.dbGetProductById = () => product;
                            dbInteractors.dbGetCustomerById = () => customer;
                            dbInteractors.dbUpdateCustomer = jest.fn()
                            updateProduct(req, res);
                        });

                        const productStateManager = stateManagerFactory(product);
                        const { customer: updatedCustomer, product: updatedProduct } = productStateManager.updatePriceAndFee({ product, customer, newFeeRate: parseInt(newFeeRate) });

                        it('Updates the customer', () => {
                            expect(dbInteractors.dbUpdateCustomer).toHaveBeenCalledWith(updatedCustomer);
                        });

                        it('Sends a success response with the updated product', () => {
                            updatedProduct.name = newName;

                            expect(res.status).toHaveBeenCalledWith(200);
                            expect(res.send).toHaveBeenCalledWith({
                                message: 'Product updated.',
                                product: updatedProduct
                            });
                        });
                    });
                });

                describe('When the product price is going to change', () => {
                    describe('When the customer is not found', () => {
                        const res: MockResponse = {
                            status: jest.fn(() => res),
                            json: jest.fn(),
                            send: jest.fn()
                        };
                        const req = {
                            query: { productId: "1234" },
                            body: { name: 'Johnny', price: "2200" }
                        };
        
                        beforeEach(() => {
                            dbInteractors.dbGetCustomerById = () => undefined
                            updateProduct(req, res);
                        });
                        
                        it('Sends a 404 error response', () => {
                            expect(res.status).toHaveBeenCalledWith(404);
                            expect(res.send).toHaveBeenCalledWith({
                                message: 'Customer not found.',
                                type: 'NotFound',
                                statusCode: 404
                            });
                        });
                    });

                    describe('When the customer is found', () => {
                        const res: MockResponse = {
                            status: jest.fn(() => res),
                            json: jest.fn(),
                            send: jest.fn()
                        };
                        const req = {
                            query: { productId: "1234" },
                            body: { name: 'Johnny', price: "2200" }
                        };

                        const { name: newName, price: newPrice } = req.body;

                        const initialTotalDebt = 200;

                        const product = createProduct({
                            feeRate: 50,
                            price: 100,
                            state: STATES.SOLD
                        });
                        const customer = createCustomer({
                            totalDebt: initialTotalDebt,
                        })
                        beforeEach(() => {
                            res.status = jest.fn(() => res);
                            res.send = jest.fn();
                            dbInteractors.dbGetProductById = () => product;
                            dbInteractors.dbGetCustomerById = () => customer;
                            dbInteractors.dbUpdateCustomer = jest.fn()
                            updateProduct(req, res);
                        });

                        const productStateManager = stateManagerFactory(product);
                        const { customer: updatedCustomer, product: updatedProduct } = productStateManager.updatePriceAndFee({ product, customer, newPrice: parseInt(newPrice) });

                        it('Updates the customer', () => {
                            expect(dbInteractors.dbUpdateCustomer).toHaveBeenCalledWith(updatedCustomer);
                        });

                        it('Sends a success response with the updated product', () => {
                            updatedProduct.name = newName;

                            expect(res.status).toHaveBeenCalledWith(200);
                            expect(res.send).toHaveBeenCalledWith({
                                message: 'Product updated.',
                                product: updatedProduct
                            });
                        });
                    });
                });
            });

            describe('When the product feeRate or price is not going to change', () => {
                const res: MockResponse = {
                    status: jest.fn(() => res),
                    json: jest.fn(),
                    send: jest.fn()
                };
                const req = {
                    query: { productId: 1234 },
                    body: { name: 'Johnny' }
                };
                const originalfeeRate = 10;
                const product = createProduct({
                    feeRate: originalfeeRate,
                    price: 3400,
                    state: STATES.SOLD
                })

                beforeEach(() => {
                    res.status = jest.fn(() => res);
                    res.send = jest.fn();
                    dbInteractors.dbGetProductById = () => product;
                    updateProduct(req, res);
                });

                it('Sends a success response with the updated product', () => {
                    product.name = req.body.name

                    expect(res.status).toHaveBeenCalledWith(200);
                    expect(res.send).toHaveBeenCalledWith({
                        message: 'Product updated.',
                        product
                    });
                });
            });
        });
    });
});
