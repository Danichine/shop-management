import { MockResponse } from "@/__tests__/testTypes";
const { deletePicture } = require( '@/controllers' );
const dbInteractors = require( '@/interactors' );

// MOCKS
jest.mock('@/interactors', () => ({
    dbGetProductById: jest.fn(),
    removeImages: jest.fn(),
    dbUpdateProduct: jest.fn()
}))

describe( 'Delete Picture', () => {
    describe( 'When there is no "productId" in the request query', () => {
        const res: MockResponse = {
            status: jest.fn( () => res ),
            json: jest.fn(),
            send: jest.fn()
        };
        const req = { query: {} };

        beforeEach( () => {
            deletePicture( req, res );
        });
        
        it( 'Sends a 422 error response', () => {
            expect( res.status ).toHaveBeenCalledWith( 422 );
            expect( res.send ).toHaveBeenCalledWith({
                message: 'Product ID should be provided.',
                type: 'UnprocessableEntity',
                statusCode: 422
            });
        });
    });

    describe( 'When there is a "productId" in the request query', () => {
        describe( 'When the product is not found', () => {
            const res: MockResponse = {
                status: jest.fn( () => res ),
                json: jest.fn(),
                send: jest.fn()
            };
            const req = { query: { productId: "123" } };
    
            beforeEach( () => {
                deletePicture( req, res );
            });
            
            it( 'Sends a 404 error response', () => {
                expect( res.status ).toHaveBeenCalledWith( 404 );
                expect( res.send ).toHaveBeenCalledWith({
                    message: 'Product not found.',
                    type: 'NotFound',
                    statusCode: 404
                });
            });
        });
    
        describe( 'When the product is found', () => {
            const product = {
                id: 1,
                images: {
                    smallImageUrl: '/location',
                    largeImageUrl: '/location',
                }
            };
            const res: MockResponse = {
                status: jest.fn( () => res ),
                json: jest.fn(),
                send: jest.fn()
            };
            const req = { query: { productId: '123' } };
    
            beforeAll( () => {
                dbInteractors.dbGetProductById = jest.fn(() => (product))
                deletePicture( req, res );
            });

            it( 'Return a success response', () => {
                // expect( res.status ).toHaveBeenCalledWith( 200 );
                expect( res.send ).toHaveBeenCalledWith({
                    message: 'Product image deleted.',
                    product
                });
            });
        });
    });
});
