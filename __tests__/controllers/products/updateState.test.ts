import { createCustomer, createProduct } from "@/__tests__/factories";
import { MockResponse } from "@/__tests__/testTypes";
import { STATES, STATES_OPTIONS, stateManagerFactory } from "@/models";
const dbInteractors = require('@/interactors');
const { updateState } = require('@/controllers');

// MOCKS
const currentDate = '2022-01-01';
jest
  .useFakeTimers()
  .setSystemTime(new Date(currentDate));

jest.mock('@/interactors', () => ({
	dbGetCustomerById: jest.fn(),
	dbGetProductById: jest.fn(),
	dbUpdateCustomer: jest.fn(),
	dbUpdateProduct: jest.fn()
}))

// FUNCTION FOR TESTING DIFFERENT STATE SCENARIOS
const testStateChange = (currentState: STATES_OPTIONS, targetState: STATES_OPTIONS) => {
	describe(`When the current state is ${targetState} and the target state is ${currentState}`, () => {
		const res: MockResponse = {
			status: jest.fn(() => res),
			json: jest.fn(),
			send: jest.fn()
		};
		const req = { query: { productId: "123" }, body: { state: targetState } };
		const product = createProduct({
			name: "Bag",
			feeRate: 10,
			price: 2200,
			ownerId: "123",
			state: currentState
		})
		const customer = createCustomer({
			totalAvailableProducts: 1,
			totalDebt: 20,
		});

		beforeEach(() => {
			dbInteractors.dbGetProductById = () => product
			dbInteractors.dbGetCustomerById = () => customer
			dbInteractors.dbUpdateProduct = jest.fn()
			res.status = jest.fn(() => res)
			res.send = jest.fn()
			updateState(req, res);
		});

		const stateManager = stateManagerFactory(product);
		const { product: updatedProduct, customer: updatedCustomer } = stateManager.updateState({ product, customer, newState: targetState });
		
		it('Updates the customer and product in the database', () => {
			expect(dbInteractors.dbUpdateProduct).toHaveBeenCalledWith(updatedProduct);
			// expect(dbInteractors.dbUpdateCustomer).toHaveBeenCalledWith(updatedCustomer);
		});

		// it('Sends a success response', () => {
		//     expect(res.status).toHaveBeenCalledWith(200);
		//     expect(res.send).toHaveBeenCalledWith({
		//         message: 'Product state updated.',
		//         product: updatedProduct
		//     });
		// });
	});
};

describe('Update State', () => {
	describe('When there is no "state" in the request body', () => {
		const res: MockResponse = {
			status: jest.fn(() => res),
			json: jest.fn(),
			send: jest.fn()
		};
		const req = { query: {}, body: {} };

		beforeEach(() => {
			updateState(req, res);
		});
		
		it('Sends a 422 error response', () => {
			expect(res.status).toHaveBeenCalledWith(422);
			expect(res.send).toHaveBeenCalledWith({
				message: 'Invalid product state.',
				type: 'UnprocessableEntity',
				statusCode: 422
			});
		});
	});

	describe('When there is a "state" in the request body', () => {
		describe('When the "state" is invalid', () => {
			const res: MockResponse = {
				status: jest.fn(() => res),
				json: jest.fn(),
				send: jest.fn()
			};
			const req = { query: {}, body: { state: 'invalidState' } };
	
			beforeEach(() => {
				updateState(req, res);
			});
			
			it('Sends a 422 error response', () => {
				expect(res.status).toHaveBeenCalledWith(422);
				expect(res.send).toHaveBeenCalledWith({
					message: 'Invalid product state.',
					type: 'UnprocessableEntity',
					statusCode: 422
				});
			});
		});

		describe('When the "state" is valid', () => {
			describe('When there is no productId in the request query', () => {
				const res: MockResponse = {
					status: jest.fn(() => res),
					json: jest.fn(),
					send: jest.fn()
				};
				const req = { query: {}, body: { state: 'available' } };
		
				beforeEach(() => {
					updateState(req, res);
				});
				
				it('Sends a 422 error response', () => {
					expect(res.status).toHaveBeenCalledWith(422);
					expect(res.send).toHaveBeenCalledWith({
						message: 'Product ID should be provided.',
						type: 'UnprocessableEntity',
						statusCode: 422
					});
				});
			});

			describe('When there is a productId in the request query', () => {
				describe('When the product is not found', () => {
					const res: MockResponse = {
						status: jest.fn(() => res),
						json: jest.fn(),
						send: jest.fn()
					};
					const req = { query: { productId: "123" }, body: { state: 'available' } };
			
					beforeEach(() => {
						updateState(req, res);
					});
					
					it('Sends a 404 error response', () => {
						expect(res.status).toHaveBeenCalledWith(404);
						expect(res.send).toHaveBeenCalledWith({
							message: 'Product not found.',
							type: 'NotFound',
							statusCode: 404
						});
					});
				});

				describe('When the product is found', () => {

					describe('When the product state is the same as the one in the query', () => {
						const res: MockResponse = {
							status: jest.fn(() => res),
							json: jest.fn(),
							send: jest.fn()
						};
						const req = { query: { productId: "123" }, body: { state: 'available' } };
						const product = { state: 'available' };
				
						beforeEach(() => {
							dbInteractors.dbGetProductById = () => product
							updateState(req, res);
						});
						
						it('Sends a 422 error response', () => {
							expect(res.status).toHaveBeenCalledWith(422);
							expect(res.send).toHaveBeenCalledWith({
								message: 'Product state remains the same.',
								type: 'UnprocessableEntity',
								statusCode: 422
							});
						});
					});

					describe('When the product state is not the same as the one in the query', () => {

						describe('When the customer is not found', () => {
							const res: MockResponse = {
								status: jest.fn(() => res),
								json: jest.fn(),
								send: jest.fn()
							};
							const req = { query: { productId: "123" }, body: { state: 'available' } };
							const product = { state: 'sold' };
					
							beforeEach(() => {
								dbInteractors.dbGetProductById = () => product
								updateState(req, res);
							});
							
							it('Sends a 404 error response', () => {
								expect(res.status).toHaveBeenCalledWith(404);
								expect(res.send).toHaveBeenCalledWith({
									message: 'Product owner not found.',
									type: 'NotFound',
									statusCode: 404
								});
							});
						});

						describe('When the customer is found', () => {
							const res: MockResponse = {
								status: jest.fn(() => res),
								json: jest.fn(),
								send: jest.fn()
							};
							testStateChange(STATES.AVAILABLE, STATES.SOLD);
							testStateChange(STATES.AVAILABLE, STATES.PAID);
							testStateChange(STATES.SOLD, STATES.AVAILABLE);
							testStateChange(STATES.SOLD, STATES.PAID);
							testStateChange(STATES.PAID, STATES.AVAILABLE);
							testStateChange(STATES.PAID, STATES.SOLD);
						});
					});
				});
			});
		});
	});
});
