import { createProduct } from "@/__tests__/factories";
import { MockRequest, MockResponse } from "@/__tests__/testTypes";
import { getProduct } from '@/controllers';
import { Request, Response } from "@/lib/types";
const dbInteractors = require('@/interactors');

// MOCKS
jest.mock('@/interactors', () => ({
    dbGetProductById: jest.fn()
}))

describe('Get Product', () => {
    describe('When there is no "productId" in the request query', () => {
        const res: MockResponse = {
            status: jest.fn(() => res),
            json: jest.fn(),
            send: jest.fn()
        };
        const req = { query: {} };

        beforeEach(() => {
            getProduct(req as Request, res as unknown as Response);
        });
        
        it('Sends a 422 error response', () => {
            expect(res.status).toHaveBeenCalledWith(422);
            expect(res.send).toHaveBeenCalledWith({
                message: 'Product ID should be provided.',
                type: 'UnprocessableEntity',
                statusCode: 422
            });
        });
    });

    describe('When there is a "productId" in the request query', () => {
        describe('When the product is not found', () => {
            const res: MockResponse = {
                status: jest.fn(() => res),
                json: jest.fn(),
                send: jest.fn()
            };
            const req: MockRequest = { query: { productId: "123" } };
    
            beforeEach(() => {
                dbInteractors.dbGetProductById = () => undefined
                getProduct(req as Request, res as unknown as Response);
            });
            
            it('Sends a 404 error response', () => {
                expect(res.status).toHaveBeenCalledWith(404);
                expect(res.send).toHaveBeenCalledWith({
                    message: 'Product not found.',
                    type: 'NotFound',
                    statusCode: 404
                });
            });
        });
    
        describe('When the product is found', () => {
            const product = createProduct({ id: "123" });
            const res: MockResponse = {
                status: jest.fn(() => res),
                json: jest.fn(),
                send: jest.fn()
            };
            const req: MockRequest = { query: { productId: "123" } };
    
            beforeEach(() => {
                dbInteractors.dbGetProductById = () => product
                getProduct(req as Request, res as unknown as Response);
            });

            it('Return a success response', () => {
                expect(res.status).toHaveBeenCalledWith(200);
                expect(res.send).toHaveBeenCalledWith({
                    message: 'Product found successfully.',
                    product
                });
            });
        });
    });
});
