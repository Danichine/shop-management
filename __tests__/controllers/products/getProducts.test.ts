import { createProduct } from "@/__tests__/factories";
import { MockRequest, MockResponse } from "@/__tests__/testTypes";
import { getProducts } from '@/controllers'
import { createUnprocessableEntityError } from "@/lib/errors";
import { Request, Response } from "@/lib/types";
const getPaginatedProducts = require('@/controllers/products/getPaginatedProducts');

jest.mock('@/controllers/products/getPaginatedProducts', () => jest.fn())

describe('Get Products', () => {

    describe('When the products are found successfully', () => {
        const res: MockResponse = {
            status: jest.fn(() => res ),
            json: jest.fn(),
            send: jest.fn()
        };
        const req: MockRequest = { query: { filter: 'invalidState' } };
        const products = [
            createProduct({ id: "123", name: "Bob" }),
            createProduct({ id: "124", name: "Stacy" })
        ]

        beforeEach(() => {
            getPaginatedProducts.mockImplementationOnce( () => ({
                products,
                lastVisibleId: "124",
                hasNextPage: true,
                hasPreviousPage: false
            }))
            getProducts(req as Request, res as unknown as Response)
        });
        
        it('Sends a success response', () => {
            expect(res.status ).toHaveBeenCalledWith(200);
            expect(res.send ).toHaveBeenCalledWith({
				message: 'Products found successfully.',
				lastVisibleId: "124",
				products,
				hasNextPage: true,
				hasPreviousPage: false
            });
        });
    });

    describe('When there is an error', () => {
        const res: MockResponse = {
            status: jest.fn(() => res ),
            json: jest.fn(),
            send: jest.fn()
        };
        const req: MockRequest = { query: { filter: 'invalidState' } };
        const error = createUnprocessableEntityError()

        beforeEach(() => {
            getPaginatedProducts.mockImplementationOnce( () => {
                throw error
            })
            getProducts(req as Request, res as unknown as Response)
        });
        
        it('Sends an error response', () => {
            expect(res.status ).toHaveBeenCalledWith(error.statusCode);
            expect(res.send).toHaveBeenCalledWith(error.asObject());
        });
    });
});
