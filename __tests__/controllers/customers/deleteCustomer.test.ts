import { MockResponse } from "@/__tests__/testTypes";

const { deleteCustomer } = require( '@/controllers' );
const dbInteractors = require('@/interactors');

// MOCKS
jest.mock('@/interactors', () => ({
    dbGetCustomerById: jest.fn(),
    dbDeleteCustomer: jest.fn(),
    removeImages: jest.fn()
}))


describe( 'Delete Customer', () => {
    describe( 'When there is no "customerId" in the request query', () => {
        const res: MockResponse = {
            status: jest.fn( () => res ),
            json: jest.fn(),
            send: jest.fn()
        };
        const req = { query: {} };

        beforeEach( () => {
            deleteCustomer( req, res );
        });
        
        it( 'Sends a 422 error response', () => {
            expect( res.status ).toHaveBeenCalledWith( 422 );
            expect( res.send ).toHaveBeenCalledWith({
                message: 'Customer ID should be provided.',
                type: 'UnprocessableEntity',
                statusCode: 422
            });
        });
    });

    describe( 'When there is a "customerId" in the request query', () => {
        describe( 'When the customer is not found', () => {
            const res: MockResponse = {
                status: jest.fn( () => res ),
                json: jest.fn(),
                send: jest.fn()
            };
            const req = { query: { customerId: 123 } };
    
            beforeEach( () => {
                dbInteractors.dbGetCustomerById = jest.fn(() => undefined)
                deleteCustomer( req, res );
            });
            
            it( 'Sends a 404 error response', () => {
                expect( res.status ).toHaveBeenCalledWith( 404 );
                expect( res.send ).toHaveBeenCalledWith({
                    message: 'Customer not found.',
                    type: 'NotFound',
                    statusCode: 404
                });
            });
        });
    
        describe( 'When the customer is found', () => {
            const products = [
                {id: 1, imageUrl: 'abc.jpeg', images: {} },
                {id: 2, imageUrl: '/cde.jpeg', images: {} }
            ]
            const customer = { id: 123, products };
            const res: MockResponse = {
                status: jest.fn( () => res ),
                json: jest.fn(),
                send: jest.fn()
            };
            const req = { query: { customerId: 123 } };
    
            beforeEach( () => {
                dbInteractors.dbGetCustomerById = jest.fn(() => (customer))
                dbInteractors.dbDeleteCustomer = jest.fn(() => ({ deletedProducts: products }))
                deleteCustomer( req, res );
            });

            it( 'Return a success response', () => {
                expect( res.status ).toHaveBeenCalledWith( 200 );
                expect( res.send ).toHaveBeenCalledWith({
                    message: 'Customer deleted successfully.',
                    customerId: customer.id.toString()
                });
            });
        });
    });
});
