import { createCustomer, createProduct } from "@/__tests__/factories";
import { MockRequest, MockResponse } from "@/__tests__/testTypes";
import { getCustomerProducts } from '@/controllers';
import { Request, Response } from "@/lib/types";
const getPaginatedProducts = require('@/controllers/products/getPaginatedProducts');
const dbInteractors = require('@/interactors');

// MOCKS
jest.mock('@/interactors', () => ({
	dbGetCustomerById: jest.fn()
}));
jest.mock('@/controllers/products/getPaginatedProducts', () => jest.fn())

describe('Get Customer Products', () => {
	describe('When there is no "customerId" in the request query', () => {
		const res: MockResponse = {
			status: jest.fn(() => res),
			json: jest.fn(),
			send: jest.fn()
		};
		const req: MockRequest = { query: {} };

		beforeEach(() => {
			getCustomerProducts(req as Request, res as unknown as Response);
		});
		
		it('Sends a 422 error response', () => {
			expect(res.status).toHaveBeenCalledWith(422);
			expect(res.send).toHaveBeenCalledWith({
				message: 'Customer ID should be provided.',
				type: 'UnprocessableEntity',
				statusCode: 422
			});
		});
	});

	describe('When there is a "customerId" in the request query', () => {
		
		describe('When the customer is not found', () => {
			const res: MockResponse = {
				status: jest.fn(() => res),
				json: jest.fn(),
				send: jest.fn()
			};
			const req: MockRequest = { query: { customerId: "123" } };
	
			beforeEach(() => {
				getCustomerProducts(req as Request, res as unknown as Response);
			});
			
			it('Sends a 404 error response', () => {
				expect(res.status).toHaveBeenCalledWith(404);
				expect(res.send).toHaveBeenCalledWith({
					message: 'Customer not found.',
					type: 'NotFound',
					statusCode: 404
				});
			});
		});
	
		describe('When the customer is found', () => {
			
			const customer = createCustomer({id: "877"});

			const res: MockResponse = {
				status: jest.fn(() => res),
				json: jest.fn(),
				send: jest.fn()
			};
			const req: MockRequest = { query: { customerId: "123" } };

			const products = [
				createProduct({ id: "123", name: "Bob" }),
				createProduct({ id: "124", name: "Stacy" })
			]
	
			beforeEach(() => {
				getPaginatedProducts.mockImplementationOnce( () => ({
					products,
					lastVisibleId: "124",
					hasNextPage: true,
					hasPreviousPage: false
				}))
				dbInteractors.dbGetCustomerById = () => customer
				getCustomerProducts(req as Request, res as unknown as Response);
			});
			
			it('Return a success response', () => {
				expect(res.status).toHaveBeenCalledWith(200);
				expect(res.send).toHaveBeenCalledWith({
					products: products,
					lastVisibleId: "124",
					hasNextPage: true,
					hasPreviousPage: false,
					message: 'Customer products found successfully.',
					customer: customer
				});
			});
		});
	});
});
