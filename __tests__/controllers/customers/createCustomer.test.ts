import { MockResponse } from "@/__tests__/testTypes";
import { Customer } from "@/models";
const { createCustomer } = require('@/controllers');
const dbInteractors = require('@/interactors');

// MOCKS
jest.mock('@/interactors', () => ({
    dbCreateCustomer: jest.fn()
}))


describe('Create Customer', () => {
    let res: MockResponse = {
        status: jest.fn(() => res),
        json: jest.fn(),
        send: jest.fn()
    };

    describe('When there is no "name" field in the request body', () => {
        beforeEach(() => {
            const req = { body: {} };
            createCustomer(req, res);
        });
        
        it('Sends a 422 error response', () => {
            expect(res.status).toHaveBeenCalledWith(422);
            expect(res.send).toHaveBeenCalledWith({
                message: 'Name is mandatory',
                type: 'UnprocessableEntity',
                statusCode: 422
            });
        });

        describe('When there are other accepted fields in the request body', () => {
            const formData = { email: 'bobs@email.com', address: 'Fake St. 123' };
            const req = { body: formData };

            beforeEach(() => {
                createCustomer(req, res);
            });
            
            it('Sends a 422 error response', () => {
                expect(res.status).toHaveBeenCalledWith(422);
                expect(res.send).toHaveBeenCalledWith({
                    message: 'Name is mandatory',
                    type: 'UnprocessableEntity',
                    statusCode: 422
                });
            });
        })
    });

    describe('When there is a "name" field in the request body', () => {

        const req = { body: { name: 'Bob' } };
        const createdCustomer = Customer.create(req.body);

        beforeEach(() => {
            res.status = jest.fn(() => res);
            res.send = jest.fn();
            Customer.create = () => createdCustomer
            createCustomer(req, res);
        });
        
        it('Creates the customer and sends the success response', () => {
            expect(res.status).toHaveBeenCalledWith(201);
            expect(res.send).toHaveBeenCalledWith({
                message: 'Customer created.',
                customer: createdCustomer
            });
        });

        describe('When there are more accepted fields in the request body', () => {
            const formData = { name: 'Bob', email: 'bobs@email.com', address: 'Fake St. 123' };
            const req = { body: formData };

            beforeEach(() => {
                dbInteractors.dbCreateCustomer = jest.fn()
                res.status = jest.fn(() => res);
                res.send = jest.fn();
                createCustomer(req, res);
            });
            
            it('Creates the customer with those fields and sends the success response', () => {
                expect(res.status).toHaveBeenCalledWith(201);
                expect(res.send).toHaveBeenCalledWith({
                    message: 'Customer created.',
                    customer: Customer.create(formData)
                });
            });
        })
    });
});
