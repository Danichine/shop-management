import { createCustomer } from "@/__tests__/factories";
import { MockRequest, MockResponse } from "@/__tests__/testTypes";
import { updateCustomer } from '@/controllers';
import { Request, Response } from "@/lib/types";
const dbInteractors = require('@/interactors');

// MOCKS
jest.mock('@/interactors', () => ({
    dbGetCustomerById: jest.fn(),
    dbUpdateCustomer: jest.fn()
}))


describe('Update Customer', () => {
    describe('When there is no "customerId" in the request query', () => {
        const res: MockResponse = {
            status: jest.fn(() => res),
            json: jest.fn(),
            send: jest.fn()
        };
        const req: MockRequest = { query: {} };

        beforeEach(() => {
            updateCustomer(req as Request, res as unknown as Response);
        });
        
        it('Sends a 422 error response', () => {
            expect(res.status).toHaveBeenCalledWith(422);
            expect(res.send).toHaveBeenCalledWith({
                message: 'Customer ID should be provided.',
                type: 'UnprocessableEntity',
                statusCode: 422
            });
        });
    });

    describe('When there is a "customerId" in the request query', () => {
        const res: MockResponse = {
            status: jest.fn(() => res),
            json: jest.fn(),
            send: jest.fn()
        };
        const req: MockRequest = { query: { customerId: "123" } };

        describe('When the customer was not found', () => {
            beforeEach(() => {
                dbInteractors.dbGetCustomerById = () => undefined
                updateCustomer(req as Request, res as unknown as Response);
            });
            
            it('Sends a 404 error response', () => {
                expect(res.status).toHaveBeenCalledWith(404);
                expect(res.send).toHaveBeenCalledWith({
                    message: 'Customer not found.',
                    type: 'NotFound',
                    statusCode: 404
                });
            });
        });

        describe('When the customer was found', () => {
            describe('When no valid fields are found in the request body', () => {
                const res: MockResponse = {
                    status: jest.fn(() => res),
                    json: jest.fn(),
                    send: jest.fn()
                };
                const req: MockRequest = { query: { customerId: "123" }, body: {} };
                const customer = createCustomer({ id: "123" });

                beforeEach(() => {
                    dbInteractors.dbGetCustomerById = () => customer
                    updateCustomer(req as Request, res as unknown as Response);
                });
                
                it('Sends a 422 error response', () => {
                    expect(res.status).toHaveBeenCalledWith(422);
                    expect(res.send).toHaveBeenCalledWith({
                        message: 'At least one field should be changed.',
                        type: 'UnprocessableEntity',
                        statusCode: 422
                    });
                });
            });

            describe('When there is one correct field in the request body', () => {
                const res: MockResponse = {
                    status: jest.fn(() => res),
                    json: jest.fn(),
                    send: jest.fn()
                };
                const req: MockRequest = {
                    query: { customerId: "123" },
                    body: { name: 'Johnny' }
                };
                const customer = createCustomer({ id: "123", name: "Bob" });

                beforeEach(() => {
                    dbInteractors.dbGetCustomerById = () => customer
                    updateCustomer(req as Request, res as unknown as Response);
                });
                
                it('Calls dbUpdateCustomer with the updated customer', () => {
                    expect(customer.name).toBe(req?.body?.name);
                    expect(dbInteractors.dbUpdateCustomer).toHaveBeenCalledWith(customer);
                });

                it('Sends a success response', () => {
                    expect(res.status).toHaveBeenCalledWith(200);
                    expect(res.send).toHaveBeenCalledWith({
                        message: 'Customer updated.',
                        customer
                    });
                });
            });
        });
    });
});
