import { createCustomer } from "@/__tests__/factories";
import { MockRequest, MockResponse } from "@/__tests__/testTypes";
import { getCustomers } from '@/controllers';
import { throwNotFoundError } from "@/lib/errors";
import { Request, Response } from "@/lib/types";
const getPaginatedCustomers = require('@/controllers/customers/getPaginatedCustomers');

// MOCKS
jest.mock('@/controllers/customers/getPaginatedCustomers', () => jest.fn())

describe('Get Customers', () => {
	describe('When getPaginatedCustomers returns an items list', () => {
		const res: MockResponse = {
			status: jest.fn(() => res),
			json: jest.fn(),
			send: jest.fn()
		};
		const req: MockRequest = { query: {} };

		const customers = [
			createCustomer({ id: "123" }),
			createCustomer({ id: "234" })
		]

		beforeEach(() => {
			getPaginatedCustomers.mockImplementationOnce(() => ({
				customers,
				hasNextPage: true,
				hasPreviousPage: false,
				lastVisibleId: "123"
			}))
			getCustomers(req as Request, res as unknown as Response);
		});
		
		it('Sends a success response', () => {
			expect(res.status).toHaveBeenCalledWith(200);
			expect(res.send).toHaveBeenCalledWith({
				message: 'Customers fetched correctly.',
				customers,
				hasNextPage: true,
				hasPreviousPage: false,
				lastVisibleId: "123"
			});
		});
	})

	describe('When getPaginatedCustomers throws an error', () => {
		const res: MockResponse = {
			status: jest.fn(() => res),
			json: jest.fn(),
			send: jest.fn()
		};
		const req = { query: {} };

		beforeEach(() => {
			getPaginatedCustomers.mockImplementationOnce(() => {
				throwNotFoundError()
			});
			getCustomers(req as Request, res as unknown as Response);
		});
		
		it('Sends an error response', () => {
			expect(res.status).toHaveBeenCalledWith(404);
			expect(res.send).toHaveBeenCalledWith({
				message: 'NotFound',
				type: 'NotFound',
				statusCode: 404
			});
		});
	})
});
