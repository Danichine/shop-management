import { MockResponse } from "@/__tests__/testTypes";
const { changePassword } = require('@/controllers');
const firebaseAuth = require('firebase/auth');

// MOCKS
jest.mock('@/firebaseConfig', () => ({}));
jest.mock('firebase/auth', () => ({
    getAuth: jest.fn(),
    updatePassword: jest.fn()
}));


describe('Change Password', () => {
    const errorResponse = {
        message: 'Unauthorized',
        statusCode: 401,
        type: 'Unauthorized'
    };

    const res: MockResponse = {
        status: jest.fn(() => res),
        json: jest.fn(),
        send: jest.fn()
    };

    describe('When the user doesn\'t exist', () => {
        const user = { id: 123 };
        const req = { body:{}, userId: user.id };

        beforeEach(() => {
            firebaseAuth.getAuth = () => ({})
            changePassword(req, res);
        });

        it('responds with a 401 error', () => {
            expect(res.status).toHaveBeenCalledWith(401);
            expect(res.send).toHaveBeenCalledWith(errorResponse);
        });
    });

    describe('When the user exists', () => {
        const oldPassword = 123;
        const newPassword = 1234;
        const confirmNewPassword = 1234;
        const user = { id: 123 };

        const req = { body: { oldPassword, newPassword, confirmNewPassword }, userId: user.id };

        beforeAll(() => {
            firebaseAuth.getAuth = () => ({ currentUser: { uid: user.id } })
            changePassword(req, res);
        });

        it('sends a success response', () => {
            expect(res.status).toHaveBeenCalledWith(200);
            expect(res.send).toHaveBeenCalledWith({
                message: 'Password changed.'
            });
        });
    });
});
