import { MockResponse } from "@/__tests__/testTypes";
const { login } = require('@/controllers/auth/login');
const { throwUnauthorizedError } = require('@/lib/errors');
const dbInteractors = require('@/interactors')

// MOCKS
jest.mock('@/interactors', () => ({
    dbLogin: jest.fn(() => ({ user: { uid: 123 }, accessToken: "123-abc" }))
}));
jest.mock('@/lib/cookies', () => ({
    createTokenCookie: () => 'abc123'
}))

describe('Login', () => {
    const email = 'one@email.com';
    const password = 'password123';
    const user = { uid: 123 };

    const errorResponse = {
        message: 'Wrong credentials.',
        statusCode: 401,
        type: 'Unauthorized'
    };

    const req = { body: { email, password }}

    describe('When the authentication throws an error', () => {
        const res: MockResponse = {
            status: jest.fn(() => res),
            json: jest.fn(),
            send: jest.fn()
        };
        beforeEach(() => {
            dbInteractors.dbLogin = jest.fn(() => throwUnauthorizedError(errorResponse.message));
            login(req, res);
        })
        it('responds with the error', () => {
            expect(res.status).toHaveBeenCalledWith(401);
            expect(res.send).toHaveBeenCalledWith(errorResponse);
        });
    });

    describe('When the user is authenticated', () => {
        const res: MockResponse = {
            status: jest.fn(() => res),
            json: jest.fn(),
            send: jest.fn(),
            setHeader: jest.fn()
        };
        beforeEach(() => {
            dbInteractors.dbLogin = jest.fn(() => ({ user: { uid: 456 }, accessToken: "123-abc" }));
            login(req, res);
        })
        it('sets the cookie in the response, responds with a message and the userId', () => {
            expect(res.setHeader).toHaveBeenCalledWith('Set-Cookie', 'abc123');
            expect(res.status).toHaveBeenCalledWith(200);
            expect(res.send).toHaveBeenCalledWith({ message: 'Logged in.', userId: "456" });
        });
    });
});

