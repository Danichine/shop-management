const ADMIN = 'admin';
const BASIC = 'basic';
export const ROLES = { ADMIN, BASIC } as const;

export type ROLE_OPTION = typeof ROLES[keyof typeof ROLES];

export const isAllowed = ( currentRole: typeof ROLES[keyof typeof ROLES], targetRole: typeof ROLES[keyof typeof ROLES] ) => {
    switch( currentRole ) {
        case ADMIN: {
            return true;
        }
        case BASIC: {
            if ( targetRole === ADMIN ) {
                return false;
            }
            return true;
        }
    }
};