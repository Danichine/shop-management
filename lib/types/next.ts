import { NextApiRequest as ExpressRequest, NextApiResponse as Response } from 'next';

type Request = ExpressRequest & {
    userId?: string
    userName?: string
    userRole?: string
    userEmail?: string
    file?: any
};

export type {
    Request, Response
}