import { NextApiRequest, NextApiResponse } from "next";
import { sendErrorResponse, throwMethodNotAllowedError } from "../errors";

type RequestHandler = ( req: NextApiRequest, res: NextApiResponse ) => any;

const validateRequestMethod = ( methods: string[] = [] ) => ( handler: RequestHandler ) => ( req: NextApiRequest, res: NextApiResponse ) => {
    try {
        if ( !methods.includes( req.method || '' ) ) {
            throwMethodNotAllowedError(
                `Request method not supported.${methods.length > 0 ? ` Try ${methods}` : ''}`
            );
        }
        return handler( req, res );
    } catch( error: any ) {
        sendErrorResponse( res, error )
    }
};

export default validateRequestMethod;