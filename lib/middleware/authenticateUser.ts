import { Request, Response } from '../types/next';
import { sendErrorResponse, throwUnauthorizedError } from "../errors";
import { getAuth } from 'firebase/auth';
import firebaseApp from '@/firebaseConfig';
import { dbGetUser } from '@/interactors';


const authenticateUser = (handler: Function) => async (req: Request, res: Response) => {
    try {

        // TODO: RECUPERAR EL USO DE COOKIES PARA NO TENER QUE HACER UN REQUEST DE USER EN CADA REQUEST
        const loggedUser = getAuth(firebaseApp).currentUser

        if (!loggedUser) {
            throwUnauthorizedError('User not found.');
        } else {
            const user = await dbGetUser(loggedUser.uid)
    
            req.userId = user.id;
            req.userName = user.name;
            req.userEmail = user.email;
            req.userRole = user.role;
        }

        return handler(req, res);
    } catch(error: any) {
        if (!error?.statusCode || error.statusCode !== 401) {
            error.statusCode = 401;
        }
        sendErrorResponse(res, error)
    }
};

export default authenticateUser;