import { sendErrorResponse } from "../errors";
import multerRequestHandler from '../imagesProcessing/multerRequestHandler';

const imageFileHandler = ( handler: Function ) => async ( req: any, res: any ) => {
    try {
        const next = () => handler( req, res );
        return multerRequestHandler( req, res, next );
    } catch( error: any ) {
        sendErrorResponse( res, error )
    }
};

export default imageFileHandler;