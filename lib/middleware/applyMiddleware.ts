import { Request, Response } from '../types/next';

type RequestHandler = ( req: Request, res: Response ) => void;
type Middleware = ( handler: RequestHandler ) => ( req: Request, res: Response ) => void;

const applyMiddleware = ( requestHandler: RequestHandler, middlewareList: Middleware[] ) => ( req: Request, res: Response ) => {
    return [ ...middlewareList ]
        .reverse()
        .reduce(
            ( nextMiddleware, currentMiddleware ) => currentMiddleware( nextMiddleware ),
            requestHandler
        )( req, res )
};

export default applyMiddleware;