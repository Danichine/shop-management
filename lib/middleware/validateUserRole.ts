import { Request, Response } from '../types/next';
import { sendErrorResponse, throwForbiddenError } from "../errors";
import { isAllowed, ROLES, ROLE_OPTION } from '@/roles/roles';

const validateUserRole = (targetRole: typeof ROLES[keyof typeof ROLES]) => (handler: Function) => async (req: Request, res: Response) => {
    try {
        if (!targetRole) {
            throwForbiddenError('A target role must be provided.');
        };

        if (!req.userRole) {
            throwForbiddenError('A user role must be provided.');
        };

        if (!Object.values(ROLES).includes(targetRole)) {
            throwForbiddenError('The target role provided is not supported.');
        };
        
        if (req.userRole && !Object.values(ROLES).includes(req.userRole as ROLE_OPTION)) {
            throwForbiddenError('The user role provided is not supported.');
        };

        if (!isAllowed(req.userRole as ROLE_OPTION, targetRole)) {
            throwForbiddenError('You are not allowed to access this resource.');
        };

        return handler(req, res);
    } catch(error: any) {
        sendErrorResponse(res, error)
    }
};

export default validateUserRole;