import cookie from 'cookie';
import { Response } from './types/next';
const MAX_AGE = 24 * 60 * 60; // One day

export const createTokenCookie = ( token: string ) => (
    cookie.serialize(
        'token',
        token,
        {
            maxAge: MAX_AGE,
            expires: new Date( Date.now() + MAX_AGE * 1000 ),
            secure: process.env.NODE_ENV === 'production',
            path: '/',
            httpOnly: true
        }
    )
);

export const removeTokenCookie = ( res: Response ) => {
    const value = cookie.serialize('token', '', {
        maxAge: -1,
        path: "/",
    });
  
    res.setHeader("Set-Cookie", value);
};