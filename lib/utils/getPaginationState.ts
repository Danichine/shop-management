import { PAGE_OPTIONS, PageOption } from "@/components/Pagination/Pagination"

export const getPaginationState = ({
    resultItems, requestedPage, queryLimit
}: {
    resultItems: any[], requestedPage: PageOption, queryLimit: number
}) => {
    const hasNextPage =
        requestedPage === PAGE_OPTIONS.NEXT && resultItems.length === queryLimit
        || requestedPage === PAGE_OPTIONS.FIRST && resultItems.length === queryLimit
        || requestedPage === PAGE_OPTIONS.PREVIOUS && !!resultItems.length
    
    const hasPreviousPage =
        requestedPage === PAGE_OPTIONS.PREVIOUS && resultItems.length === queryLimit
        || requestedPage === PAGE_OPTIONS.LAST && resultItems.length === queryLimit
        || requestedPage === PAGE_OPTIONS.NEXT && !!resultItems.length
    
    // We need to return the resultItems within the actual limit asked by the user
    if ((requestedPage === PAGE_OPTIONS.FIRST || requestedPage === PAGE_OPTIONS.NEXT) && hasNextPage && resultItems.length > 1) {
        resultItems.pop()
    }
    if ((requestedPage === PAGE_OPTIONS.LAST || requestedPage === PAGE_OPTIONS.PREVIOUS) && hasPreviousPage && resultItems.length > 1) {
        resultItems.shift()
    }
    
    const lastVisible = resultItems.length > 0 ? resultItems[resultItems.length - 1].id : ""

    return {
        lastVisible,
        hasNextPage,
        hasPreviousPage
    }
}