export const stringToDate = ( date: string|undefined|null, placeholder?: string ) => (
    !date ? placeholder : new Date( date ).toLocaleDateString()
);
