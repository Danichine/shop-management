export const clamp = ( value: number, min: number, max: number ) => {
    if ( value < min ) return min;
    return Math.min( value, max );
}
