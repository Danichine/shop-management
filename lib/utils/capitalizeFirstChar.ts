export const capitalizeFirstLetter = ( text: string|undefined|null ) => {
    if ( !text ) return;
    return text.charAt(0).toUpperCase() + text.slice(1);
};
