const decodeImageUrl = (imageUrl: string) => {
    // Remove nextjs api uri
    const smallWithoutApiUri = imageUrl.replace("/api/images/", "")

    // Replace encoded slashes (from "%2F" to "/")
    return smallWithoutApiUri.replace(/%2F/g, '/');
}

export const decodeImageUrls = (imageUrls: string[]) => {
    return imageUrls.map(decodeImageUrl)
}