import { useEffect } from "react";

const useClickOutside = ( ref: any, callback: () => void ) => {

  useEffect(() => {
	const handleClickOutside = ( event: MouseEvent ) => {
		if (ref.current && !ref.current.contains(event.target)) {
			callback();
		}
	}
	
	document.addEventListener("mousedown", handleClickOutside);

	return () => {
	  	document.removeEventListener("mousedown", handleClickOutside);
	};

  	}, [ ref, callback ]);
};

export default useClickOutside;