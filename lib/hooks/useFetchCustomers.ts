import { useRouter } from 'next/router';
import { useState, useEffect, useCallback, useRef } from 'react';
import { Customer } from '@/models';
import { DEFAULT_CUSTOMER_FILTER } from '@/models/customer/filterOptions';
import { CustomerSortOption, DEFAULT_CUSTOMER_SORT } from '@/models/customer/sortOptions';
import { DEFAULT_PER_PAGE_LIMIT, PAGE_OPTIONS, PageOption } from '@/components/Pagination/Pagination';
import { fetchCustomers } from '@/services';

const useFetchCustomers = () => {
	const [ isLoading, setIsLoading ] = useState(false);
	const [ searchValue, setSearchValue ] = useState<string>('');
	const [ sortValue, setSortValue ] = useState<CustomerSortOption>(DEFAULT_CUSTOMER_SORT);
	const [ filterValue, setFilterValue ] = useState(DEFAULT_CUSTOMER_FILTER);
	const [ limit, setLimit ] = useState(DEFAULT_PER_PAGE_LIMIT);
	const [ customersList, setCustomersList ] = useState<Customer[]>([]);
	const [ hasNextPage, setHasNextPage ] = useState<boolean>(false);
	const [ hasPreviousPage, setHasPreviousPage ] = useState<boolean>(false);

	const router = useRouter();
	const lastVisibleRef = useRef<string | undefined>()

	const fetcherFunction = useCallback(async (page?: PageOption) => {
		try {
			setIsLoading(true);
	
			const {
				customers, lastVisibleId, hasNextPage: hasNext, hasPreviousPage: hasPrev
			} = await fetchCustomers({
				page: page || PAGE_OPTIONS.FIRST,
				limit,
				filter: filterValue,
				sort: sortValue,
				search: searchValue,
				lastVisible: lastVisibleRef.current
			})
			lastVisibleRef.current = lastVisibleId

			setHasNextPage(hasNext)
			setHasPreviousPage(hasPrev)

			setCustomersList(customers);

			setIsLoading(false);
		}
		catch(error: any) {
			if (error?.statusCode === 401) {
				router.push('/login');
			}
			setIsLoading(false);
			console.log(error);
		}
	}, [
		// ADDING LAST VISIBLE TO THE DEPENDENCY ARRAY WOULD RESULT ON A INFINITE LOOP
		limit, filterValue, sortValue,
		searchValue, router
	]);

	useEffect(() => {
		fetcherFunction();
	}, [ fetcherFunction ]);

	const onSearch = (value: string) => {
		setSearchValue(value);
	}

	return {
		customersList, limit, setLimit, sortValue,
		setSortValue, filterValue, setFilterValue,
		fetchCustomers: fetcherFunction, onSearch,
		searchValue, isLoading, hasNextPage, hasPreviousPage
	}
};

export default useFetchCustomers;