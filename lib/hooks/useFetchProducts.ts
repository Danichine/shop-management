import { useRouter } from 'next/router';
import { useState, useEffect, useCallback, useRef } from 'react';
import { Product } from '@/models';
import { DEFAULT_PRODUCT_FILTER, PRODUCT_FILTER_OPTIONS } from '@/models/product/filterOptions';
import { DEFAULT_PRODUCT_SORT, PRODUCT_SORT_OPTIONS } from '@/models/product/sortOptions';
import { DEFAULT_PER_PAGE_LIMIT, PAGE_OPTIONS, PageOption } from '@/components/Pagination/Pagination';
import { fetchProducts } from '@/services';

const useFetchProducts = () => {
	const [ searchValue, setSearchValue ] = useState('');
	const [ sortValue, setSortValue ] = useState<typeof PRODUCT_SORT_OPTIONS[keyof typeof PRODUCT_SORT_OPTIONS]>(DEFAULT_PRODUCT_SORT);
	const [ filterValue, setFilterValue ] = useState<typeof PRODUCT_FILTER_OPTIONS[keyof typeof PRODUCT_FILTER_OPTIONS]>(DEFAULT_PRODUCT_FILTER);
	const [ limit, setLimit ] = useState(DEFAULT_PER_PAGE_LIMIT);
	const [ hasNextPage, setHasNextPage ] = useState(false);
	const [ hasPreviousPage, setHasPreviousPage ] = useState(false);
	const [ productsList, setProductsList ] = useState<Product[]>([]);
	const [ isLoading, setIsLoading ] = useState(false);

	const router = useRouter();
	const lastVisibleRef = useRef<string | undefined>()

	const fetcherFunction = useCallback(async (page?: PageOption) => {
		try {
			setIsLoading(true);

			const {
				products, lastVisibleId, hasPreviousPage: hasPrevious, hasNextPage: hasNext
			} = await fetchProducts({
				page: page || PAGE_OPTIONS.FIRST,
				limit,
				filter: filterValue,
				search: searchValue,
				sort: sortValue,
				lastVisible: lastVisibleRef.current,
			});

			lastVisibleRef.current = lastVisibleId;
			
			setHasNextPage(hasNext);
			setHasPreviousPage(hasPrevious)

			setProductsList(products);
		}
		catch(error: any) {
			if (error?.statusCode === 401) {
				router.push('/login');
			}
			console.log(error);
		}
		finally {
			setIsLoading(false)
		}
	}, [
		limit, filterValue, sortValue,
		searchValue, router
	]);

	useEffect(() => {
		fetcherFunction();
	}, [ fetcherFunction ]);

	const onSearch = (value: string) => {
		setSearchValue(value);
	}

	return {
		productsList, setProductsList, fetchProducts: fetcherFunction,
		limit, setLimit, sortValue, setSortValue, filterValue,
		setFilterValue, onSearch, searchValue, isLoading,
		hasNextPage, hasPreviousPage
	}
};

export default useFetchProducts;