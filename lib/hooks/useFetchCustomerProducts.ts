import { useRouter } from 'next/router';
import { useState, useEffect, useCallback, useRef } from 'react';
import { Customer, Product } from '@/models';
import { DEFAULT_PRODUCT_FILTER, PRODUCT_FILTER_OPTIONS } from '@/models/product/filterOptions';
import { DEFAULT_PRODUCT_SORT, PRODUCT_SORT_OPTIONS } from '@/models/product/sortOptions';
import { DEFAULT_PER_PAGE_LIMIT, PAGE_OPTIONS, PageOption } from '@/components/Pagination/Pagination';
import { fetchCustomerProducts } from '@/services';

const useFetchCustomerProducts = (customerId: string) => {
	const [ searchValue, setSearchValue ] = useState('');
	const [ sortValue, setSortValue ] = useState<typeof PRODUCT_SORT_OPTIONS[keyof typeof PRODUCT_SORT_OPTIONS]>(DEFAULT_PRODUCT_SORT);
	const [ filterValue, setFilterValue ] = useState<typeof PRODUCT_FILTER_OPTIONS[keyof typeof PRODUCT_FILTER_OPTIONS]>(DEFAULT_PRODUCT_FILTER);
	const [ limit, setLimit ] = useState(DEFAULT_PER_PAGE_LIMIT);
	const [ hasNextPage, setHasNextPage ] = useState(false);
	const [ hasPreviousPage, setHasPreviousPage ] = useState(false);
	const [ productsList, setProductsList ] = useState<Product[]>([]);
	const [ customerData, setCustomerData ] = useState<Customer>({} as Customer);
	const [ isLoading, setIsLoading ] = useState(false);

	const router = useRouter();
    const lastVisibleRef = useRef<string | undefined>()

	const fetcherFunction = useCallback(async (page?: PageOption) => {
		try {
			if (!customerId || typeof customerId !== 'string') return;

			setIsLoading(true);
	
			const {
				customer, products, lastVisibleId, hasNextPage: hasNext, hasPreviousPage: hasPrevious
			} = await fetchCustomerProducts({
				customerId,
				page: page || PAGE_OPTIONS.FIRST,
				limit,
				filter: filterValue === DEFAULT_PRODUCT_FILTER ? undefined : filterValue,
				search: searchValue,
				sort: sortValue,
				lastVisible: lastVisibleRef.current
			})
			lastVisibleRef.current = lastVisibleId

			setHasPreviousPage(hasPrevious);
			setHasNextPage(hasNext);


			setIsLoading(false)

			setProductsList(products);
			setCustomerData(customer);
		}
		catch(error: any) {
			if (error?.statusCode === 401) {
				router.push('/login');
			}
			setIsLoading(false);
			console.log(error);
		}
	}, [
		limit, filterValue, sortValue,
		customerId, searchValue, router
	]);

	useEffect(() => {
		fetcherFunction();
	}, [ fetcherFunction ]);

	const onSearch = (value: string) => {
		setSearchValue(value);
	}

	return {
		productsList, setProductsList, fetchCustomerProducts : fetcherFunction,
		limit, setLimit, sortValue, setSortValue, filterValue,
		setFilterValue, customerData, setCustomerData,
		onSearch, searchValue, isLoading, hasNextPage, hasPreviousPage
	}
};

export default useFetchCustomerProducts;