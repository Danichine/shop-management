import { useRouter } from 'next/router';
import { useState, useEffect, useCallback } from 'react';
import { fetchCurrentUser } from '@/services';

const useFetchUser = () => {
    const [ user, setUser ] = useState({ name: '', email: '', role: ''});

    const router = useRouter();

    const fetchUserData = useCallback(async () => {
        try {
            const { user } = await fetchCurrentUser();
            setUser( user );
        }
        catch( error: any ) {
            if ( error?.statusCode === 401 ) {
                router.push( '/login' );
            }
            console.log( error );
        }
    }, [ router ])

    useEffect(() => {
        fetchUserData();
    }, [ fetchUserData ])

    return { user }
};

export default useFetchUser;