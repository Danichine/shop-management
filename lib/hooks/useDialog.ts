import { useState } from 'react';
import type { dialogProps } from '@/components/Dialog/Dialog';

const useDialog = () => {
	const [ showDialog, setShowDialog ] = useState( false );
    const [ dialogProps, setDialogProps ] = useState({} as dialogProps);

    const showDialogWithProps = ( props: dialogProps ) => {
        const defaultProps = {
            ...props,
            cancelAction: () => {
                if ( props.cancelAction ) {
                    props.cancelAction();
                }
                setShowDialog( false );
            },
            confirmAction: () => {
                props.confirmAction();
                setShowDialog( false );
            }
        };

        setDialogProps( defaultProps );
        setShowDialog( true );
    }

    return {
        onShowDialog: showDialogWithProps,
        dialogProps,
        showDialog
    }
}

export default useDialog;