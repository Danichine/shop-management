import { useRouter } from 'next/router';
import { useCallback, useEffect, useState } from 'react';
import { Customer } from '@/models';
import { fetchCustomer } from '@/services';

const useFetchCustomer = (customerId?: string) => {
    const [ customer, setCustomer ] = useState<Customer | null>(null);
    const [ isLoading, setIsLoading ] = useState(false);

    const router = useRouter();

    const fetcherFunction = useCallback(async () => {
        try {
            if (!customerId) return;

            setIsLoading(true);

            const customer = await fetchCustomer(customerId)

            setCustomer(customer);
            setIsLoading(false);
        }
        catch(error: any) {
            if (error?.statusCode === 401) {
                router.push('/login');
            }
            setIsLoading(false);
            console.log(error);
        }
    }, [ customerId, router ]);

    useEffect(() => {
        fetcherFunction();
    }, [ fetcherFunction ]);

    return {
        customer, isLoading
    }
}

export default useFetchCustomer;