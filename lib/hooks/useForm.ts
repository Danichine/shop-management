import { useEffect, useState, ChangeEvent, SyntheticEvent } from 'react';

type initialValuesType = {
    [key: string]: string | number | undefined;
};

type validateType = {
    [key: string]: ( value: any, values: initialValuesType ) => string | null;
};

type filesType = {
    [key: string]: File | null | undefined;
};

type errorsType = {
    [key: string]: string | undefined | null;
};

type formData = {
    initialValues: initialValuesType;
    validate: validateType;
}

const objectHasValues = ( object: initialValuesType | errorsType, ignoreKeys?: string[] ) => {
    const objectCopy = { ...object };
    if ( ignoreKeys && ignoreKeys.length > 0 ) {
        ignoreKeys.forEach( ignoreKey => delete objectCopy[ ignoreKey ] );
    }
    return Object.values( objectCopy ).some( v => !!v );
}

const objectsAreDifferent = ( object1: initialValuesType, object2: initialValuesType ) => (
    Object.keys( object1 ).length !== Object.keys( object2 ).length ||
            Object.keys(object1).some(( key: keyof initialValuesType ) => object1[key] !== object2[key])
)


const useForm = ({
    initialValues, validate
}: formData) => {
    const [ values, setValues ] = useState( initialValues );
    const [ files, setFiles ] = useState<filesType>({});
    const [ errors, setErrors ] = useState<errorsType>({});
    const [ wasEdited, setWasEdited ] = useState( false );

    useEffect(() => {
        if ( objectHasValues( values, [ 'feeRate' ] ) ) return;

        if ( objectHasValues( initialValues ) && objectsAreDifferent( initialValues, values )  ) {
            setValues( initialValues )
        }
    }, [ initialValues, values ]);

    const getInputProps = ( fieldName: keyof initialValuesType ) => {
        return {
            value: values[ fieldName ] || '',
            onChange: ( event: ChangeEvent<HTMLInputElement> ) => {
                setValues( ( prev ) => ({
                    ...prev,
                    [fieldName]: event.target.value
                }) )
                setWasEdited( true );
            },
            error: errors[ fieldName ]
        }
    };

    const getFileInputProps = ( fieldName: keyof filesType ) => {
        return {
            onChange: ( event: ChangeEvent<HTMLInputElement> ) => {
                setFiles( ( prev ) => ({
                    ...prev,
                    [fieldName]: event.target.files?.[ 0 ]
                }) )
                setWasEdited( true );
            },
            error: errors[ fieldName ]
        }
    };

    const runValidations = () => {
        const validationErrors: errorsType = {};
        Object.keys( validate ).forEach( ( key: keyof errorsType ) => {
            validationErrors[ key ] = validate[ key ]( values[ key ], values )
        });
        return validationErrors;
    };

    const reset = () => {
        setValues({});
        setFiles({});
        setErrors({});
        setWasEdited(false);
    }

    const onSubmit = ( handler: ( formValues: any, formFileValues: any ) => void ) => ( event: SyntheticEvent ) => {
        if ( event ) {
            event.preventDefault();
        }
        const result = runValidations();
        if ( objectHasValues( result ) ) {
            setErrors( result );
            return;
        }
        setErrors({});
        handler( values, files );
    }

    return {
        getInputProps, values, onSubmit, reset,
        wasEdited, getFileInputProps, files
    }
}

export default useForm;