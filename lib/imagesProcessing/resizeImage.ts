import sharp from 'sharp';
const SMALL_IMAGE_PIXEL_SIZE = 200;
const LARGE_IMAGE_PIXEL_SIZE = 900;

export const resizeImage = async ({ resourcePath, imageFile }: { resourcePath: string, imageFile: any }) => {
	if ( !imageFile ) return;

	const { buffer, originalname } = imageFile;

	const date = new Date().toISOString();

	const smallImageName = `${date}-small-${originalname}`;
	const largeImageName = `${date}-large-${originalname}`;

	const smallImageUrl = `/api/images/${resourcePath}${smallImageName}`;
	const largeImageUrl = `/api/images/${resourcePath}${largeImageName}`;

	const smallImageBuffer = await sharp( buffer )
		.resize({
			width: SMALL_IMAGE_PIXEL_SIZE,
			height: SMALL_IMAGE_PIXEL_SIZE,
			fit: sharp.fit.outside
		})
		.toBuffer();

	const largeImageBuffer = await sharp( buffer )
		.resize({
			width: LARGE_IMAGE_PIXEL_SIZE,
			height: LARGE_IMAGE_PIXEL_SIZE,
			fit: sharp.fit.outside
		})
		.toBuffer();

	return {
		imageUrls: { smallImageUrl, largeImageUrl },
		imageBuffers: { smallImageBuffer, largeImageBuffer },
		storedImageNames: { smallImageName, largeImageName }
	}
};
