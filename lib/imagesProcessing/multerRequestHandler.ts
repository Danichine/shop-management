import multer, { FileFilterCallback } from 'multer';

const storage = multer.memoryStorage();

const fileFilter = ( req: any, file: any, cb: FileFilterCallback ) => {
    if ( file.mimetype === 'image/png' || file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg' ) {
        return cb( null, true );
    }
    return cb( null, false )
}

export const multerRequestHandler = multer({ storage, fileFilter }).single('image');

export default multerRequestHandler;