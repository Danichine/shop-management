import { ExportToCsv } from 'export-to-csv';
 
var exampleData = [
  {
	name: 'Test 1',
	age: 13,
	average: 8.2,
	approved: true,
	description: "using 'Content here, content here' "
  },
  {
	name: 'Test 2',
	age: 11,
	average: 8.2,
	approved: true,
	description: "using 'Content here, content here' "
  },
  {
	name: 'Test 4',
	age: 10,
	average: 8.2,
	approved: true,
	description: "using 'Content here, content here' "
  },
];

export const exportToCsv = ({ data, filename, title }: { data: any, filename?: string, title?: string }) => {
	 const options = { 
	   fieldSeparator: ',',
	   quoteStrings: '"',
	   decimalSeparator: ',',
	   showLabels: true, 
	   showTitle: true,
	   title: title || new Date().toLocaleString(),
	   useTextFile: false,
	   useBom: true,
	   useKeysAsHeaders: true,
	   filename: filename || new Date().toISOString(),
	   // headers: ['Column 1', 'Column 2', etc...] <-- Won't work with useKeysAsHeaders present!
	};

	const csvExporter = new ExportToCsv(options);
	 
	csvExporter.generateCsv(data);
}
 