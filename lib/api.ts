import CustomError from "./errors/customError";

type optionsType = {
    method?: string;
    headers?: {
        'Content-Type'?: string;
    };
    body?: any;
}


class Api {
    baseUrl = process.env.BASE_URL || '';
    defaultOptions = {
        method: 'GET'
    };

    mergeOptions( method: string, options?: optionsType, body?: any ) {
        let mergedOptions: optionsType = {
            ...this.defaultOptions,
            ...options,
            method
        };

        if ( body ) {
            mergedOptions = {
                ...mergedOptions,
                body: body instanceof FormData ? body : JSON.stringify( body )
            }
            if ( !(body instanceof FormData) ) {
                mergedOptions = {
                    ...mergedOptions,
                    headers: {
                        ...mergedOptions.headers,
                        'Content-Type': 'application/json'
                    }
                }
            }
        }
        return mergedOptions
    }

    async parseResponse( response: any ) {
        const responseData = await response.json();
        if ( !response.ok ) {
            throw new CustomError(
                responseData.message,
                responseData.statusCode,
                responseData.type
            );
        }
        return responseData;
    }
    
    async get( url: string, options?: optionsType ) {
        const mergedOptions = this.mergeOptions( 'GET', options );
        const response = await fetch(`${this.baseUrl}${url}`, mergedOptions );
        return await this.parseResponse( response );
    };

    async post( url: string, body?: object, options?: optionsType ) {
        const mergedOptions = this.mergeOptions('POST', options, body );
        const response = await fetch(`${this.baseUrl}${url}`, mergedOptions );
        return await this.parseResponse( response );
    };

    async put( url: string, body: object, options?: optionsType ) {
        const mergedOptions = this.mergeOptions('PUT', options, body );
        const response = await fetch(`${this.baseUrl}${url}`, mergedOptions);
        return await this.parseResponse( response );
    };

    async delete( url: string, body?: object, options?: optionsType ) {
        const mergedOptions = this.mergeOptions('DELETE', options, body );
        const response = await fetch(`${this.baseUrl}${url}`, mergedOptions);
        return await this.parseResponse( response );
    };
};

export default new Api();