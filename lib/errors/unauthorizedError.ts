import CustomError from './customError';
const ERROR_TYPE = 'Unauthorized';

export const createUnauthorizedError = ( message = ERROR_TYPE ) => {
    return new CustomError( message, 401, ERROR_TYPE );
}

export const throwUnauthorizedError = ( message?: string ) => {
    throw createUnauthorizedError( message );
}