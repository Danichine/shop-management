import CustomError from './customError';
const ERROR_TYPE = 'UnprocessableEntity';

export const createUnprocessableEntityError = ( message = ERROR_TYPE ) => {
    return new CustomError( message, 422, ERROR_TYPE );
}

export const throwUnprocessableEntityError = ( message?: string ) => {
    throw createUnprocessableEntityError( message );
}