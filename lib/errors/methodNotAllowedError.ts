import CustomError from './customError';
const ERROR_TYPE = 'Method not allowed';

export const createMethodNotAllowedError = ( message = ERROR_TYPE ) => {
    return new CustomError( message, 405, ERROR_TYPE );
}

export const throwMethodNotAllowedError = ( message?: string ) => {
    throw createMethodNotAllowedError( message );
}