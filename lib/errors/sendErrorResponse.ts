import { DEFAULT_MESSAGE, DEFAULT_STATUS, DEFAULT_TYPE } from './customError';
import { NextApiResponse } from 'next';

export const sendErrorResponse = ( res: NextApiResponse, error: any ) => {
    return res.status( error.statusCode || DEFAULT_STATUS ).send( {
        message: error.message || DEFAULT_MESSAGE,
        type: error.type || DEFAULT_TYPE,
        statusCode: error.statusCode || DEFAULT_STATUS
    });
};