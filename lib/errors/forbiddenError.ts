import CustomError from './customError';
const ERROR_TYPE = 'Forbidden';

export const createForbiddenError = ( message = ERROR_TYPE ) => {
    return new CustomError( message, 403, ERROR_TYPE );
}

export const throwForbiddenError = ( message?: string ) => {
    throw createForbiddenError( message );
}