export const DEFAULT_STATUS = 500;
export const DEFAULT_MESSAGE = 'Something went wrong';
export const DEFAULT_TYPE = 'Server error';

export default class CustomError extends Error {
    type: string;
    statusCode: number;
    
    constructor( message = DEFAULT_MESSAGE, statusCode = DEFAULT_STATUS, type = DEFAULT_TYPE ) {
        super( message );
        this.type = type;
        this.statusCode = statusCode
    }

    asObject() {
        return {
            message: this.message,
            statusCode: this.statusCode,
            type: this.type
        }
    }
}