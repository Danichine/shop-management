import CustomError from './customError';
const ERROR_TYPE = 'NotFound';

export const createNotFoundError = ( message = ERROR_TYPE ) => {
    return new CustomError( message, 404, ERROR_TYPE );
}

export const throwNotFoundError = ( message?: string ) => {
    throw createNotFoundError( message );
}