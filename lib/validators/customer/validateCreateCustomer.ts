import { body } from 'express-validator';
import { validateRequestBody } from '../validateRequestBody';

const validators = [
    body('name')
        .exists()
        .trim()
        .isLength({min: 3, max: 50})
        .withMessage('should be between 3 and 50 characters'),
    body('email')
        .if((value: string) => !!value)
        .isLength({min: 5, max: 50})
        .withMessage('snvalid email address'),
    body('telephone')
        .if((value: string) => !!value)
        .isLength({min: 8, max: 15})
        .withMessage('should be between 8 and 15 digits'),
    body('address')
        .isLength({max: 200})
        .withMessage('should be up to 200 characters'),
    body('description')
        .isLength({max: 200})
        .withMessage('should be up to 200 characters')
];

export const validateCreateCustomer = validateRequestBody(validators);