import { body } from 'express-validator';
import { validateRequestBody } from '../validateRequestBody';

const validators = [
    body('name')
        .if((value: string) => !!value)
        .trim()
        .isLength({min: 3, max: 50})
        .withMessage('Name should be between 3 and 50 characters'),
    body('email')
        .if((value: string) => !!value)
        .isLength({min: 5, max: 50})
        .withMessage('Invalid email address'),
    body('telephone')
        .if((value: string) => !!value)
        .isLength({min: 8, max: 15})
        .withMessage('Telephone should be between 8 and 15 digits'),
    body('address')
        .isLength({max: 200})
        .withMessage('Address should be up to 200 characters'),
    body('description')
        .isLength({max: 200})
        .withMessage('Description should be up to 200 characters')
];

export const validateUpdateCustomer = validateRequestBody(validators);