import { body } from 'express-validator';
import { validateRequestBody } from '../validateRequestBody';

const validators = [
    body('email')
        .exists()
        .isLength({min: 5, max: 50})
        .withMessage('Invalid email address. Email should be between 6 and 50 characters.'),
    body('password')
        .exists()
        .isLength({min: 6, max: 50})
        .withMessage('Password should be between 6 and 50 characters.')
];

export const validateLogin = validateRequestBody(validators);