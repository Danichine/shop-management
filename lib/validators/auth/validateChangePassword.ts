import { body } from 'express-validator';
import { validateRequestBody } from '../validateRequestBody';

const validators = [
    body('oldPassword')
        .exists()
        .trim()
        .isLength({max: 50})
        .withMessage('Old Password should be less than 50 characters.'),
    body('newPassword')
        .exists()
        .trim()
        .isLength({min: 6, max: 50})
        .withMessage('New Password should be between 6 and 50 characters.'),
    body('confirmNewPassword')
        .exists()
        .trim()
        .custom((value, { req: { body } }) => value === body.newPassword)
        .withMessage('Passwords should match.')
];

export const validateChangePassword = validateRequestBody(validators);