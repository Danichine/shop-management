import { throwUnprocessableEntityError } from "../errors";
import { STATES, STATES_OPTIONS } from '@/models';

export const validateProductState = (state: STATES_OPTIONS) => {
    if (!state || !Object.values(STATES).includes(state)) {
        throwUnprocessableEntityError('Invalid product state.');
    }
};
