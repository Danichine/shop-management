import { validationResult } from 'express-validator';
import { sendErrorResponse, throwUnprocessableEntityError } from '../errors';
import { Request, Response } from '../types/next';

export const validateRequestBody = ( validations: any[] ) => ( handler: Function) => async ( req: Request, res: Response ) => {
    try {
        if ( !validations || validations.length === 0 ) {
            throwUnprocessableEntityError( 'Validations error.' )
        }
        await Promise.all( validations.map( v => v.run( req )) );

        const result = validationResult( req );
        
        if ( !result.isEmpty() ) {
            const { param, msg } = result.array()[ 0 ];
            throwUnprocessableEntityError( `"${param}" ${msg}` )
        }
        return handler( req, res );
    }
    catch( error: any ) {
        sendErrorResponse( res, error );
    }
};
