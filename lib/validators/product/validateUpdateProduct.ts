import { body } from 'express-validator';
import { validateRequestBody } from '../validateRequestBody';

const validators = [
    body('name')
        .if((value: string) => !!value)
        .trim()
        .isLength({min: 3, max: 50})
        .withMessage('Name should be between 3 and 50 characters'),
    body('price')
        .custom(value => value === undefined || (parseInt(value) >= 0 && parseInt(value) <= 999999))
        .withMessage('Price should be between 0 and 999999'),
    body('feeRate')
        .custom(value => value === undefined || value >= 0 && value <= 100)
        .withMessage('FeeRate should be between 0 and 100'),
    body('brand')
        .isLength({max: 50})
        .withMessage('Brand should be up to 50 characters'),
    body('size')
        .isLength({max: 50})
        .withMessage('Size should be up to 50 characters'),
    body('reservation')
        .isLength({max: 200})
        .withMessage('Reservation should be up to 200 characters'),
    body('category')
        .isLength({max: 50})
        .withMessage('Category should be up to 50 characters'),
    body('subcategory')
        .isLength({max: 50})
        .withMessage('Subcategory should be up to 50 characters'),
    body('color')
        .isLength({max: 20})
        .withMessage('Color should be up to 20 characters'),
    body('description')
        .isLength({max: 200})
        .withMessage('Description should be up to 200 characters'),
];

export const validateUpdateProducts = validateRequestBody(validators);