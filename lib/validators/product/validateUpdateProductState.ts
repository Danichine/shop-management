import { body } from 'express-validator';
import { STATES, STATES_OPTIONS } from '@/models';
import { validateRequestBody } from '../validateRequestBody';

const validators = [
    body('state')
        .exists()
        .trim()
        .custom((value: STATES_OPTIONS) => Object.values(STATES).includes(value))
        .withMessage('State should be one of "available", "sold" or "paid"')
];

export const validateUpdateProductState = validateRequestBody(validators);