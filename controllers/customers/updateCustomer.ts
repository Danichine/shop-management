import type { NextApiRequest, NextApiResponse } from 'next';
import { sendErrorResponse, throwNotFoundError, throwUnprocessableEntityError } from '@/lib/errors';
import { Customer } from '@/models';
import { dbGetCustomerById, dbUpdateCustomer } from '@/interactors'

type Data = {
	customer: Customer,
	message: string
};

export const updateCustomer = async ( req: NextApiRequest, res: NextApiResponse<Data> ) => {
	try {
		const { customerId: id } = req.query;
		const customerId = Array.isArray(id) ? "" : id;

		if ( !customerId ) {
			throwUnprocessableEntityError('Customer ID should be provided.');
		}
		const customer = await dbGetCustomerById(customerId);
		if ( !customer ) {
			throwNotFoundError('Customer not found.');
		}
		const {
			name, email, telephone, address,
			description, defaultFeeRate
		} = req.body;

		if ( !name && !email && !telephone && !address && !description ) {
			throwUnprocessableEntityError('At least one field should be changed.');
		}
		customer.name = name || customer.name;
		customer.email = email || customer.email;
		customer.telephone = telephone || customer.telephone;
		customer.address = address || customer.address;
		customer.description = description || customer.description;
		customer.defaultFeeRate = defaultFeeRate || customer.defaultFeeRate;
		
		await dbUpdateCustomer(customer);
	
		res.status(200)
			.send({
				message: 'Customer updated.',
				customer
			});
	}
	catch( error: any ) {
		sendErrorResponse( res, error );
	}
};

export default updateCustomer;
