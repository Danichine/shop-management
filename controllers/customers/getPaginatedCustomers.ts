import type { NextApiRequest } from 'next';
import { dbGetCustomersFirstPage, dbGetCustomersLastPage, dbGetCustomersNextPage, dbGetCustomersPreviousPage } from '@/interactors';
import { DEFAULT_PER_PAGE_LIMIT, PAGE_OPTIONS, PageOption } from '@/components/Pagination/Pagination';
import { getPaginationState } from '@/lib/utils';

export const getPaginatedCustomers = async ({
	req, filterBy, sortBy, searchBy
}: {
	req: NextApiRequest, filterBy?: string[], sortBy: string, searchBy?: string
}) => {
    const { page: pageId, lastVisibleId: last, limit: limitString } = req.query;

    const page = typeof pageId === 'string' ? pageId : "";
    const lastVisibleId = typeof last === 'string' ? last : "";
    const limit = typeof limitString === 'string' ? parseInt(limitString) : DEFAULT_PER_PAGE_LIMIT;

	// In order to know if there are more pages after or before, we need to query one more item
	const queryLimit = limit + 1

    let customers = [];

    switch (page) {
        case PAGE_OPTIONS.FIRST:
            customers = await dbGetCustomersFirstPage({ sortBy, limit: queryLimit, filters: filterBy, search: searchBy })
            break;
        case PAGE_OPTIONS.NEXT:
            customers = await dbGetCustomersNextPage({ sortBy, limit: queryLimit, lastVisibleId, filters: filterBy, search: searchBy })
            break;
        case PAGE_OPTIONS.PREVIOUS:
            customers = await dbGetCustomersPreviousPage({ sortBy, limit: queryLimit, lastVisibleId, filters: filterBy, search: searchBy })
            break;
        case PAGE_OPTIONS.LAST:
            customers = await dbGetCustomersLastPage({ sortBy, limit: queryLimit, filters: filterBy, search: searchBy })
    break;
        default:
            customers = await dbGetCustomersFirstPage({ sortBy, limit: queryLimit, filters: filterBy, search: searchBy })
            break;
    }

	const {
		lastVisible, hasNextPage, hasPreviousPage
	} = getPaginationState({ resultItems: customers, requestedPage: page as PageOption, queryLimit })

    return {
        customers, lastVisibleId: lastVisible, hasNextPage, hasPreviousPage
    };
};

export default getPaginatedCustomers;
