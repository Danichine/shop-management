import type { NextApiRequest, NextApiResponse } from 'next';
import { sendErrorResponse, throwUnprocessableEntityError } from '@/lib/errors';
import { dbCreateCustomer } from '@/interactors';
import { Customer } from '@/models';

type Data = {
	customer: Customer,
	message: string
};

export const createCustomer = async (req: NextApiRequest, res: NextApiResponse<Data>) => {
	try {
		const {
			name, email, telephone, address,
			description, defaultFeeRate
		} = req.body;

		if (!name) {
			throwUnprocessableEntityError('Name is mandatory');
		}

		const customer = Customer.create({ name, email, telephone, address, description, defaultFeeRate })

		await dbCreateCustomer(customer)
	
		res.status(201)
			.send({
				message: 'Customer created.',
				customer: customer
			});
	}
	catch(error: any) {
		sendErrorResponse(res, error);
	}
};

export default createCustomer;
