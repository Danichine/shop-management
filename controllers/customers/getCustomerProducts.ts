import type { NextApiRequest, NextApiResponse } from 'next';
import { sendErrorResponse, throwNotFoundError, throwUnprocessableEntityError } from '@/lib/errors';
import { Product, Customer } from '@/models';
import { dbGetCustomerById } from '@/interactors';
import { getPaginatedProducts } from '../products';

type Data = {
	products: Product[];
	lastVisibleId: string;
	hasNextPage: boolean;
	hasPreviousPage: boolean;
	message: string;
	customer: Customer;
};

export const getCustomerProducts = async (req: NextApiRequest, res: NextApiResponse<Data> ) => {
	try {
		const { customerId: id, filter, sort, search } = req.query;
		const customerId = typeof id === 'string' ? id : '';
		const filterBy = typeof filter === 'string' ? [filter] : undefined;
		const sortBy = typeof sort === 'string' ? sort : '';
		const searchBy = typeof search === 'string' ? search : undefined;

		if (!customerId ) {
			throwUnprocessableEntityError('Customer ID should be provided.');
		}
		
		const customer = await dbGetCustomerById(customerId)
		if (!customer ) {
			throwNotFoundError('Customer not found.');
		}
	
		const {
			products,
			lastVisibleId,
			hasNextPage,
			hasPreviousPage
		} = await getPaginatedProducts({ customerId: customer.id, req, filterBy, sortBy, searchBy })
	
		res.status(200)
			.send({
				message: 'Customer products found successfully.',
				products,
				lastVisibleId,
				customer,
				hasNextPage,
				hasPreviousPage
			});
	}
	catch(error: any ) {
		sendErrorResponse(res, error );
	}
}

export default getCustomerProducts;