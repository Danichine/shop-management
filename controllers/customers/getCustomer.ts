import type { NextApiRequest, NextApiResponse } from 'next';
import { sendErrorResponse, throwNotFoundError, throwUnprocessableEntityError } from '@/lib/errors';
import { Customer } from '@/models';
import { dbGetCustomerById } from '@/interactors';

type Data = {
	customer: Customer,
	message: string
};

export const getCustomer = async (req: NextApiRequest, res: NextApiResponse<Data>) => {
	try {
		const { customerId: id } = req.query;
		const customerId = typeof id === 'string' ? id : "";

		if (!customerId) {
			throwUnprocessableEntityError('Customer ID should be provided.');
		}

		const customer = await dbGetCustomerById(customerId);
		if (!customer) {
			throwNotFoundError('Customer not found.');
		}
	
		res.status(200)
			.send({
				message: 'Customer found successfully.',
				customer
			});
	}
	catch(error: any) {
		sendErrorResponse(res, error);
	}
};

export default getCustomer;
