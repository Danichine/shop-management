import type { NextApiRequest, NextApiResponse } from 'next';
import { sendErrorResponse } from '@/lib/errors';
import { Customer } from '@/models';
import getPaginatedCustomers from './getPaginatedCustomers';

type Data = {
	customers: Customer[];
	message: string;
	hasNextPage: boolean,
	hasPreviousPage: boolean,
	lastVisibleId: string;
};

export const getCustomers = async ( req: NextApiRequest, res: NextApiResponse<Data> ) => {
	try {
		const { filter, sort, search } = req.query;

		const sortBy = typeof sort === 'string' ? sort : '';
		const filterBy = typeof filter === 'string' ? [ filter ] : undefined;
		const searchBy = typeof search === 'string' ? search : undefined;
	
		const {
			customers,
			hasNextPage,
			hasPreviousPage,
			lastVisibleId
		} = await getPaginatedCustomers({ req, filterBy, sortBy, searchBy });

		res.status(200)
			.send({
				message: 'Customers fetched correctly.',
				customers,
				hasNextPage,
				hasPreviousPage,
				lastVisibleId
			});
	}
	catch( error: any ) {
		sendErrorResponse( res, error );
	}
};

export default getCustomers;
