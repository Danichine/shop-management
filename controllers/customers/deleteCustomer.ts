import type { NextApiRequest, NextApiResponse } from 'next';
import { sendErrorResponse, throwNotFoundError, throwUnprocessableEntityError } from '@/lib/errors';
import { dbDeleteCustomer, dbGetCustomerById, removeImages } from '@/interactors';
import { Product } from '@/models';
import { decodeImageUrls } from '@/lib/utils';

type Data = {
	customerId: string,
	message: string
};

export const deleteCustomer = async ( req: NextApiRequest, res: NextApiResponse<Data> ) => {
	try {
		const { customerId: id } = req.query;

		const customerId = Array.isArray(id) ? "" : id;

		if ( !customerId ) {
			throwUnprocessableEntityError( 'Customer ID should be provided.' );
		}

		const customer = await dbGetCustomerById(customerId);
		if ( !customer ) {
			throwNotFoundError( 'Customer not found.' );
		}

		const { deletedProducts } = await dbDeleteCustomer(customerId)

		const imageUrls = deletedProducts.reduce( ( acc: string[], p: Product ) => {
			const { smallImageUrl, largeImageUrl } = p.images;

			if ( !smallImageUrl || !largeImageUrl ) return acc;
			
			const decodedImagesUrls = decodeImageUrls([ smallImageUrl, largeImageUrl ]);

			return [ ...acc, ...decodedImagesUrls ];
		} , []);
		removeImages( imageUrls );

		res.status(200)
			.send({
				message: 'Customer deleted successfully.',
				customerId: customer.id.toString()
			});
	}
	catch( error: any ) {
		sendErrorResponse( res, error );
	}
}

export default deleteCustomer;
