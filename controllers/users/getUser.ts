import type { Request, Response } from '@/lib/types';
import { sendErrorResponse } from '@/lib/errors';

type Data = {
	message: string;
    user: {
        name: string;
        email: string;
        role: string;
    };
};

export const getUser = async ( req: Request, res: Response<Data> ) => {
	try {        
        res.status(200)
            .send({
                message: 'User found.',
                user: {
                    name: req.userName || "",
                    email: req.userEmail || "",
                    role: req.userRole || ""
                }
            });
	}
	catch( error: any ) {
        sendErrorResponse( res, error );
	}
}

export default getUser;