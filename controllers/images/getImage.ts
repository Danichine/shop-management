import type { NextApiRequest, NextApiResponse } from 'next';
import { sendErrorResponse, throwUnprocessableEntityError } from '@/lib/errors';
import { downloadImage } from '@/interactors';

type Data = NodeJS.ReadableStream;

export const getImage = async ( req: NextApiRequest, res: NextApiResponse<Data> ) => {
	try {
		const { imageId } = req.query;

		if ( !imageId ) {
			throwUnprocessableEntityError( 'Image ID should be provided.' );
		}
        
        const readStream = await downloadImage( imageId.toString() );

        readStream.pipe( res );
	}
	catch( error: any ) {
		error.message = 'This is a custom error message'
		sendErrorResponse( res, error );
	}
};

export default getImage;
