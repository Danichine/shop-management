import type { NextApiRequest, NextApiResponse } from 'next';
import { sendErrorResponse } from '@/lib/errors';
import { removeTokenCookie } from '@/lib/cookies';
import { dbLogout } from '@/interactors';

type Data = {
	message: string
};

export const logout = async ( _: NextApiRequest, res: NextApiResponse<Data> ) => {
	try {
		await dbLogout()
        removeTokenCookie( res );
        
        res.status(200)
            .send({
                message: 'Logged out.'
            });
	}
	catch( error: any ) {
		sendErrorResponse( res, error );
	}
}

export default logout;