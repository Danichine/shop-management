import type { NextApiRequest, NextApiResponse } from 'next';
import { sendErrorResponse } from '@/lib/errors';
import { createTokenCookie } from '@/lib/cookies';
import { dbLogin } from '@/interactors';

type Data = {
	message: string
    userId: string
};


export const login = async ( req: NextApiRequest, res: NextApiResponse<Data> ) => {
	try {
        const { email, password } = req.body;
        
        const { user, accessToken } = await dbLogin(email, password)

        const cookie = createTokenCookie( accessToken );
        res.setHeader( 'Set-Cookie', cookie );
        
        res.status(200)
            .send({
                message: 'Logged in.',
                userId: user.uid.toString()
            });
	}
	catch( error: any ) {
		sendErrorResponse( res, error );
	}
}

export default login;