import type { Request, Response } from '@/lib/types';
import { sendErrorResponse, throwUnauthorizedError } from '@/lib/errors';
import { updatePassword, getAuth } from 'firebase/auth';
import firebaseApp from '@/firebaseConfig';

type Data = {
	message: string
};

export const changePassword = async ( req: Request, res: Response<Data> ) => {
	try {
        const {
            body: { newPassword }
        } = req;

        const loggedUser = getAuth(firebaseApp).currentUser;

        if (!loggedUser) {
            throwUnauthorizedError()
        } else {
            await updatePassword(loggedUser, newPassword)
        }

        res.status(200)
            .send({
                message: 'Password changed.'
            });
	}
	catch( error: any ) {
        sendErrorResponse( res, error );
	}
}

export default changePassword;