import type { Request, Response } from '@/lib/types';
import { sendErrorResponse, throwNotFoundError, throwUnprocessableEntityError } from '@/lib/errors';
import { Product, stateManagerFactory } from '@/models';
import { resizeImage } from '@/lib/imagesProcessing/resizeImage';
import {
	 dbGetProductById, dbUpdateCustomer, removeImages, uploadProductImage,
	 PRODUCTS_IMAGES_ENCODED_PATH,
	 dbGetCustomerById,
	 dbUpdateProduct
} from '@/interactors';
import { decodeImageUrls } from '@/lib/utils';


type Data = {
	product: Product,
	message: string
};

export const updateProduct = async (req: Request, res: Response<Data>) => {
	try {
		const { query: { productId: id }, file } = req;
		const productId = Array.isArray(id) ? "" : id;

		const {
			name, price, feeRate, brand,
			size, category, subcategory, color,
			description, reservation
		} = req.body || {};

		if (!productId) {
			throwUnprocessableEntityError('Product ID should be provided.');
		}
		const product = await dbGetProductById(productId);
		if (!product) {
			throwNotFoundError('Product not found.');
		};

		if (
			!name && price === undefined && feeRate === undefined && !brand
			&& !size && !category && !subcategory && !color
			&& !description && !reservation && !file
		) {
			throwUnprocessableEntityError('At least one field should be changed.');
		}

		// A change in price or feeRate may result in a change of the customer
		if (
				(price && parseFloat(price) > 0)
				|| (feeRate && parseInt(feeRate) > 0)
			) {
			const customer =  await dbGetCustomerById(product.ownerId);
			if (!customer) {
				throwNotFoundError('Customer not found.');
			};
			
			const productStateManager = stateManagerFactory(product);
			const newPrice = price && parseFloat(price) > 0 ? parseFloat(price) : undefined;
			const newFeeRate = feeRate && parseInt(feeRate) > 0 ? parseInt(feeRate) : undefined;

			const { customer: updatedCustomer } = productStateManager.updatePriceAndFee({ product, customer, newPrice, newFeeRate });

			await dbUpdateCustomer(updatedCustomer);
		}

		const { imageUrls, imageBuffers, storedImageNames } = await resizeImage({ resourcePath: PRODUCTS_IMAGES_ENCODED_PATH, imageFile: file }) || {};

		if (imageUrls && imageBuffers && storedImageNames) {
			// upload the new image to the storage
			await uploadProductImage({ imageBuffers, storedImageNames });

			// If product had a previous image, remove it from the storage
			const { images: { smallImageUrl, largeImageUrl} } = product;
			if (largeImageUrl && smallImageUrl) {
				const decodedUrls = decodeImageUrls([ largeImageUrl, smallImageUrl ])
				
				await removeImages(decodedUrls);
			}
		}

		const { smallImageUrl, largeImageUrl } = imageUrls || {};

		product.name = name || product.name;
		product.price = price ? parseFloat(price) : product.price;
		product.feeRate = feeRate ? parseInt(feeRate) : product.feeRate;
		product.brand = brand || product.brand;
		product.size = size || product.size;
		product.category = category || product.category;
		product.subcategory = subcategory || product.subcategory;
		product.color = color || product.color;
		product.description = description || product.description;
		product.reservation = reservation || product.reservation;
		product.images = {
			smallImageUrl: smallImageUrl || product.images?.smallImageUrl,
			largeImageUrl: largeImageUrl || product.images?.largeImageUrl
		}

		await dbUpdateProduct(product);
	
		res.status(200)
			.send({
				message: 'Product updated.',
				product
			});
	}
	catch(error: any) {
		sendErrorResponse(res, error);
	}
};

export default updateProduct;
