import type { NextApiRequest, NextApiResponse } from 'next';
import {
    sendErrorResponse, throwNotFoundError, throwUnprocessableEntityError
} from '@/lib/errors';
import { stateManagerFactory, Product } from '@/models';
import { validateProductState } from '@/lib/validators';
import { dbGetCustomerById, dbGetProductById, dbUpdateCustomer, dbUpdateProduct } from '@/interactors';

type Data = {
	product: Product,
	message: string
};

export const updateState = async (req: NextApiRequest, res: NextApiResponse<Data>) => {
	try {
		const { body: { state }, query: { productId: id } } = req;
		const productId = typeof id === "string" ? id : "";

		validateProductState(state);

		if (!productId) {
			throwUnprocessableEntityError('Product ID should be provided.');
		}

		const product = await dbGetProductById(productId);
		if (!product) {
			throwNotFoundError('Product not found.');
		};
		if (product.state === state) {
			throwUnprocessableEntityError('Product state remains the same.');
		};

		const customer = await dbGetCustomerById(product.ownerId);
		if (!customer) {
			throwNotFoundError('Product owner not found.');
		};

		/**
		 * A change in the product state has side effects on the customer
		 * **/
		const stateManager = stateManagerFactory(product);
		const { product: updatedProduct, customer: updatedCustomer } = stateManager.updateState({ product, customer, newState: state });

		dbUpdateProduct(updatedProduct)
		dbUpdateCustomer(updatedCustomer)
	
		res.status(200)
			.send({
				message: 'Product state updated.',
				product
			});
	}
	catch(error: any) {
		sendErrorResponse(res, error);
	}
};

export default updateState;
