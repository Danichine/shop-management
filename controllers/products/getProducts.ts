import type { NextApiRequest, NextApiResponse } from 'next';
import { sendErrorResponse } from '@/lib/errors';
import { Product } from '@/models';
import getPaginatedProducts from './getPaginatedProducts';

type Data = {
	products: Product[];
	lastVisibleId: string;
	message: string;
	hasNextPage: boolean;
	hasPreviousPage: boolean;
};

export const getProducts = async ( req: NextApiRequest, res: NextApiResponse<Data> ) => {
	try {
		const { filter, sort, customerId: id, search } = req.query;

		const customerId = typeof id === 'string' ? id : '';
		const sortBy = typeof sort === 'string' ? sort : '';
		const filterBy = typeof filter === 'string' ? [filter] : undefined;
		const searchBy = typeof search === 'string' ? search : undefined;

		const {
			products, lastVisibleId, hasNextPage, hasPreviousPage
		} = await getPaginatedProducts({ req, customerId, sortBy, filterBy, searchBy })

		return res.status(200)
			.send({
				message: 'Products found successfully.',
				lastVisibleId,
				products,
				hasNextPage,
				hasPreviousPage
			});
	}
	catch( error: any ) {
		sendErrorResponse( res, error );
	}
};

export default getProducts;