import type { NextApiRequest, NextApiResponse } from 'next';
import { sendErrorResponse, throwNotFoundError, throwUnprocessableEntityError } from '@/lib/errors';
import { Product } from '@/models';
import { dbDeleteProduct, dbGetCustomerById, dbGetProductById, dbUpdateCustomer, removeImages } from '@/interactors';
import { decodeImageUrls } from '@/lib/utils';

type Data = {
	product: Product,
	message: string
};

export const deleteProduct = async (req: NextApiRequest, res: NextApiResponse<Data>) => {
	try {
		const { productId: id } = req.query;

		const productId = Array.isArray(id) ? "" : id;

		if (!productId) {
			throwUnprocessableEntityError('Product ID should be provided.');
		}
	
		const product = await dbGetProductById(productId);
		if (!product) {
		  throwNotFoundError('Product not found.');
		}

		const customer = await dbGetCustomerById(product.ownerId);
		if (!customer) {
			throwNotFoundError('Product owner not found.');
		}

		customer.deleteProduct(product)
		
		await dbUpdateCustomer(customer);

		await dbDeleteProduct(productId);

		const { images: { smallImageUrl, largeImageUrl } } = product;
		
		
		if (smallImageUrl && largeImageUrl) {
			const decodedImagesUrls = decodeImageUrls([ smallImageUrl, largeImageUrl ]);
			removeImages(decodedImagesUrls);
		}
	  
		res.status(200)
			.send({
				message: 'Product deleted.',
				product
			});
	  }
	  catch(error: any) {
		sendErrorResponse(res, error);
	  }
};

export default deleteProduct;
