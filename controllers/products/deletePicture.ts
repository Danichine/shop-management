import type { NextApiRequest, NextApiResponse } from 'next';
import {
    sendErrorResponse, throwNotFoundError, throwUnprocessableEntityError,
} from '@/lib/errors';
import { Product } from '@/models';
import { dbGetProductById, dbUpdateProduct, removeImages } from '@/interactors';

type Data = {
	product: Product,
	message: string
};

export const deletePicture = async ( req: NextApiRequest, res: NextApiResponse<Data> ) => {
	try {
		const { productId: id } = req.query;

		const productId = typeof id === 'string' ? id : '';

		if ( !productId ) {
			throwUnprocessableEntityError( 'Product ID should be provided.' );
		}

		const product = await dbGetProductById(productId);
		if ( !product ) {
			throwNotFoundError( 'Product not found.' );
		};

		const { images: { smallImageUrl, largeImageUrl } } = product;
		
		if ( !smallImageUrl || !largeImageUrl ) {
			throwUnprocessableEntityError('Product doesn\'t have images');
		}

		removeImages([ smallImageUrl, largeImageUrl ]);
		product.images = {
			smallImageUrl: "",
			largeImageUrl: ""
		};

		await dbUpdateProduct(product);
	
		res.status(200)
			.send({
				message: 'Product image deleted.',
				product
			});
	}
	catch( error: any ) {
		sendErrorResponse( res, error );
	}
};

export default deletePicture;
