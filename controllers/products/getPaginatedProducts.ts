import type { NextApiRequest } from 'next';
import {
	dbGetProductsFirstPage, dbGetProductsLastPage, dbGetProductsNextPage,
	dbGetProductsPreviousPage
} from '@/interactors';
import { DEFAULT_PER_PAGE_LIMIT, PAGE_OPTIONS, PageOption } from '@/components/Pagination/Pagination';
import { getPaginationState } from '@/lib/utils';


export const getPaginatedProducts = async ({
	req, customerId, filterBy, sortBy, searchBy,
}: {
	req: NextApiRequest, customerId?: string, filterBy?: string[], sortBy: string, searchBy?: string
}) => {
	const {
		page: pageId, lastVisibleId: last, limit: limitString
	} = req.query;

	const page = typeof pageId === 'string' ? pageId : '';
	const lastVisibleId = typeof last === 'string' ? last : '';
	const limit = typeof limitString === 'string' ? parseInt(limitString) : DEFAULT_PER_PAGE_LIMIT;

	// In order to know if there are more pages after or before, we need to query one more item
	const queryLimit = limit + 1

	let products = [];

	switch (page) {
		case PAGE_OPTIONS.FIRST:
			products = await dbGetProductsFirstPage({ customerId, sortBy, limit: queryLimit, filters: filterBy, search: searchBy })
			break;
		case PAGE_OPTIONS.NEXT:
			products = await dbGetProductsNextPage({ customerId, sortBy, limit: queryLimit, lastVisibleId, filters: filterBy, search: searchBy })
			break;
		case PAGE_OPTIONS.PREVIOUS:
			products = await dbGetProductsPreviousPage({ customerId, sortBy, limit: queryLimit, lastVisibleId, filters: filterBy, search: searchBy })
			break;
		case PAGE_OPTIONS.LAST:
			products = await dbGetProductsLastPage({ customerId, sortBy, limit: queryLimit, filters: filterBy, search: searchBy })
			break;
		default:
			products = await dbGetProductsFirstPage({ customerId, sortBy, limit: queryLimit, filters: filterBy, search: searchBy })
			break;
	}

	const {
		lastVisible, hasNextPage, hasPreviousPage
	} = getPaginationState({ resultItems: products, requestedPage: page as PageOption, queryLimit})
	
	return {
		products, lastVisibleId: lastVisible, hasNextPage, hasPreviousPage
	};
};

export default getPaginatedProducts;
