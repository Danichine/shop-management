import type { NextApiRequest, NextApiResponse } from 'next';
import { sendErrorResponse, throwNotFoundError, throwUnprocessableEntityError } from '@/lib/errors';
import { Product } from '@/models';
import { dbGetProductById } from '@/interactors';

type Data = {
	product: Product,
	message: string
};

export const getProduct = async (req: NextApiRequest, res: NextApiResponse<Data>) => {
	try {
		const { productId: id } = req.query;
		const productId = typeof id === 'string' ? id : "";

		if (!productId) {
			throwUnprocessableEntityError('Product ID should be provided.');
		}
		const product = await dbGetProductById(productId);
		if (!product) {
			throwNotFoundError('Product not found.');
		}
	
		res.status(200).send({ message: 'Product found successfully.', product })
	}
	catch(error: any) {
		sendErrorResponse(res, error);
	}
};

export default getProduct;
