import type { Request, Response } from '@/lib/types';
import { sendErrorResponse, throwNotFoundError, throwUnprocessableEntityError } from '@/lib/errors';
import { Product } from '@/models';
import { resizeImage } from '@/lib/imagesProcessing/resizeImage';
import {
	dbGetCustomerById, dbUpdateCustomer, dbCreateProduct,
	uploadProductImage, PRODUCTS_IMAGES_ENCODED_PATH
} from '@/interactors';

type Data = {
	product: Product;
	message: string;
};

export const createProduct = async ( req: Request, res: Response<Data> ) => {
	try {
		const {
			body: {
				name, price, feeRate, customerId, color,
				brand, size, category, subcategory,
				description, reservation
			},
			file
		} = req;

		const { imageUrls, imageBuffers, storedImageNames } = await resizeImage({ resourcePath: PRODUCTS_IMAGES_ENCODED_PATH, imageFile: file }) || {};

		if (!name || (!price && price !== 0) || (!feeRate && feeRate !== 0) || !customerId) {
			throwUnprocessableEntityError( 'Name, price, feeRate and customerId are mandatory.' );
		}

		const customer = await dbGetCustomerById(customerId);
		if ( !customer ) {
			throwNotFoundError( 'Customer not found.' );
		};

		if ( imageBuffers && storedImageNames ) {
			await uploadProductImage({ imageBuffers, storedImageNames });
		}

		const { smallImageUrl, largeImageUrl } = imageUrls || {};

		const product = Product.create({
			name,
			price,
			feeRate,
			ownerId: customerId,
			brand,
			size,
			color,
			category,
			subcategory,
			description,
			reservation,
            images: {
				smallImageUrl,
				largeImageUrl,
			}
		});

		const createdProduct = await dbCreateProduct(product)

		customer.productIds.push( createdProduct.id );
		customer.totalAvailableProducts++;

		await dbUpdateCustomer(customer);
	
		res.status(201)
			.send({
				message: 'Product created.',
				product: createdProduct
			});
	}
	catch( error: any ) {
		sendErrorResponse( res, error );
	}
};

export default createProduct;