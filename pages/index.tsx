import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useEffect } from 'react'
import Loader from '../components/UI/Loader/Loader';


const Home: NextPage = () => {

    const router = useRouter();

    useEffect(() => {
        const { pathname } = router;
        if (pathname === '/') {
            router.push( 'customers' );
        }
    });

    return (
        <Loader />
    );
};

export default Home;