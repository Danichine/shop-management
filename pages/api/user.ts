import type { Request, Response } from '@/lib/types';
import { sendErrorResponse } from '@/lib/errors';
import { applyMiddleware, authenticateUser, validateRequestMethod } from '@/lib/middleware';
import getUser from '@/controllers/users/getUser';

export const user = async ( req: Request, res: Response ) => {
	try {
        return getUser( req, res );
	}
	catch( error: any ) {
        sendErrorResponse( res, error );
	}
}

export default applyMiddleware( user, [
    validateRequestMethod(['GET']),
	authenticateUser
]);