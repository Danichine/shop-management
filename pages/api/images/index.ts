import type { NextApiRequest, NextApiResponse } from 'next';
import { sendErrorResponse } from '@/lib/errors';
import getImage from '@/controllers/images/getImage';
import {
	applyMiddleware, authenticateUser,
	validateRequestMethod, validateUserRole
} from '@/lib/middleware';
import { ROLES } from '@/roles/roles';


const images = async ( req: NextApiRequest, res: NextApiResponse ) => {
	try {
        return applyMiddleware(
            getImage,
            [
                validateUserRole( ROLES.BASIC )
            ]
        )( req, res );
	}
	catch( error: any ) {
		console.log( error );
		sendErrorResponse( res, error );
	}
};

export default applyMiddleware( images, [
	validateRequestMethod([ 'GET' ]),
	authenticateUser
]);
