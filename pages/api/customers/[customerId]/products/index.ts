import type { NextApiRequest, NextApiResponse } from 'next';
import { sendErrorResponse } from '@/lib/errors';
import { applyMiddleware, authenticateUser, validateRequestMethod, validateUserRole } from '@/lib/middleware';
import { getCustomerProducts } from '@/controllers';
import { ROLES } from '@/roles/roles';

const customerIndex = async ( req: NextApiRequest, res: NextApiResponse ) => {
	try {
		return getCustomerProducts( req, res );
	}
	catch( error: any ) {
		sendErrorResponse( res, error );
	}
}

export default applyMiddleware( customerIndex, [
	validateRequestMethod([ 'GET' ]),
	authenticateUser,
	validateUserRole( ROLES.BASIC )
]);