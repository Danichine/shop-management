import type { NextApiRequest, NextApiResponse } from 'next';
import { sendErrorResponse } from '@/lib/errors';
import { getCustomer, updateCustomer, deleteCustomer} from '@/controllers';
import { authenticateUser, applyMiddleware, validateRequestMethod, validateUserRole } from '@/lib/middleware';
import { ROLES } from '@/roles/roles';
import { validateUpdateCustomer } from '@/lib/validators';

const customer = async ( req: NextApiRequest, res: NextApiResponse ) => {
	try {
		switch( req.method ) {
			case 'PUT':
				return applyMiddleware(
					updateCustomer,
					[
						validateUserRole( ROLES.ADMIN ),
						validateUpdateCustomer
					]
				)( req, res );
			case 'DELETE':
				return applyMiddleware(
					deleteCustomer,
					[
						validateUserRole( ROLES.ADMIN )
					]
				)( req, res );
			default:
				return applyMiddleware(
					getCustomer,
					[
						validateUserRole( ROLES.BASIC )
					]
				)( req, res );
		}
	}
	catch( error: any ) {
		sendErrorResponse( res, error );
	}
};

export default applyMiddleware( customer, [
	validateRequestMethod([ 'GET', 'PUT', 'DELETE' ]),
	authenticateUser
]);