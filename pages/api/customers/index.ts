import type { NextApiRequest, NextApiResponse } from 'next';
import { sendErrorResponse } from '@/lib/errors';
import { applyMiddleware, authenticateUser, validateRequestMethod, validateUserRole } from '@/lib/middleware';
import { getCustomers, createCustomer} from '@/controllers';
import { ROLES } from '@/roles/roles';
import { validateCreateCustomer } from '@/lib/validators';

const customersIndex = async ( req: NextApiRequest, res: NextApiResponse ) => {
	try {
		switch( req.method ) {
			case 'POST':
				return applyMiddleware(
					createCustomer,
					[
						validateUserRole( ROLES.BASIC ),
						validateCreateCustomer
					]
				)( req, res );
			default:
				return applyMiddleware(
					getCustomers,
					[
						validateUserRole( ROLES.BASIC )
					]
				)( req, res );
		}
	}
	catch( error: any ) {
		sendErrorResponse( res, error );
	}
};

export default applyMiddleware( customersIndex, [
	validateRequestMethod([ 'GET', 'POST' ]),
	authenticateUser
]);