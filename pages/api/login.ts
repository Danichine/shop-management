import type { NextApiRequest, NextApiResponse } from 'next';
import { sendErrorResponse } from '@/lib/errors';
import { applyMiddleware, validateRequestMethod } from '@/lib/middleware';
import loginController from '@/controllers/auth/login';
import { validateLogin } from '@/lib/validators';

export const login = async ( req: NextApiRequest, res: NextApiResponse ) => {

	try {
        return loginController( req, res );
	}
	catch( error: any ) {
		sendErrorResponse( res, error );
	}
}

export default applyMiddleware( login, [
    validateRequestMethod(['POST']),
	validateLogin
]);