import type { NextApiRequest, NextApiResponse } from 'next';
import { sendErrorResponse } from '@/lib/errors';
import { applyMiddleware, validateRequestMethod } from '@/lib/middleware';
import logoutController from '@/controllers/auth/logout';

export const logout = async ( req: NextApiRequest, res: NextApiResponse ) => {
	try {
        return logoutController( req, res );
	}
	catch( error: any ) {
		sendErrorResponse( res, error );
	}
}

export default applyMiddleware( logout, [
    validateRequestMethod(['POST'])
]);