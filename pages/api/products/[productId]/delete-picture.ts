import type { NextApiRequest, NextApiResponse } from 'next';
import { sendErrorResponse } from '@/lib/errors';
import {
	applyMiddleware, authenticateUser,
	validateRequestMethod,
	validateUserRole
} from '@/lib/middleware';
import { deletePicture as deletePictureController } from '@/controllers';
import { ROLES } from '@/roles/roles';

const deletePicture = async ( req: NextApiRequest, res: NextApiResponse ) => {
	try {
        return deletePictureController( req, res );
	}
	catch( error: any ) {
		sendErrorResponse( res, error );
	}
};

export default applyMiddleware( deletePicture, [
	validateRequestMethod([ 'PUT' ]),
	authenticateUser,
	validateUserRole( ROLES.ADMIN )
]);