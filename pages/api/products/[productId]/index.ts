import type { NextApiRequest, NextApiResponse } from 'next';
import { sendErrorResponse } from '@/lib/errors';
import { updateProduct, deleteProduct, getProduct } from '@/controllers';
import {
	applyMiddleware, authenticateUser, imageFileHandler,
	validateRequestMethod, validateUserRole
} from '@/lib/middleware';
import { ROLES } from '@/roles/roles';
import { validateUpdateProducts } from '@/lib/validators';

const product = async ( req: NextApiRequest, res: NextApiResponse ) => {
	try {
		switch( req.method ) {
			case 'PUT':
				return applyMiddleware(
					updateProduct,
					[
						validateUserRole( ROLES.ADMIN ),
						imageFileHandler,
						validateUpdateProducts
					]
				)( req, res );
			case 'DELETE':
				return applyMiddleware(
					deleteProduct,
					[
						validateUserRole( ROLES.ADMIN )
					]
				)( req, res );
			default:
				return applyMiddleware(
					getProduct,
					[
						validateUserRole( ROLES.BASIC )
					]
				)( req, res );
		}
	}
	catch( error: any ) {
		sendErrorResponse( res, error );
	}
};

export default applyMiddleware( product, [
	validateRequestMethod([ 'GET', 'PUT', 'DELETE' ]),
	authenticateUser
]);

export const config = {
    api: {
      bodyParser: false
    }
};