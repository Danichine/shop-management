import type { NextApiRequest, NextApiResponse } from 'next';
import { sendErrorResponse } from '@/lib/errors';
import {
	applyMiddleware, authenticateUser,
	validateRequestMethod,
	validateUserRole
} from '@/lib/middleware';
import { validateUpdateProductState } from '@/lib/validators';
import { updateState as updateStateController } from '@/controllers';
import { ROLES } from '@/roles/roles';

const updateState = async ( req: NextApiRequest, res: NextApiResponse ) => {
	try {
		return updateStateController( req, res );
	}
	catch( error: any ) {
		sendErrorResponse( res, error );
	}
};

export default applyMiddleware( updateState, [
	validateRequestMethod([ 'PUT' ]),
	authenticateUser,
	validateUserRole( ROLES.ADMIN ),
	validateUpdateProductState
]);