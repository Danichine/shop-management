import type { NextApiRequest, NextApiResponse } from 'next';
import { sendErrorResponse } from '@/lib/errors'
import { applyMiddleware, authenticateUser, imageFileHandler, validateRequestMethod, validateUserRole } from '@/lib/middleware';
import { ROLES } from '@/roles/roles';
import { getProducts, createProduct } from '@/controllers';
import { validateCreateProduct } from '@/lib/validators';

const productsIndex = async ( req: NextApiRequest, res: NextApiResponse ) => {
	try {
		switch( req.method ) {
			case 'POST':
				return applyMiddleware(
					createProduct,
					[
						validateUserRole( ROLES.ADMIN ),
						imageFileHandler,
						validateCreateProduct
					]
				)( req, res );
			default:
				return applyMiddleware(
					getProducts,
					[
						validateUserRole( ROLES.BASIC )
					]
				)( req, res );
		}
	}
	catch( error: any ) {
		sendErrorResponse( res, error );
	}
};

export default applyMiddleware( productsIndex, [
	validateRequestMethod([ 'GET', 'POST' ]),
	authenticateUser
]);

export const config = {
    api: {
      bodyParser: false
    }
};