import type { Request, Response } from '@/lib/types';
import { sendErrorResponse } from '@/lib/errors';
import { applyMiddleware, authenticateUser, validateRequestMethod } from '@/lib/middleware';
import { validateChangePassword } from '@/lib/validators';
import changePasswordController from '@/controllers/auth/changePassword';

export const changePassword = async ( req: Request, res: Response ) => {
	try {
        return changePasswordController( req, res );
	}
	catch( error: any ) {
        sendErrorResponse( res, error );
	}
}

export default applyMiddleware( changePassword, [
    validateRequestMethod(['POST']),
	authenticateUser,
    validateChangePassword
]);