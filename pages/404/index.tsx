import { NextPage } from "next";
import styles from './404.module.scss';
import { Text } from '@mantine/core';

const PageNotFound: NextPage = () => (
    <div className={styles.PageNotFound}>
        <div className={styles.container}>
            <Text className={styles.title}>404</Text>
            <Text className={styles.subtitle}>Page not found</Text>
        </div>
    </div>
)

export default PageNotFound;