import { useState } from 'react';
import { Text } from '@mantine/core';
import useForm from '@/lib/hooks/useForm';
import styles from './Profile.module.scss';
import AppShell, { NAVBAR_OPTIONS } from '@/components/AppShell/AppShell';
import { NextPageWithLayout } from '../_app';
import { showNotification } from '@mantine/notifications';
import { Button, TextInput } from '@/components/UI';
import { capitalizeFirstLetter } from '@/lib/utils/capitalizeFirstChar';
import { useRouter } from 'next/router';
import { dbGetCurrentUser } from '@/interactors';
import { throwUnauthorizedError } from '@/lib/errors';
import { changePassword } from '@/services';

export const getServerSideProps = async () => {
    try {
        const user = await dbGetCurrentUser()
   
        if (!user) {
            throwUnauthorizedError()
        }

        return {
            props: {
                name: user.name,
                email: user.email,
                role: user.role
            }
        }
    }
    catch( error ) {
        return {
            redirect: {
                permanent: false,
                destination: '/login',
              },
            props:{},
        }
    }
};

type props = {
    name: string;
    email: string;
    role: string;
}

const Profile: NextPageWithLayout<props> = ( { name, email, role } ) => {
    const [ isLoading, setIsLoading ] = useState( false );

    const router = useRouter();

    const {
        getInputProps, values, onSubmit,
        reset, wasEdited
    } = useForm({
        initialValues: {
            oldPassword: '',
            newPassword: '',
            confirmNewPassword: ''
        },
        validate: {
            oldPassword: value => value.length > 5 && value.length < 50 ? null : 'Between 6 and 50 characters',
            newPassword: value => value.length > 5 && value.length < 50 ? null : 'Between 6 and 50 characters',
            confirmNewPassword: ( value, values ) => value === values.newPassword ? null : 'Passwords should match'
        }
    });

    const handleSubmit = onSubmit( async ( finalValues: typeof values) => {
        try {
            setIsLoading( true );

            await changePassword(finalValues as { oldPassword: string, newPassword: string, confirmNewPassword: string })

            reset();
            showNotification({
                title: 'Success!',
                message: 'Password changed.',
                color: 'success'
            });
        }
        catch( error: any ) {
            if ( error?.statusCode === 401 ) {
                router.push( '/login' );
            }
            showNotification({
                title: 'Failed!',
                message: error.message || 'Password change failed. Try Again later.',
                color: 'error'
            });
        }
        finally {
            setIsLoading( false );
        }
    })

    return (
        <div className={styles.Profile}>
            <Text size='xl' weight={600}>{name}</Text>
            <Text size='lg'>{email}</Text>
            <Text color='blue'>{capitalizeFirstLetter( role )}</Text>
            <form className={styles.changePassword} onSubmit={handleSubmit}>
                <Text mb='1rem' size='lg'>Change Password</Text>
                <TextInput
                    className={styles.textInput}
                    label='Old Password'
                    type='password'
                    saveErrorSpace
                    required
                    {...getInputProps( 'oldPassword' )}
                />
                <TextInput
                    className={styles.textInput}
                    label='New Password'
                    type='password'
                    saveErrorSpace
                    required
                    {...getInputProps( 'newPassword' )}
                />
                <TextInput
                    className={styles.textInput}
                    label='Confirm New Password'
                    type='password'
                    saveErrorSpace
                    required
                    {...getInputProps( 'confirmNewPassword' )}
                />
                <Button
                    type='submit'
                    disabled={isLoading || !wasEdited}
                    label={isLoading ? 'Saving...' : 'Save'}
                />
            </form>
        </div>
    );
};

Profile.getLayout = page => (
    <AppShell
        title='Profile'
        selectedMenuOption={NAVBAR_OPTIONS.PROFILE}
    >
        {page}
    </AppShell>
);

export default Profile;