import type { NextPage } from 'next';
import useForm from '@/lib/hooks/useForm';
import { useRouter } from 'next/router';
import { useState } from 'react';
import styles from './login.module.scss';
import { showNotification } from '@mantine/notifications';
import { Button, TextInput } from '@/components/UI';
import Head from 'next/head';
import { login } from '@/services';


const Login: NextPage = () => {
    const router = useRouter();
    const [ isLoading, setIsLoading ] = useState( false );

    const {
        getInputProps, values, onSubmit, wasEdited
    } = useForm({
        initialValues: {
            email: '',
            password: ''
        },
        validate: {
            email: (value) => (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value) ? null : 'Invalid email'),
            password: value => value.length > 5 && value.length < 50 ? null : 'Password should be between 5 and 50 characters' 
        }
    });

    const handleSubmit = onSubmit( async ( finalValues: typeof values) => {
        try {
            setIsLoading( true );

            await login(finalValues as { email: string, password: string });

            showNotification({
                title: 'Success!',
                message: 'Logged in.',
                color: 'success'
            });

            router.push( '/customers' );
        }
        catch( error: any ) {
            setIsLoading( false );

            showNotification({
                title: 'Failed!',
                message: error.message || 'Login failed. Try Again later.',
                color: 'error'
            });
        }
    })

	return (
		<div className={styles.Login}>
            <Head>
				<title>Login</title>
			</Head>
            <form
                onSubmit={handleSubmit}
                className={styles.formContainer}
                autoComplete='on'
            >
                <TextInput
                    label="Email"
                    placeholder='your@email.com'
                    saveErrorSpace
                    required
                    {...getInputProps( 'email' )}
                />
                <TextInput
                    label="Password"
                    type='password'
                    placeholder='******'
                    saveErrorSpace
                    required
                    {...getInputProps( 'password' )}
                />
                <div className={styles.buttonContainer}>
                    <Button
                        type='submit'
                        disabled={!wasEdited}
                        label={isLoading ? 'Loading...' : 'Login'}
                    />
                </div>
            </form>
		</div>
	)
};

export default Login;
