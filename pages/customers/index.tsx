import { useMemo, useState } from 'react';
import styles from './Customers.module.scss';
import Grid from '@/components/Grid/Grid';
import Toolbar from '@/components/Toolbar/Toolbar';
import Pagination from '@/components/Pagination/Pagination';
import CustomerGridItem from '@/components/CustomerGridItem/CustomerGridItem';
import UpdateCustomerModal from '@/components/UpdateCustomerModal/UpdateCustomerModal';
import AppShell, { NAVBAR_OPTIONS } from '@/components/AppShell/AppShell';
import { NextPageWithLayout } from '../_app';
import FAB from '@/components/UI/FAB/FAB';
import useFetchCustomers from '@/lib/hooks/useFetchCustomers';
import { CUSTOMER_SORT_OPTIONS, CustomerSortOption } from '@/models/customer/sortOptions';
import { CUSTOMER_FILTER_OPTIONS, DEFAULT_CUSTOMER_FILTER } from '@/models/customer/filterOptions';
import EmptyListMessage from '@/components/EmptyListMessage/EmptyListMessage';
import Loader from '@/components/UI/Loader/Loader';
import { exportToCsv } from '@/lib/csv/exportToCsv';

const Customers: NextPageWithLayout = () => {
	const [ isOpen, setIsOpen ] = useState( false );

	const {
		customersList, limit, setLimit,
		sortValue, setSortValue, filterValue,
		setFilterValue, fetchCustomers, onSearch,
		searchValue, isLoading, hasNextPage, hasPreviousPage
	} = useFetchCustomers();

	const customersItemComponents = customersList.map( c => (
		<CustomerGridItem
			key={c.id}
			customer={c}
		/>
	));

	const onCustomerCreated = () => {
		setIsOpen( false );
		setSortValue( (prevSort: CustomerSortOption) => {
			if ( prevSort === CUSTOMER_SORT_OPTIONS.CREATED ) {
				fetchCustomers();
			}
			return CUSTOMER_SORT_OPTIONS.CREATED;
		});
	};

	const exportCustomers = () => {
		const data = customersList.map((customer) => customer.asCsvObject())
		exportToCsv({ data, filename: `Customers ${new Date().toISOString()}` })
	}

	const customersExist = customersItemComponents?.length > 0;
	const isFiltering = filterValue !== DEFAULT_CUSTOMER_FILTER || !!searchValue;

	const placeholderComponent = useMemo( () => isLoading ?
		<Loader /> :
		<EmptyListMessage
			message='There are no customers yet'
			secondaryMessage='There are no customers for your current applied filters.'
			applySecondary={isFiltering}
		/>,
		[ isFiltering, isLoading ]
	);

	return (
		<div className={styles.Customers}>
			<UpdateCustomerModal
				isOpen={isOpen}
				onClose={() => setIsOpen( false )}
				onUpdateCustomer={onCustomerCreated}
			/>
			<Toolbar
				className={styles.toolbar}
				sortValue={sortValue}
				sortOptions={[
					{ value: CUSTOMER_SORT_OPTIONS.UPDATED, label: 'Date updated' },
					{ value: CUSTOMER_SORT_OPTIONS.CREATED, label: 'Date created' },
					{ value: CUSTOMER_SORT_OPTIONS.DEBT, label: 'Debt' },
					{ value: CUSTOMER_SORT_OPTIONS.PRODUCTS, label: 'Products' },
				]}
				onSortValueChange={setSortValue as (value: string) => void}
				filterValue={filterValue}
				filterOptions={[
					{ value: CUSTOMER_FILTER_OPTIONS.ALL, label: 'All' },
					{ value: CUSTOMER_FILTER_OPTIONS.PRODUCTS, label: 'Products' },
					{ value: CUSTOMER_FILTER_OPTIONS.DEBT, label: 'Debt' }
				]}
				onFilterValueChange={setFilterValue as (value: string) => void}
				onSearch={onSearch}
			/>
			<Pagination
				onPageSet={fetchCustomers}
				onLimitSet={setLimit}
				limit={limit}
				hasNextPage={hasNextPage}
				hasPreviousPage={hasPreviousPage}
			/>
			{customersExist ?
				<Grid className={styles.gridContainer} itemSize='l'>
					{customersItemComponents}
				</Grid> :
				placeholderComponent
			}
			<div className={styles.fabContainer}>
				<FAB
					onClick={exportCustomers}
					iconName='table'
					relativePosition
				/>
				<FAB
					onClick={() => setIsOpen( true )}
					iconName='plus'
					relativePosition
				/>
			</div>
		</div>
	);
};

Customers.getLayout = page => (
	<AppShell
		selectedMenuOption={NAVBAR_OPTIONS.CUSTOMERS}
		title='Customers'
	>
		{page}
	</AppShell>
);

export default Customers;