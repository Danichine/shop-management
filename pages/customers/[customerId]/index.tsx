import { useState } from 'react';
import { Text } from '@mantine/core';
import styles from './CustomerDetail.module.scss';
import { Customer, PRODUCT_FILTER_OPTION, PRODUCT_SORT_OPTION, Product } from '@/models';
import { useRouter } from 'next/router';
import { AppShell, NAVBAR_OPTIONS, ProductsGrid } from '@/components';
import { IconButton, FAB } from '@/components/UI';
import {
    UpdateCustomerModal, UpdateProductModal, UpdateProductStateModal,
    ViewImageModal
} from '@/components';
import classNames from 'classnames';
import useFetchCustomerProducts from '@/lib/hooks/useFetchCustomerProducts';
import Image from 'next/image';
import { exportToCsv } from '@/lib/csv/exportToCsv';
import { NextPageWithLayout } from '@/pages/_app';

type props = {
    onShowDialog: () => {}
};

const createStatisticItem = (itemLabel: string, itemValue: string|number|undefined) => (
    <div className={styles.statistic}>
        <Text className={styles.statisticLabel}>{itemLabel}</Text>
        <Text className={styles.statisticContent}>{itemValue}</Text>
    </div>
);

const createContactInformationItem = (itemLabel: string, itemValue: string|number|undefined) => (
    <>
        <Text className={styles.dataLabel}>{itemLabel}</Text>
        <Text className={styles.dataContent}>{itemValue || '-'}</Text>
    </>
);

const CustomerDetail: NextPageWithLayout<props> = ({ onShowDialog }) => {
    const [ customerModalIsOpen, setCustomerModalIsOpen ] = useState(false);
    const [ editProductModalIsOpen, setEditProductModalIsOpen ] = useState(false);
    const [ updateProductStateModalIsOpen, setUpdateProductStateModalIsOpen ] = useState(false);
    const [ viewImageModalIsOpen, setViewImageModalIsOpen ] = useState(false);
    const [ viewedImagePath, setViewedImagePath ] = useState<string|undefined>();

    const [ editableProduct, setEditableProduct ] = useState<Product|undefined>();    const router = useRouter();

    const { customerId } = router.query;
    const id = typeof customerId === 'string' ? customerId : '';

    const {
        productsList, setProductsList,
        fetchCustomerProducts, limit, setLimit,
        sortValue, setSortValue, filterValue,
        setFilterValue, customerData, setCustomerData,
        onSearch, searchValue, isLoading, hasNextPage,
        hasPreviousPage
    } = useFetchCustomerProducts(id);

    const onCustomerUpdated = (updatedCustomer: Customer) => {
        setCustomerModalIsOpen(false);
        setCustomerData(updatedCustomer);
    }

    const onProductUpdated = (product: Product) => {
        setEditProductModalIsOpen(false);
        setUpdateProductStateModalIsOpen(false);
        setProductsList((prevState) => {
            const index = productsList.findIndex(p => p.id === product.id);
            if (index >= 0) {
                prevState[ index ] = product;
                return prevState
            }
            return [ product, ...prevState ];
        });
        setEditableProduct(undefined);
    }

    const onEditProductDetailsClicked = (product: Product) => {
        setEditableProduct(product);
        setEditProductModalIsOpen(true);
    }

    const onUpdateProductStateClicked = (product: Product) => {
        setEditableProduct(product);
        setUpdateProductStateModalIsOpen(true);
    }

    const onViewImageClicked = (product: Product) => {
        if (!product.images?.largeImageUrl) return;
        setViewedImagePath(product.images.largeImageUrl);
        setViewImageModalIsOpen(true);
    };

    const onEditProductModalClosed = () => {
        setEditProductModalIsOpen( false);
        setEditableProduct(undefined);
    }

    const onUpdateProductStateModalClosed = () => {
        setUpdateProductStateModalIsOpen( false);
        setEditableProduct(undefined);
    }

    const onViewImageModalClosed = () => {
        setViewImageModalIsOpen(false);
        setViewedImagePath(undefined);
    }

    const onProductDeleted = (product: Product) => {
        onEditProductModalClosed();
        setProductsList(prevState => prevState.filter(p => p.id !== product.id));
    }

    const exportCustomerProducts = () => {
        if (!customerData || !productsList) return;

		const products = productsList.map((product) => {
            return {
                customerName: customerData.name,
                customerEmail: customerData.email,
                date: new Date().toLocaleString(),
                ...product.asCsvObject()
            }
        })

        if (!!products.length) {
            exportToCsv({ data: products, filename: `${customerData.name} Productos ${new Date().toISOString()}` })
        }

        const customer = {
            ...customerData.asCsvObject(),
            date: new Date().toLocaleString(),
        }

		exportToCsv({ data: [ customer ], filename: `${customerData.name} ${new Date().toISOString()}` })
	}

    const {
        name, email, telephone, address, description,
        totalAvailableProducts, totalDebt, totalDebtProducts,
        totalSold, totalSoldProducts, totalPaid, defaultFeeRate,
        dateCreated
    } = customerData || {};

    const productsExist = !!productsList && productsList.length > 0;

    return (
        <div className={styles.CustomerDetail}>
            <ViewImageModal
                isOpen={viewImageModalIsOpen}
                onClose={onViewImageModalClosed}
                imagePath={viewedImagePath}
            />
           <UpdateProductStateModal
                isOpen={updateProductStateModalIsOpen}
                onClose={onUpdateProductStateModalClosed}
                onUpdateProduct={onProductUpdated}
                product={editableProduct}
            />
            <UpdateProductModal
                isOpen={editProductModalIsOpen}
                onClose={onEditProductModalClosed}
                onUpdateProduct={onProductUpdated}
                editableProduct={editableProduct}
                showDialog={onShowDialog}
                ownerId={customerData.id}
                onDeleteProduct={onProductDeleted}
                defaultFeeRate={defaultFeeRate}
            />
            <UpdateCustomerModal
                isOpen={customerModalIsOpen}
                onClose={() => setCustomerModalIsOpen(false)}
                onUpdateCustomer={onCustomerUpdated}
                editableCustomer={customerData}
                showDialog={onShowDialog}
            />
            <div className={styles.customerInformation}>
                <div className={styles.imageContainer}>
                    <Image
                        src='/product-placeholder.png'
                        alt='Customer Image'
                        layout='fill'
                        objectFit='cover'
                    />
                </div>
                <div className={styles.basicInformation}>
                    <div className={styles.nameContainer}>
                        <Text size='xl'>{name}</Text>
                        <IconButton
                            iconName='edit'
                            onClick={() => setCustomerModalIsOpen(true)}
                            withBackground
                        />
                    </div>
                    <Text size='sm'>{dateCreated?.toLocaleDateString()}</Text>
                    <div className={styles.contactInformation}>
                        {createContactInformationItem('Email', email)}
                        {createContactInformationItem('Telephone', telephone)}
                        {createContactInformationItem('Address', address)}
                        {createContactInformationItem('Description', description)}
                    </div>
                </div>
                <div className={styles.statistics}>
                    {createStatisticItem('Available Products', totalAvailableProducts)}
                    {createStatisticItem('Debt', totalDebt)}
                    {createStatisticItem('Debt Products', totalDebtProducts)}
                    {createStatisticItem('Total Sold', totalSold)}
                    {createStatisticItem('Total Sold Products', totalSoldProducts)}
                    {createStatisticItem('Total Paid', totalPaid)}
                    {defaultFeeRate &&
                        createStatisticItem('Default Fee', defaultFeeRate)}
                </div>
            </div>
            <div className={styles.productsContainer}>
                <ProductsGrid
                    className={classNames(
                        styles.ProductsGrid,
                        { [styles.withProducts]: productsExist }
                    )}
                    products={productsList}
                    onPageChange={fetchCustomerProducts}
                    filterValue={filterValue}
                    onFilterChange={(value: PRODUCT_FILTER_OPTION) => setFilterValue(value)}
                    sortValue={sortValue}
                    onSortChange={(value: PRODUCT_SORT_OPTION) => setSortValue(value)}
                    onLimitChange={setLimit}
                    limit={limit}
                    onEditDetailsClick={onEditProductDetailsClicked}
                    onUpdateStateClick={onUpdateProductStateClicked}
                    onViewImageClick={onViewImageClicked}
                    onSearch={onSearch}
                    searchValue={searchValue}
                    isLoading={isLoading}
                    hasNextPage={hasNextPage}
                    hasPreviousPage={hasPreviousPage}
                    contrastMode
                />
            </div>
			<div className={styles.fabContainer}>
				<FAB
					onClick={exportCustomerProducts}
					iconName='table'
					relativePosition
				/>
                <FAB
                    onClick={() => setEditProductModalIsOpen(true)}
                    iconName='plus'
                    relativePosition
                />
			</div>
        </div>
    );
};

CustomerDetail.getLayout = page => (
    <AppShell
        backHref='/customers'
        selectedMenuOption={NAVBAR_OPTIONS.CUSTOMERS}
        title='Customer Detail'
    >
        {page}
    </AppShell>
);

export default CustomerDetail;