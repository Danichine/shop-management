import { useState } from 'react';
import ProductsGrid from '@/components/ProductsGrid/ProductsGrid';
import { Product } from '@/models';
import AppShell, { NAVBAR_OPTIONS } from '@/components/AppShell/AppShell';
import { NextPageWithLayout } from '../_app';
import UpdateProductModal from '@/components/UpdateProductModal/UpdateProductModal';
import useFetchProducts from '@/lib/hooks/useFetchProducts';
import UpdateProductStateModal from '@/components/UpdateProductStateModal/UpdateProductStateModal';
import ViewImageModal from '@/components/ViewImageModal/ViewImageModal';


type props = {
	onShowDialog: () => {}
};

const Products: NextPageWithLayout<props> = ({ onShowDialog }) => {
	const [ editProductModalIsOpen, setEditProductModalIsOpen ] = useState(false);
	const [ updateProductStateModalIsOpen, setUpdateProductStateModalIsOpen ] = useState(false);
	const [ editableProduct, setEditableProduct ] = useState<Product|undefined>();
	const [ viewImageModalIsOpen, setViewImageModalIsOpen ] = useState(false);
	const [ viewedImagePath, setViewedImagePath ] = useState<string|undefined>(undefined);

	const {
		productsList, setProductsList, fetchProducts,
		limit, setLimit, sortValue, setSortValue,
		filterValue, setFilterValue, onSearch, searchValue,
		isLoading, hasNextPage, hasPreviousPage
	} = useFetchProducts();

	const onProductUpdated = (product: Product) => {
		setEditProductModalIsOpen(false);
		setUpdateProductStateModalIsOpen(false);
		setProductsList((prevState) => {
			const index = productsList.findIndex(p => p.id === product.id);
			if (index >= 0) {
				prevState[ index ] = product;
			} else {
				[ product, ...prevState ]
			}
			return prevState
		});
		setEditableProduct(undefined);
	}

	const onEditProductDetailsClicked = (product: Product) => {
		setEditableProduct(product);
		setEditProductModalIsOpen(true);
	};

	const onUpdateProductStateClicked = (product: Product) => {
		setEditableProduct(product);
		setUpdateProductStateModalIsOpen(true);
	};

	const onViewImageClicked = (product: Product) => {
		if (!product.images?.largeImageUrl) return;
		setViewedImagePath(product.images.largeImageUrl);
		setViewImageModalIsOpen(true);
	};

	const onProductModalClosed = () => {
		setEditProductModalIsOpen(false);
		setUpdateProductStateModalIsOpen(false);
		setEditableProduct(undefined);
	}

	const onViewImageModalClosed = () => {
		setViewImageModalIsOpen(false);
		setViewedImagePath(undefined);
	}

	const onProductDeleted = (product: Product) => {
		onProductModalClosed();
		setProductsList(prevState => prevState.filter(p => p.id !== product.id));
	}

	return (
		<>
			<ViewImageModal
				isOpen={viewImageModalIsOpen}
				onClose={onViewImageModalClosed}
				imagePath={viewedImagePath}
			/>
			<UpdateProductStateModal
				isOpen={updateProductStateModalIsOpen}
				product={editableProduct}
				onClose={onProductModalClosed}
				onUpdateProduct={onProductUpdated}
			/>
			<UpdateProductModal
				isOpen={editProductModalIsOpen}
				editableProduct={editableProduct}
				onClose={onProductModalClosed}
				onUpdateProduct={onProductUpdated}
				ownerId={editableProduct?.id || ""}
				showDialog={onShowDialog}
				onDeleteProduct={onProductDeleted}
			/>
			<ProductsGrid
				products={productsList}
				onPageChange={fetchProducts}
				onLimitChange={setLimit}
				limit={limit}
				sortValue={sortValue}
				onSortChange={setSortValue}
				filterValue={filterValue}
				onFilterChange={setFilterValue}
				onEditDetailsClick={onEditProductDetailsClicked}
				onUpdateStateClick={onUpdateProductStateClicked}
				onViewImageClick={onViewImageClicked}
				onSearch={onSearch}
				searchValue={searchValue}
				isLoading={isLoading}
				hasNextPage={hasNextPage}
				hasPreviousPage={hasPreviousPage}
			/>
		</>
);
};

Products.getLayout = page => (
	<AppShell
		selectedMenuOption={NAVBAR_OPTIONS.PRODUCTS}
		title='Products'
	>
		{page}
	</AppShell>
);

export default Products;