import '../styles/globals.scss';
import type { AppProps } from 'next/app';
import { MantineProvider } from '@mantine/core';
import { NotificationsProvider } from '@mantine/notifications';
import { ReactElement } from 'react';
import { NextPage } from 'next';
import mantineTheme from '../styles/mantineTheme';
import DialogProvider from '../components/Dialog/DialogProvider';

export type NextPageWithLayout<T = {}> = NextPage<T> & {
	getLayout?: (page: ReactElement) => ReactElement
};
  
type AppPropsWithLayout = AppProps & {
	Component: NextPageWithLayout
};

function MyApp({ Component, pageProps }: AppPropsWithLayout) {
	const getLayout = Component.getLayout ?? ((page) => page);

	return (
		<MantineProvider
			withGlobalStyles
			withNormalizeCSS
			theme={mantineTheme}
		>
			<NotificationsProvider>
				<DialogProvider>
					{getLayout(<Component {...pageProps} />)}
				</DialogProvider>
			</NotificationsProvider>
		</MantineProvider>
	)
};

export default MyApp;
